#!/usr/bin/env bash
[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"

TC=/bin/tc
#Interface
IF=${2:-lo}
# Rate to throttle to
MIN_RATE=250kbps
# Peak rate to allow
MAX_RATE=250kbps
# Average to delay packets by
LATENCY=400ms
# Jitter value for packet delay
# Packets will be delayed by $LATENCY +/- $JITTER
JITTER=100ms
# Package loss
LOSS=5%

start() {
    $TC qdisc add dev $IF root handle 1: htb default 12
    $TC class add dev $IF parent 1:1 classid 1:12 htb rate $MIN_RATE ceil $MAX_RATE
    $TC qdisc add dev $IF parent 1:12 netem delay $LATENCY $JITTER loss $LOSS
}

stop() {
    $TC qdisc del dev $IF root
}

restart() {
    stop
    sleep 1
    start
}

show() {
    $TC -s qdisc ls dev $IF
}

case "$1" in

start)

echo -n "Starting bandwidth shaping: "
start
echo "done"
;;

stop)

echo -n "Stopping bandwidth shaping: "
stop
echo "done"
;;

restart)

echo -n "Restarting bandwidth shaping: "
restart
echo "done"
;;

show)

echo "Bandwidth shaping status for $IF:"
show
echo ""
;;

*)

pwd=$(pwd)
echo "Usage: $(basename $BASH_SOURCE) {start|stop|restart|show}"
;;

esac 
exit 0
