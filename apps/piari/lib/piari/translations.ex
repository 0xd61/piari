defmodule Piari.Translations do
  @moduledoc """
  The Translations context.

  TODO: use changeset for translation changes
  """
  alias Piari.Translations.Translator

  defmacro __using__(opts) do
    quote do
      Module.put_attribute(__MODULE__, :trans_container, unquote(translation_container(opts)))

      @after_compile {Piari.Translations, :__validate_translation_container__}

      @spec __trans__(:container) :: atom
      def __trans__(:container), do: @trans_container
    end
  end

  @doc false
  def __validate_translation_container__(%{module: module}, _bytecode) do
    container = module.__trans__(:container)

    unless Enum.member?(Map.keys(module.__struct__()), container) do
      raise ArgumentError,
        message:
          "The field #{container} used as the translation container is not defined in #{module} struct"
    end
  end

  @type key :: atom
  @type locale :: String.t()
  @type translation :: String.t()
  @type translation_container :: %{optional(locale) => %{optional(key) => translation}}

  def list_languages() do
    Application.fetch_env!(:piari, :languages)
  end

  def lang_is_valid?(lang) do
    Enum.member?(list_languages(), lang)
  end

  @spec list_translations(struct()) :: translation_container()
  def list_translations(struct) do
    Translator.list_translations(struct)
  end

  @spec localize(struct(), locale) :: map()
  def localize(struct, locale) do
    Translator.localize(struct, locale)
  end

  @spec localize(struct(), locale, key) :: translation
  def localize(struct, locale, key) do
    Translator.localize(struct, locale, key)
  end

  @spec localize(struct(), locale, key, term()) :: translation | term()
  def localize(struct, locale, key, fallback) do
    Translator.localize(struct, locale, key, fallback)
  end

  defp translation_container(opts) do
    Keyword.get(opts, :container, :translations)
  end
end
