defmodule Piari.Accounts do
  @moduledoc """
  The Accounts context.
  """
  alias Piari.Accounts.{Users, Auths, Businesses, Keys}

  defdelegate create_auth(user, attrs \\ %{}), to: Auths, as: :new
  defdelegate delete_auth(auth), to: Auths, as: :delete
  defdelegate list_auths(user), to: Auths, as: :list
  defdelegate get_auth!(user, id), to: Auths, as: :get!

  defdelegate key_auth(secret, usage_info), to: Auths, as: :key_auth
  defdelegate create_key(user, attrs \\ %{}), to: Keys, as: :new
  defdelegate revoke_key(key), to: Keys, as: :revoke

  defdelegate get_user!(user_id, preload \\ []), to: Users, as: :get!
  defdelegate change_user(user), to: Users, as: :change
  defdelegate update_user(user, user_params), to: Users, as: :update
  defdelegate update_user_email(user, email), to: Users, as: :update_email
  defdelegate delete_user(user), to: Users, as: :delete
  defdelegate register_user(attrs \\ %{}), to: Users, as: :new
  defdelegate user_has_email?(user, email), to: Users, as: :has_email?

  defdelegate get_business!(id_or_slug, preload \\ []), to: Businesses, as: :get!
  defdelegate update_business(business, attrs), to: Businesses, as: :update
  defdelegate change_business(business), to: Businesses, as: :change
  defdelegate delete_business(business), to: Businesses, as: :delete
  defdelegate verify_business(business, token), to: Businesses, as: :verify
  defdelegate list_user_businesses(user, preload \\ []), to: Businesses, as: :list_by_user
  defdelegate create_business(user, attrs \\ %{}), to: Businesses, as: :new
  defdelegate preload_business(business, preload), to: Businesses, as: :preload
  defdelegate confirm_business_member(member), to: Businesses, as: :confirm_member
  defdelegate get_business_member(business, user), to: Businesses, as: :get_member

  defdelegate get_user_business!(user, id_or_slug, preload \\ []),
    to: Businesses,
    as: :get_by_user!

  defdelegate add_member_to_business(business, user, attrs \\ %{}),
    to: Businesses,
    as: :add_member

  defdelegate remove_member_from_business(business, user),
    to: Businesses,
    as: :remove_member

  @deprecate "use bouncer authorization"
  defdelegate user_is_business_owner?(user, business), to: Users, as: :is_owner?

  @deprecate "use bouncer authorization"
  defdelegate user_is_business_member?(user, business),
    to: Users,
    as: :is_member?

  @spec get_or_register_user(String.t(), String.t(), atom) ::
          {:ok, map()} | {:ok, :not_assigned, map()} | {:ok, :new, map()}
  def get_or_register_user(ext_uid, email, provider) do
    cond do
      # Return user assigned with auth
      #
      user = Users.get_by_auth_id(ext_uid) ->
        {:ok, user}

      # Return user and assign with auth
      user = Users.get_by_email(email) ->
        {:ok, _auth} =
          Auths.new(user, %{
            ext_uid: ext_uid,
            provider: provider
          })

        {:ok, :assigned, user}

      # Return new user and assign with auth
      true ->
        {:ok, user} =
          Users.new(%{
            email: email,
            auths: [%{ext_uid: ext_uid, provider: provider}]
          })

        {:ok, :new, user}
    end
  end

  def auth_url(provider, opts \\ []) do
    Auths.request_url(provider, opts)
  end

  def authenticate(provider, params \\ [], opts \\ []) do
    Auths.authenticate(provider, params, opts)
  end
end
