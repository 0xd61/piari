defmodule Piari.Notifications.Email do
  use Notify.Notifier,
    otp_app: :piari,
    adapter: Notify.Adapter.Email
end
