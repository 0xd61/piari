defmodule Piari.Repo do
  use Ecto.Repo,
    otp_app: :piari,
    adapter: Ecto.Adapters.MyXQL
end
