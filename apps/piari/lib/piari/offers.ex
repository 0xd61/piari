defmodule Piari.Offers do
  @moduledoc """
  The Offers context.
  """

  @behaviour Piari.Bouncer

  alias Piari.Offers.Products

  defdelegate create_product(business, image, attrs \\ %{}), to: Products, as: :new
  defdelegate delete_product(product), to: Products, as: :delete
  defdelegate delete_all_products(multi, business), to: Products, as: :delete_all
  defdelegate update_product(product, attrs), to: Products, as: :update
  defdelegate change_product(product), to: Products, as: :change
  defdelegate list_products(business, preload \\ []), to: Products, as: :list
  defdelegate list_public_products(business, preload \\ []), to: Products, as: :list_public
  defdelegate get_business_product!(business, id, preload \\ []), to: Products, as: :get!
  defdelegate get_public_product!(id, preload \\ []), to: Products, as: :get_public!
  defdelegate localize_product(product, locale), to: Products, as: :localize
  defdelegate publish_product!(product, days, timezone \\ "Etc/UTC"), to: Products, as: :publish!
  defdelegate unpublish_product!(product), to: Products, as: :unpublish!

  @impl true
  def allow(user, action, %{business: business, offer: offer}) do
    Products.allow(user, action, business, offer)
  end
end
