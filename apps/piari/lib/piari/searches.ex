defmodule Piari.Searches do
  alias Search.Query

  def index(index, data) do
    index
    |> Query.on()
    |> Query.put(data)
    |> Search.index()
  end

  def delete(index, data) do
    index
    |> Query.on()
    |> Query.delete(data)
    |> Search.delete()
  end

  def search(query) do
    Search.search(query)
  end
end
