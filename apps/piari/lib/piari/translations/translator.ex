defmodule Piari.Translations.Translator do
  def list_translations(%mod{} = struct) do
    container = mod.__trans__(:container)

    case struct do
      %{^container => data} ->
        data || %{}

      _ ->
        %{}
    end
  end

  def localize(%mod{} = struct, locale) do
    struct
    |> Map.from_struct()
    |> Map.delete(mod.__trans__(:container))
    |> Enum.map(fn {key, val} ->
      {key, localize(struct, locale, key, val)}
    end)
    |> Map.new()
  end

  def localize(struct, locale, key) when is_atom(key) do
    run_localize(struct, locale, key) || Map.fetch!(struct, key)
  end

  def localize(struct, locale, key, fallback) when is_atom(key) do
    run_localize(struct, locale, key) || fallback
  end

  defp run_localize(%mod{} = struct, locale, key) do
    key = to_string(key)
    container = mod.__trans__(:container)

    case struct do
      %{
        ^container => %{
          ^locale => %{
            ^key => string
          }
        }
      } ->
        string

      _ ->
        nil
    end
  end
end
