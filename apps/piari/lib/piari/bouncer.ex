defmodule Piari.Bouncer do
  @type user :: Piari.Accounts.User.t()
  @type id :: integer() | binary()
  @type action :: atom()

  @callback allow(user, action, id) :: {:ok} | {:error, reason :: atom()}

  defmacrop raise_undefined_behaviour(exception, module, top) do
    quote do
      exception =
        case __STACKTRACE__ do
          [unquote(top) | _] ->
            reason = "#{inspect(unquote(module))} does not implement the Piari.Access behaviour"
            %{unquote(exception) | reason: reason}

          _ ->
            unquote(exception)
        end

      reraise exception, __STACKTRACE__
    end
  end

  def can?(_user, _action, nil, _id) do
    true
  end

  def can?(user, action, module, resource) do
    case module.allow(user, action, resource) do
      {:ok} -> true
      {:error, _} -> false
    end
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :can?, [^action], _})
  end
end
