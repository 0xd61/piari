defmodule Piari.Ecto.Changeset do
  import Ecto.Changeset

  def validate_future_date(changeset, field, _options \\ []) do
    validate_change(changeset, field, fn _, date ->
      case Date.compare(date, Date.utc_today()) do
        :gt -> []
        :eq -> []
        :lt -> [{field, {"%{date} must be in the future", date: date}}]
      end
    end)
  end
end
