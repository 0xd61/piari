defmodule Piari.Searches.Offers do
  @moduledoc """
    Build query for Offers
  """
  import Search.Query

  @behaviour Search.Indexes
  @index_name :offers

  def build() do
    %Search.Query{on: @index_name}
  end

  def defaults(query) do
    query
    |> where(field: {"title*", 3}, field: "description*")
  end
end
