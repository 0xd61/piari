defmodule Piari.Accounts.Business do
  use Piari, :schema
  alias Piari.Accounts.Member

  schema "businesses" do
    field(:name, :string)
    field(:phone, :string)
    field(:verified, :boolean, default: false)
    field(:verify_token, :string)
    field(:slug, :string)
    has_many(:members, Member)
    # has_many(:users, through: [:members, :user])

    timestamps()
  end

  @roles ~w(member owner)a

  @doc false
  def changeset(business, attrs) do
    business
    |> cast(attrs, [:phone, :name, :slug])
    |> validate_required([:phone, :name])
    |> unique_constraint(:phone)
    |> validate_format(
      :phone,
      ~r/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
    )
    |> validate_length(:name, max: 50)
    |> slugify_name()
    |> validate_required(:slug)
    |> validate_format(:slug, ~r/^[a-z0-9_\%\-]*$/)
    |> unique_constraint(:slug)
    |> reset_business_verification()
  end

  def change_member(member, attrs, confirm? \\ not Application.get_env(:piari, :member_confirm)) do
    member
    |> cast(attrs, [:role])
    |> put_change(:confirmed, confirm?)
    |> validate_required([:role])
    |> validate_inclusion(:role, @roles)
    |> unique_constraint(:business, name: :users_businesses_business_id_user_id_index)
  end

  def verify(business, token) do
    changeset = change(business, %{verified: true})

    if business.verify_token == token do
      changeset
    else
      add_error(changeset, :title, "verification failed", additional: "wrong token")
    end
  end

  defp reset_business_verification(changeset) do
    case fetch_change(changeset, :phone) do
      {:ok, _} ->
        changeset
        |> put_change(:verified, false)
        |> put_change(:verify_token, generate_token(6))

      _ ->
        changeset
    end
  end

  defp generate_token(length) do
    # credo:disable-for-lines:2
    (:rand.uniform() * :math.pow(10, length))
    |> Kernel.trunc()
    |> to_string
    |> String.pad_leading(length, "0")
  end

  defp slugify_name(changeset) do
    with nil <- get_field(changeset, :slug),
         {:ok, new_name} <- fetch_change(changeset, :name) do
      put_change(changeset, :slug, slugify(new_name))
    else
      _ -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.replace(~r/[^\w-]+/u, "-")
    |> URI.encode_www_form()
    |> String.downcase()
  end

  def can(:owner), do: ["read", "write"]
  def can(:member), do: ["read"]
end
