defmodule Piari.Accounts.Users do
  @moduledoc """
  The Users secondary context.
  """
  use Piari, :context
  alias Piari.Accounts.User
  @behaviour Piari.Bouncer

  @doc """
  Returns the list of users.

  ## Examples

      iex> list()
      [%User{}, ...]

  """
  def list do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get!(123)
      %User{}

      iex> get!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(id, preload \\ []) do
    Repo.get!(User, id)
    |> Repo.preload(preload)
  end

  @doc """
  Gets a single user by email.

  ## Examples

  iex> get_by_email("somebody@example.com")
  %User{}

  iex> get_by_email("somebody@example.com")
  nil

  """
  def get_by_email(email) do
    Repo.get_by(User, email: email)
  end

  @doc """
  Gets a single user by email.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

  iex> get_by_email!("somebody@example.com")
  %User{}

  iex> get_by_email!("somebody@example.com")
  ** (Ecto.NoResultsError)

  """
  def get_by_email!(email) do
    Repo.get_by!(User, email: email)
  end

  @doc """
  Updates a user.
  IT ONLY CHECKS THE EMAIL ADDRESS.
  To update the email address use the function update_user_email/2.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update(%User{} = user, attrs) do
    user
    |> User.update_changeset(attrs)
    |> Repo.update()
  end

  def update_email(%User{} = user, email) do
    user
    |> User.update_email_changeset(email)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete(user)
      {:ok, %User{}}

      iex> delete(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%User{} = user) do
    count = Repo.aggregate(assoc(user, :ownerships), :count, :id)

    if count > 0 do
      {:error, :is_owner}
    else
      Repo.delete(user)
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Registers new user
  """
  def new(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def has_email?(%User{} = user, email) do
    user.email === email
  end

  @deprecate "use bouncer authorization"
  def is_owner?(%User{} = user, %{id: business_id}) do
    case Repo.get_by(assoc(user, :ownerships), business_id: business_id, confirmed: true) do
      nil -> false
      _ -> true
    end
  end

  @deprecate "use bouncer authorization"
  def is_member?(%User{} = user, %{id: business_id}) do
    case Repo.get_by(assoc(user, :memberships), business_id: business_id, confirmed: true) do
      nil -> false
      _ -> true
    end
  end

  def get_by_auth_id(auth_id, preload \\ [])

  def get_by_auth_id(auth_id, preload) when is_integer(auth_id) do
    # credo:disable-for-lines:6
    from(u in User,
      join: a in assoc(u, :auths),
      where: a.id == ^auth_id,
      select: u
    )
    |> Repo.one()
    |> Repo.preload(preload)
  end

  def get_by_auth_id(auth_id, preload) when is_binary(auth_id) do
    # credo:disable-for-lines:6
    from(u in User,
      join: a in assoc(u, :auths),
      where: a.ext_uid == ^auth_id,
      select: u
    )
    |> Repo.one()
    |> Repo.preload(preload)
  end

  @impl true
  def allow(user, _action, %User{id: id}) do
    if user.id === id, do: {:ok}, else: {:error, :no_access}
  end
end
