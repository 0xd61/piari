defmodule Piari.Accounts.Businesses do
  @moduledoc """
  The Businesses secondary context.
  """
  use Piari, :context
  alias Piari.Accounts.{Business, Member, User}
  @behaviour Piari.Bouncer

  def new(user, attrs \\ %{}) do
    multi =
      Multi.new()
      |> Multi.insert(:business, %Business{} |> Business.changeset(attrs))
      |> Multi.insert(:owner, fn %{business: business} ->
        member = %Member{business_id: business.id, user_id: user.id}
        Business.change_member(member, %{role: :owner}, true)
      end)

    case Repo.transaction(multi) do
      {:ok, %{business: business, owner: _}} ->
        {:ok, business}

      {:error, :business, changeset, _} ->
        {:error, changeset}
    end
  end

  def add_member(%Business{} = business, %User{} = user, attrs \\ %{}) do
    member = %Member{business_id: business.id, user_id: user.id}

    member
    |> Business.change_member(attrs)
    |> Repo.insert()
  end

  def confirm_member(%Member{} = member) do
    member
    |> Business.change_member(%{}, true)
    |> Repo.update()
  end

  def remove_member(business, %{id: user_id}) do
    count = Repo.aggregate(assoc(business, :members), :count, :id)

    if count == 1 do
      {:error, :last_member}
    else
      member = Repo.get_by(assoc(business, :members), user_id: user_id)

      if member do
        Repo.delete(member)
      end

      {:ok, member}
    end
  end

  def change(%Business{} = business) do
    business
    |> Business.changeset(%{})
  end

  def get!(id_or_slug, preload \\ [])

  def get!(id, preload) when is_integer(id) do
    Business
    |> Repo.get!(id)
    |> Repo.preload(preload)
  end

  def get!(slug, preload) when is_binary(slug) do
    # credo:disable-for-lines:2
    from(b in Business, where: b.slug == ^slug)
    |> Repo.one!()
    |> Repo.preload(preload)
  end

  def list_by_user(user, preload \\ []) do
    Repo.all(assoc(user, :businesses))
    |> Repo.preload(preload)
  end

  def get_by_user!(user, id_or_slug, preload \\ [])

  def get_by_user!(user, id, preload) when is_integer(id) do
    Repo.get_by!(assoc(user, :businesses), id: id)
    |> Repo.preload(preload)
  end

  def get_by_user!(user, slug, preload) when is_binary(slug) do
    Repo.get_by!(assoc(user, :businesses), slug: slug)
    |> Repo.preload(preload)
  end

  def update(%Business{} = business, attrs) do
    business
    |> Business.changeset(attrs)
    |> Repo.update()
  end

  def delete(%Business{} = business) do
    multi =
      Multi.new()
      |> Multi.delete(:business, business)
      |> Piari.Offers.delete_all_products(business)

    case Repo.transaction(multi) do
      {:ok, %{business: business}} ->
        {:ok, business}

      {:error, :business, changeset, _} ->
        {:error, changeset}
    end
  end

  def preload(%Business{} = business, preload) do
    Repo.preload(business, preload)
  end

  def verify(%Business{} = business, token) do
    case business
         |> Business.verify(token)
         |> Repo.update() do
      {:ok, business} -> {:ok, business}
      {:error, _} -> {:error, business}
    end
  end

  def get_member(%Business{} = business, user) do
    Repo.get_by(assoc(business, :members), user_id: user.id, confirmed: true)
  end

  @impl true
  def allow(user, action, business) do
    case get_member(business, user) do
      nil ->
        {:error, :no_access}

      %{role: role} ->
        if action in Business.can(role), do: {:ok}, else: {:error, :no_access}
    end
  end
end
