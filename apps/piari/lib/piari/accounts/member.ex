defmodule Piari.Accounts.Member do
  use Piari, :schema

  alias Piari.Accounts.User
  alias Piari.Accounts.Business
  alias Piari.Ecto.Atom

  schema "members" do
    field(:role, Atom, default: :member)
    field(:confirmed, :boolean, default: false)
    belongs_to(:user, User)
    belongs_to(:business, Business)

    timestamps()
  end
end
