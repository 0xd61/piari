defmodule Piari.Accounts.Auths.Email do
  use Auth.Authenticator,
    otp_app: :piari,
    adapter: Auth.Adapter.Native
end
