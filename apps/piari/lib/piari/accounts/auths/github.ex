defmodule Piari.Accounts.Auths.Github do
  use Auth.Authenticator,
    otp_app: :piari,
    adapter: Auth.Adapter.Github
end
