defmodule Piari.Accounts.User do
  use Piari, :schema
  alias Piari.Accounts.{Auth, Member, Key}
  alias Piari.Ecto.Atom
  alias Piari.Translations

  schema "users" do
    field(:email, :string)
    field(:name, :string)
    field(:locale, :string, default: "en")
    field(:timezone, :string, default: "America/Asuncion")
    has_many(:memberships, Member)
    has_many(:ownerships, Member, where: [role: :owner])
    has_many(:businesses, through: [:memberships, :business])
    has_many(:auths, Auth)
    has_many(:keys, Key)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :locale, :timezone])
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/)
    |> unique_constraint(:email)
    |> validate_language(:locale)
  end

  def update_changeset(user, attrs) do
    user
    |> changeset(attrs)
    |> delete_change(:email)
  end

  def update_email_changeset(user, email) do
    user
    |> changeset(%{email: email})
  end

  # Create mandatory Auth assoc on registration
  # On normal change, the Auth should not be changed.
  def registration_changeset(user, attrs) do
    user
    |> changeset(attrs)
    |> put_name()
    |> cast_assoc(:auths, with: &Auth.changeset/2, required: true)
  end

  defp put_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, _} ->
        changeset

      _ ->
        {_, email} = fetch_field(changeset, :email)

        changeset
        |> put_change(:name, name_from_email(email))
    end
  end

  defp name_from_email(email) do
    email |> String.split("@") |> hd()
  end

  defp validate_language(changeset, field, _options \\ []) do
    validate_change(changeset, field, fn _, lang ->
      case Translations.lang_is_valid?(lang) do
        true ->
          []

        false ->
          [
            {field,
             {"%{lang} is not in [%{languages}]",
              lang: lang, languages: Translations.list_languages() |> Enum.join(", ")}}
          ]
      end
    end)
  end
end
