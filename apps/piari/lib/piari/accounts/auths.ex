defmodule Piari.Accounts.Auths do
  @moduledoc """
  The Auths secondary context.
  """
  use Piari, :context
  alias Auth
  alias Piari.Accounts.Auth, as: A
  alias Piari.Accounts.{User, Keys}

  @doc """
  Creates an auth for a user.

  ## Examples

      iex> new(%{field: value}, %User{})
      {:ok, %Auth{}}

      iex> new(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def new(%User{} = user, attrs \\ %{}) do
    %A{}
    |> A.changeset(attrs)
    |> put_assoc(:user, user)
    |> Repo.insert()
  end

  @doc """
  Deletes a Auth.

  ## Examples

      iex> delete(auth)
      {:ok, %Auth{}}

      iex> delete(auth)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%A{} = auth) do
    Repo.delete(auth)
  end

  def get!(user, id) do
    Repo.get_by!(assoc(user, :auths), id: id)
  end

  def list(user) do
    user
    |> assoc(:auths)
    |> Repo.all()
  end

  def gen_key() do
    Auth.gen_key()
  end

  def key_auth(user_secret, usage_info) do
    # Database index lookup on the first part of the key and then
    # secure compare on the second part to avoid timing attacks
    app_secret = Application.get_env(:piari, :secret)

    <<first::binary-size(32), second::binary-size(32)>> =
      :crypto.hmac(:sha256, app_secret, user_secret)
      |> Base.encode16(case: :lower)

    result = Keys.get(first)

    case result do
      nil ->
        :error

      key ->
        if Piari.Utils.secure_check(key.secret_second, second) do
          if Keys.revoked?(key) do
            :revoked
          else
            Keys.update_last_use(key, usage_info)

            {:ok,
             %{
               key: key,
               user: key.user,
               source: :key
             }}
          end
        else
          :error
        end
    end
  end

  # Automatically generate functions for each auth provider
  def request_url(provider, opts \\ [])
  def authenticate(provider, params \\ [], opts \\ [])

  for provider <- Application.fetch_env!(:piari, :auth_provider) do
    shortname =
      provider
      |> Module.split()
      |> List.last()
      |> String.downcase()
      |> String.to_atom()

    def request_url(unquote(shortname), opts) do
      unquote(provider).authorize_url!(opts)
    end

    def authenticate(unquote(shortname), params, opts) do
      unquote(provider).authenticate(params, opts)
    end
  end
end
