defmodule Piari.Accounts.Key do
  use Piari, :schema
  alias Piari.Accounts.User

  schema "keys" do
    field :secret_first, :string
    field :secret_second, :string
    field :revoke_at, :utc_datetime_usec
    field :revoked_at, :utc_datetime_usec
    timestamps()

    embeds_one :last_use, Use, on_replace: :delete do
      field :used_at, :utc_datetime_usec
      field :user_agent, :string
      field :ip, :string
    end

    belongs_to :user, User
    # Only used after key creation to hold the user's key (not hashed)
    # the user key will never be retrievable after this
    field :user_secret, :string, virtual: true
  end

  def changeset(key, params) do
    cast(key, params, ~w()a)
    |> validate_required(~w()a)
    |> add_keys()
  end

  def build(user, params) do
    build_assoc(user, :keys)
    |> changeset(params)
  end

  def gen_key() do
    user_secret = Auth.gen_key()
    app_secret = Application.get_env(:piari, :secret)

    <<first::binary-size(32), second::binary-size(32)>> =
      :crypto.hmac(:sha256, app_secret, user_secret)
      |> Base.encode16(case: :lower)

    {user_secret, first, second}
  end

  def revoke(key, revoked_at \\ DateTime.utc_now()) do
    key
    |> change()
    |> put_change(:revoked_at, key.revoked_at || revoked_at)
    |> validate_required(:revoked_at)
  end

  def update_last_use(key, params) do
    usage =
      __MODULE__.Use
      |> struct(params)
      |> to_string(:ip)
      |> to_string(:user_agent)

    key
    |> change()
    |> put_embed(:last_use, usage)
  end

  defp to_string(%{ip: ip} = struct, :ip) when not is_nil(ip) do
    %{struct | ip: ip |> :inet.ntoa() |> to_string()}
  end

  defp to_string(%{user_agent: user_agent} = struct, :user_agent) when not is_nil(user_agent) do
    %{struct | user_agent: to_string(user_agent)}
  end

  defp to_string(struct, _), do: struct

  defp add_keys(changeset) do
    {user_secret, first, second} = gen_key()

    changeset
    |> put_change(:user_secret, user_secret)
    |> put_change(:secret_first, first)
    |> put_change(:secret_second, second)
  end
end
