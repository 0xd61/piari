defmodule Piari.Accounts.Auth do
  use Piari, :schema
  alias Piari.Accounts.User
  alias Piari.Ecto.Atom

  schema "auths" do
    field(:ext_uid, :string)
    field(:provider, Atom)
    belongs_to(:user, User)

    timestamps()
  end

  @doc false
  def changeset(auth, attrs) do
    auth
    |> cast(attrs, [:ext_uid, :provider])
    |> validate_required([:ext_uid, :provider])
    |> unique_constraint(:ext_uid)
  end
end
