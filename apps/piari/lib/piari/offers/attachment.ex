defmodule Piari.Offers.Attachment do
  use Piari, :schema
  alias Piari.Offers.Product

  schema "attachments" do
    field(:uri, :string)
    field(:size, :integer)
    field(:content_type, :string)
    field(:thumbnail, :boolean, default: false)
    field(:hash, :string)
    belongs_to(:product, Product)

    timestamps()
  end

  @doc false
  def changeset(attachment, attrs) do
    attachment
    |> cast(attrs, [
      :uri,
      :size,
      :content_type,
      :thumbnail
    ])
    |> validate_required([:uri])
    |> unique_constraint(:uri)
  end
end
