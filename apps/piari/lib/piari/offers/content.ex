defmodule Piari.Offers.Content do
  use Ecto.Schema
  import Ecto.Changeset
  use Piari.Translations, container: :translations

  # @dynamic_fields %{}

  embedded_schema do
    field(:title, :string)
    field(:description, :string)

    # for {field_name, type} <- @dynamic_fields do
    #   # Dynamically add fields to schema
    #   field(field_name, type)
    # end

    field(:translations, :map)
  end

  @doc false
  def changeset(detail, attrs) do
    detail
    |> cast(attrs, [
      :title,
      :description,
      :translations
      # | Map.keys(@dynamic_fields)
    ])
    |> validate_required([:title])
    |> validate_length(:title, max: 50)
    |> validate_length(:description, max: 200)
  end
end
