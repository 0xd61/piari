defmodule Piari.Offers.Product do
  use Piari, :schema
  use Piari.Offers.Product.Searchable
  alias Piari.Offers.{Product, Content, Attachment, Permalink}

  @primary_key {:id, Permalink, autogenerate: true}
  schema "products" do
    field(:quantity, :integer)
    field(:expires, :utc_datetime)
    field(:price, :float)
    field(:slug, :string)
    embeds_one(:content, Content)
    field(:accounts_business_id, :integer)
    has_one(:image, Attachment, where: [thumbnail: true])
    has_many(:attachments, Attachment)
    field(:draft, :boolean, default: true)

    timestamps()
  end

  @required ~w(quantity price)a

  @doc false
  def changeset(%Product{draft: true} = product, attrs) do
    product
    |> cast(attrs, [:quantity, :price])
    |> validate()
    |> cast_embed(:content, with: &Content.changeset/2, required: true)
    |> slugify_title()
  end

  def changeset(%Product{draft: false} = product, attrs) do
    product
    |> cast(attrs, [:quantity, :price])
    |> validate()
  end

  def publish(product, until, timezone \\ "Etc/UTC") do
    product
    |> change(%{draft: false})
    |> put_change(:expires, add_days_to_now(timezone, until))
    |> validate_future_date(:expires)
  end

  def unpublish(product) do
    product
    |> change(%{draft: true})
  end

  defp validate(changeset) do
    changeset
    |> validate_required(@required)
    |> validate_number(:quantity, greater_than_or_equal_to: 0)
    |> validate_number(:price, greater_than_or_equal_to: 0)
  end

  defp slugify_title(changeset) do
    with {:ok, content_changeset} <- fetch_change(changeset, :content),
         {:ok, new_title} <- fetch_change(content_changeset, :title) do
      put_change(changeset, :slug, slugify(new_title))
    else
      _ -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.replace(~r/[^\w-]+/u, "-")
    |> URI.encode_www_form()
    |> String.downcase()
  end

  defp add_days_to_now(timezone, days) do
    now =
      with {:ok, now} <- DateTime.now(timezone, Tzdata.TimeZoneDatabase),
           {:ok, utc} <- DateTime.shift_zone(now, "Etc/UTC") do
        utc
      else
        _ -> DateTime.utc_now()
      end

    now
    |> DateTime.add(days * 24 * 60 * 60, :second)
    |> DateTime.truncate(:second)
  end

  def build(business_id, attrs) do
    %Product{draft: true}
    |> changeset(attrs)
    |> put_change(:accounts_business_id, business_id)
  end

  def can(:owner), do: ["write", "publish", "read"]
  def can(:member), do: ["publish", "read"]
end
