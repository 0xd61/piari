defmodule Piari.Offers.Products do
  @moduledoc """
  The Products secondary context.
  """
  use Piari, :context
  alias Piari.Offers.Product
  alias Piari.Offers.Attachments
  alias Piari.Searches

  def new(%{id: business_id}, image, attrs) do
    multi =
      Multi.new()
      |> Multi.insert(:product, Product.build(business_id, attrs))
      |> Multi.run(:image, fn _repo, %{product: product} ->
        Attachments.new(product, image, %{thumbnail: true})
      end)

    case Repo.transaction(multi) do
      {:ok, %{product: product, image: image}} ->
        Searches.index(Searches.Offers, product)
        {:ok, product |> Repo.preload([:image, :attachments])}

      {:error, :product, changeset, _} ->
        {:error, changeset}
    end
  end

  def change(%Product{} = product) do
    product
    |> Product.changeset(%{})
  end

  def update(%Product{} = product, attrs) do
    changeset = Product.changeset(product, attrs)

    case Repo.update(changeset) do
      {:ok, product} ->
        Searches.index(Searches.Offers, product)
        {:ok, product |> Repo.preload([:image, :attachments])}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def get!(%{id: business_id}, id, preload \\ []) do
    Product
    |> Repo.get_by!(id: id, accounts_business_id: business_id)
    |> Repo.preload(preload)
  end

  def get_public!(id, preload \\ []) do
    from(p in Product,
      where: p.id == ^id,
      where: p.draft == false,
      where: p.expires >= ^DateTime.utc_now()
    )
    |> Repo.one!()
    |> Repo.preload(preload)
  end

  def list(%{id: id}, preload \\ []) do
    from(p in Product,
      where: p.accounts_business_id == ^id
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def list_public(%{id: id}, preload \\ []) do
    from(p in Product,
      where: p.accounts_business_id == ^id,
      where: p.draft == false,
      where: p.expires >= ^DateTime.utc_now()
    )
    |> Repo.all()
    |> Repo.preload(preload)
  end

  def delete(%Product{} = product) do
    multi =
      Multi.new()
      |> Multi.delete(:product, product)
      |> Multi.run(:search, fn _repo, _ ->
        Searches.delete(Searches.Offers, product)
        {:ok, product}
      end)

    case Repo.transaction(multi) do
      {:ok, %{product: product}} ->
        {:ok, product}

      {:error, :product, changeset, _} ->
        {:error, changeset}
    end
  end

  def delete_all(multi, %{id: id}) do
    query =
      from(p in Product,
        where: p.accounts_business_id == ^id
      )

    products = Repo.all(query)

    multi
    |> Multi.delete_all(:product, query)
    |> Multi.run(:search, fn _, _ ->
      products
      |> Enum.each(&Searches.delete(Searches.Offers, &1))

      {:ok, products}
    end)
  end

  def publish!(product, days, timezone \\ "Etc/UTC")

  def publish!(%Product{} = product, days, timezone) when is_integer(days) do
    product
    |> Product.publish(days, timezone)
    |> Repo.update!()
  end

  def publish!(%Product{} = product, days, timezone) do
    d = String.to_integer(days)
    publish!(product, d, timezone)
  end

  def unpublish!(%Product{} = product) do
    product
    |> Product.unpublish()
    |> Repo.update!()
  end

  def localize(%Product{content: content} = product, locale) do
    %{product | content: Piari.Translations.localize(content, locale)}
  end

  def allow(user, action, business, %Product{} = product) do
    cond do
      product.accounts_business_id !== business.id ->
        {:error, :no_access}

      member = Piari.Accounts.get_business_member(business, user) ->
        %{role: role} = member
        if action in Product.can(role), do: {:ok}, else: {:error, :no_access}

      true ->
        {:error, :no_access}
    end
  end

  def allow(user, action, business, nil) do
    case Piari.Accounts.get_business_member(business, user) do
      nil ->
        {:error, :no_access}

      %{role: role} ->
        if action in Product.can(role), do: {:ok}, else: {:error, :no_access}
    end
  end
end
