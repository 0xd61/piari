defmodule Piari.Offers.Attachments do
  import Ecto.Query, warn: false
  alias Piari.Repo
  alias Piari.Offers.Attachment

  def new(product, asset_base64, attrs \\ %{}) when is_binary(asset_base64) do
    case Store.put(asset_base64) do
      {:ok, upload} ->
        attrs =
          upload
          |> Map.from_struct()
          |> Map.merge(attrs)

        %Attachment{}
        |> Attachment.changeset(attrs)
        |> Ecto.Changeset.put_assoc(:product, product)
        |> Repo.insert()

      {:error, reason} ->
        {:error, reason}
    end
  end

  def get!(id), do: Repo.get!(Attachment, id)

  def delete(%Attachment{} = attachment) do
    Store.delete(attachment.uri)
    Repo.delete(attachment)
  end
end
