defmodule Piari.Offers.Product.Searchable do
  defmacro __using__(_) do
    quote do
      @searchable_fields [:title, :description]
      @behaviour Search.Searchable
      alias Piari.Offers.{Products, Product}
      import Ecto.Query, warn: false

      def query(ids) do
        from(p in Piari.Offers.Product,
          where: p.id in ^ids,
          where: p.expires >= ^DateTime.utc_now(),
          where: p.draft == false,
          select: p
        )
        |> preload([:image, :attachments])
        |> Piari.Repo.all()
      end

      def to_index(%{id: id, content: content}) do
        content
        |> Piari.Translations.list_translations()
        |> Enum.flat_map(fn {locale, translation} ->
          translation
          |> Enum.map(fn {key, value} ->
            {key |> add_suffix(locale), value}
          end)
        end)
        |> Enum.into(
          content
          |> Map.take(@searchable_fields)
          |> put_in([:id], id)
        )
      end

      defp add_suffix(key, nil) when is_atom(key) do
        key
      end

      defp add_suffix(key, suffix) when is_atom(key) and is_binary(suffix) do
        key |> to_string |> add_suffix(suffix)
      end

      defp add_suffix(key, suffix) when is_binary(suffix) do
        key
        |> Kernel.<>("_")
        |> Kernel.<>(suffix)
        |> String.to_atom()
      end

      def id(product) do
        product.id
      end

      def type() do
        Product
      end

      def stream() do
        Piari.Repo.stream(Product)
      end

      def transaction(fun) do
        {:ok, result} = Piari.Repo.transaction(fun, timeout: :infinity)
        result
      end
    end
  end
end
