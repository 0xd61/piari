defmodule Piari.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add(:quantity, :integer, null: false)
      add(:expires, :utc_datetime)
      add(:price, :float, null: false)

      add(
        :accounts_business_id,
        references(:businesses, on_delete: :delete_all),
        null: false
      )

      add(:slug, :string)
      add(:draft, :boolean, default: true)
      add(:content, :map, null: false)

      timestamps()
    end

    create(index(:products, [:accounts_business_id]))
  end
end
