defmodule Piari.Repo.Migrations.CreateBusinesses do
  use Ecto.Migration

  def change do
    create table(:businesses) do
      add(:name, :string, null: false)
      add(:phone, :string, null: false)
      add(:verified, :boolean, null: false, default: false)
      add(:verify_token, :string)
      add(:slug, :string, null: false)

      timestamps()
    end

    create(unique_index(:businesses, [:slug]))
    create(unique_index(:businesses, [:phone]))
  end
end
