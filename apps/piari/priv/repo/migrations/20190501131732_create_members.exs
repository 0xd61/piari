defmodule Piari.Repo.Migrations.CreateMembers do
  use Ecto.Migration

  def change do
    create table(:members) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:business_id, references(:businesses, on_delete: :delete_all), null: false)
      add(:role, :string)
      add(:confirmed, :boolean, default: false)

      timestamps()
    end

    create(unique_index(:members, [:business_id, :user_id]))
  end
end
