defmodule Piari.Repo.Migrations.CreateAttachments do
  use Ecto.Migration

  def change do
    create table(:attachments) do
      add(:uri, :string, null: false)
      add(:size, :bigint)
      add(:content_type, :string)
      add(:hash, :string, size: 64)
      add(:thumbnail, :boolean, default: false)
      add(:product_id, references(:products, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(unique_index(:attachments, [:uri]))
  end
end
