defmodule Piari.Repo.Migrations.CreateAuths do
  use Ecto.Migration

  def change do
    create table(:auths) do
      add(:ext_uid, :string, null: false)
      add(:provider, :string, null: false)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(unique_index(:auths, [:ext_uid]))
    create(index(:auths, [:user_id]))
  end
end
