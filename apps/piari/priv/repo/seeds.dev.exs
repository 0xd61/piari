# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Piari.Repo.insert!(%Piari.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

user = Piari.Repo.insert!(%Piari.Accounts.User{email: "someone@example.com"})

business =
  Piari.Repo.insert!(%Piari.Accounts.Business{
    name: "Some Business",
    phone: "0123456789",
    slug: "some+business"
  })

_member =
  Piari.Repo.insert!(%Piari.Accounts.Member{
    business_id: business.id,
    user_id: user.id,
    role: :owner,
    confirmed: true
  })

1..100
|> Enum.map(fn id ->
  Piari.Repo.insert!(%Piari.Offers.Product{
    id: id,
    quantity: 2,
    expires: ~U[9999-07-18 20:40:21Z],
    draft: false,
    accounts_business_id: business.id,
    slug: "#{id}-Example Product #{id}",
    price: 5.1,
    content: %{
      title: "Example Product #{id}",
      description:
        "The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar,.",
      translations: %{
        "es" => %{
          title: "Producto De Ejemplo #{id}",
          description:
            "Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pro"
        },
        "de" => %{
          title: "Beispiel Produkt #{id}",
          description:
            "Überall dieselbe alte Leier. Das Layout ist fertig, der Text lässt auf sich warten. Damit das Layout nun nicht nackt im Raume steht und sich klein und leer vorkommt, springe ich ein: der Blindtext."
        }
      }
    }
  })
end)
