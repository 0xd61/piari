defmodule Piari.Searches.OffersTest do
  use Piari.DataCase
  alias Piari.Searches.Offers
  alias Search.Query

  test "build/1 creates index of a searchable" do
    assert %Query{on: :offers} = Offers.build()
  end

  test "defaults/2 applies index defaults" do
    query = Offers.build()
    assert %Query{fields: ["description*", {"title*", 3}]} = Offers.defaults(query)
  end
end
