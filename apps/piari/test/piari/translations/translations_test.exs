defmodule Piari.TranslationsTest do
  use Piari.DataCase

  alias Piari.Translations

  @struct %Piari.Offers.Content{
    title: "Test Title",
    description: "Test Description",
    translations: %{
      "de" => %{
        "title" => "Test Titel",
        "description" => "Test Bescheibung"
      },
      "es" => %{
        "title" => "Título de Prueba",
        "description" => "Descripción de la Prueba"
      }
    }
  }

  describe "translations" do
    test "list_translations/1 lists all translations" do
      assert %{
      "de" => %{
        "title" => "Test Titel",
        "description" => "Test Bescheibung"
      },
      "es" => %{
        "title" => "Título de Prueba",
        "description" => "Descripción de la Prueba"
      }

             } = Translations.list_translations(@struct)
    end

    test "localize/3 returns localized field" do
      assert Translations.localize(@struct, "de", :title) == "Test Titel"
      assert Translations.localize(@struct, "unknown", :title) == "Test Title"
      assert Translations.localize(@struct, "unknown", :title, nil) == nil
      assert Translations.localize(@struct, "unknown", :title, "abc") == "abc"
    end
  end
end
