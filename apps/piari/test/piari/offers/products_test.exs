defmodule Piari.Offers.ProductsTest do
  use Piari.DataCase, async: true

  describe "products" do
    alias Piari.Offers.{Products, Product}

    setup [:business, :business_product, :product]

    @image "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="

    @valid_attrs %{
      quantity: 5,
      price: 25_000.0,
      draft: true,
      content: %{
        title: "Example Title",
        description: "Example Description"
      }
    }
    @update_attrs %{
      quantity: 2,
      price: 10_000.0,
      content: %{
        title: "New Title",
        description: "New Description"
      }
    }
    @invalid_attrs %{quantity: -5, expires: nil, content: %{}}

    test "get!/1 returns the product with given id", %{
      business: business,
      business_product: product
    } do
      assert Products.get!(business, product.id) == product
    end

    test "get!/1 raises if no product was found", %{product: product, business: business} do
      assert_raise Ecto.NoResultsError, fn ->
        Products.get!(business, product.id) == product
      end
    end

    test "new/2 with valid data creates an product", %{business: business} do
      assert {:ok, %Product{} = product} = Products.new(business, @image, @valid_attrs)

      assert product.quantity == 5

      assert product.content.title == "Example Title"
      assert product.content.description == "Example Description"
    end

    test "new/2 with invalid data returns error changeset", %{business: business} do
      assert {:error, %Ecto.Changeset{}} = Products.new(business, @image, @invalid_attrs)
    end

    test "update/2 with valid data updates the product", %{business_product: product} do
      product = %Product{product | draft: true}
      attrs = put_in(@update_attrs, [:content, :id], product.content.id)
      assert {:ok, %Product{} = product} = Products.update(product, attrs)

      assert product.price == 10_000.0
      assert product.quantity == 2

      assert product.content.title == "New Title"
    end

    test "update/2 with published product does only update quantity and price", %{
      business_product: product
    } do
      attrs = put_in(@update_attrs, [:content, :id], product.content.id)
      assert {:ok, %Product{} = product} = Products.update(product, attrs)

      assert product.price == 10_000.0
      assert product.quantity == 2

      refute product.content.title == "New Title"
    end

    test "update/2 with invalid data returns error changeset", %{
      business: business,
      business_product: product
    } do
      product = product
      attrs = put_in(@invalid_attrs, [:content, :id], product.content.id)
      assert {:error, %Ecto.Changeset{}} = Products.update(product, attrs)
      assert Products.get!(business, product.id) == product
    end

    test "delete/1 deletes the product", %{business_product: product, business: business} do
      assert {:ok, %Product{}} = Products.delete(product)
      assert_raise Ecto.NoResultsError, fn -> Products.get!(business, product.id) end
    end

    test "delete_all/2 deletes all products for a business", %{
      business_product: product,
      business: business
    } do
      assert {:ok, _} =
               Ecto.Multi.new()
               |> Products.delete_all(business)
               |> Piari.Repo.transaction()

      assert_raise Ecto.NoResultsError, fn -> Products.get!(business, product.id) end
    end

    test "change/2 returns an product changeset", %{business_product: product} do
      assert %Ecto.Changeset{} = Products.change(product)
    end

    test "list/1 returns all offers for a business", %{
      business_product: product,
      business_product_draft: draft,
      business: business
    } do
      assert Products.list(business) == [product, draft]
    end

    test "list_public/1 returns published offers for a business", %{
      business_product: product,
      business: business
    } do
      assert Products.list_public(business) == [product]
    end

    test "publish!/3 publishes a product for a number of days", %{business_product_draft: product} do
      now = DateTime.utc_now() |> DateTime.truncate(:second)
      product = Products.publish!(product, 0)
      assert product.draft == false
      assert product.expires == now
    end

    test "unpublish!/1 unpublishes a product", %{business_product: product} do
      assert %{draft: true} = Products.unpublish!(product)
    end

    test "allow/3 returns ok if user is authorized" do
      user = user_fixture()
      business = business_fixture()
      _member = member_fixture(user, business, %{role: :owner, confirmed: true})
      product = product_fixture(business, %{})

      assert {:ok} = Products.allow(user, "write", business, %Product{} = product)
    end

    test "allow/3 returns error if not authorized" do
      user = user_fixture()
      business = business_fixture()
      product = product_fixture()

      assert {:error, :no_access} = Products.allow(user, "write", business, %Product{} = product)
    end
  end

  def business(_) do
    business = business_fixture()

    {:ok, %{business: business}}
  end

  def business_product(%{business: business}) do
    product = product_fixture(business, %{})
    draft = product_fixture(business, %{draft: true})
    {:ok, %{business_product: product, business_product_draft: draft}}
  end

  def product(_) do
    product = product_fixture()

    {:ok, %{product: product}}
  end
end
