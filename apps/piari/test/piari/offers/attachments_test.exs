defmodule Piari.Offers.AttachmentsTest do
  use Piari.DataCase, async: true
  alias Piari.Offers.{Attachments, Attachment}

  describe "attachments" do
    setup [:product]

    @valid_attrs "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="
    @invalid_attrs "aW52YWxpZF9kYXRhCg=="

    test "new/2 creates a new attachment", %{product: product} do
      assert {:ok, %Attachment{} = att} = Attachments.new(product, @valid_attrs)

      assert att.product_id == product.id
      assert att.uri != nil
    end

    test "create_attachment/2 with invalid data raises", %{product: product} do
      assert {:error, _} = Attachments.new(product, @invalid_attrs)
    end

    test "delete_attachment/1 deletes existing attachment", %{product: product} do
      {:ok, att} = Attachments.new(product, @valid_attrs)

      assert {:ok, %Attachment{}} = Attachments.delete(att)
      assert_raise Ecto.NoResultsError, fn -> Attachments.get!(att.id) end
    end
  end

  def product(_) do
    product = product_fixture()
    {:ok, %{product: product}}
  end
end
