defmodule Piari.OffersTest do
  use Piari.DataCase, async: true

  alias Piari.Offers

  describe "offers" do
    setup [:business, :business_product, :product]

    @image "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="

    @valid_attrs %{
      quantity: 5,
      price: 25000.0,
      content: %{
        title: "Example Title",
        description: "Example Description"
      }
    }

    test "create_product/2 with valid data creates a product with associated business account", %{
      business: business
    } do
      assert {:ok, product} =
               business
               |> Offers.create_product(@image, @valid_attrs)

      assert product.quantity == 5

      assert product.accounts_business_id == business.id
    end

    test "list_products/1 returns all products of a business", %{
      business: business,
      business_product: product,
      business_product_draft: draft
    } do
      assert Offers.list_products(business) == [product, draft]
    end

    test "list_public_products/1 returns all products of a business", %{
      business: business,
      business_product: product
    } do
      assert Offers.list_public_products(business) == [product]
    end

    test "get_business_product!/2 returns the product of a business", %{
      business: business,
      business_product: product
    } do
      assert Offers.get_business_product!(business, product.id) == product
    end

    test "get_business_product!/2 returns no results error of the product does not belong to the business",
         %{business: business, product: product} do
      assert_raise Ecto.NoResultsError, fn ->
        Offers.get_business_product!(business, product.id)
      end
    end
  end

  def business(_) do
    business = business_fixture()
    {:ok, %{business: business}}
  end

  def business_product(%{business: business}) do
    product = product_fixture(business, %{})
    draft = product_fixture(business, %{draft: true})
    {:ok, %{business_product: product, business_product_draft: draft}}
  end

  def product(_) do
    product = product_fixture()
    {:ok, %{product: product}}
  end
end
