defmodule Piari.Accounts.UsersTest do
  use Piari.DataCase

  alias Piari.TestHelpers.Accounts.Auths.FakeAuth
  alias Piari.Accounts.{Users, User}

  @valid_attrs %{email: "someone@example.com", name: "some name", locale: "en"}
  @update_attrs %{
    email: "another@example.com",
    name: "some updated name",
    locale: "es"
  }
  @invalid_attrs %{email: "invalid email", name: nil}

  describe "CRUD user" do
    setup [:user, :user_business]

    test "list/0 returns all users", %{user: user} do
      assert Users.list() == [user]
    end

    test "get!/1 returns the user with given id", %{user: user} do
      assert Users.get!(user.id) == user
    end

    test "get_by_email!/1 returns the user with given email", %{user: user} do
      assert Users.get_by_email!(user.email) == user
    end

    test "get_by_email/1 returns the user with given email", %{user: user} do
      assert Users.get_by_email(user.email) == user
    end

    test "get_by_email!/1 raises Ecto NoResultsError if user with given email not found" do
      assert_raise Ecto.NoResultsError, fn ->
        Users.get_by_email!("email@doesnotexist.com")
      end
    end

    test "get_by_email/1 returns error if user with given email not found" do
      assert Users.get_by_email("email@doesnotexist.com") == nil
    end

    test "new/1 with valid data creates a user with authentication data" do
      auth = %{ext_uid: "only_for_testing", provider: FakeAuth}
      attrs = Map.put_new(@valid_attrs, :auths, [auth])
      assert {:ok, %User{} = user} = Users.new(attrs)
      assert user.locale == "en"
      assert user.email == "someone@example.com"
      assert user.name == "some name"
      Enum.each(user.auths, fn item -> assert item.ext_uid == auth.ext_uid end)
    end

    test "new/1 with invalid data error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.new(@invalid_attrs)
    end

    test "update/2 with valid data updates the user but ignores email", %{user: user} do
      assert {:ok, %User{} = user_new} = Users.update(user, @update_attrs)
      assert user_new.locale == "es"
      assert user_new.email == user.email
      assert user_new.name == "some updated name"
    end

    test "update/2 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Users.update(user, @invalid_attrs)
      assert user == Users.get!(user.id)
    end

    test "update_email/2 with valid data updates the users email address", %{user: user} do
      assert {:ok, %User{} = user_new} = Users.update_email(user, "some_mail@example.com")

      assert user_new.email == "some_mail@example.com"
    end

    test "delete/1 deletes the user" do
      # does not have a membership
      user = user_fixture()
      assert {:ok, %User{}} = Users.delete(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get!(user.id) end
    end

    test "delete/1 returns error if user is business owner", %{user: user} do
      assert {:error, :is_owner} = Users.delete(user)
    end

    test "change/1 returns a user changeset", %{user: user} do
      assert %Ecto.Changeset{} = Users.change(user)
    end

    test "has_email?/2 returns true if email belongs to user" do
      user = user_fixture(%{email: "test@someone.com"})

      assert user |> Users.has_email?("test@someone.com") == true
      assert user |> Users.has_email?("failed@someone.com") == false
    end

    test "get_by_auth_id/2 returns user by auth id", %{user: user, auth: auth} do
      assert Users.get_by_auth_id(auth.ext_uid) == user
      assert Users.get_by_auth_id(auth.id) == user
    end

    test "allow/3 returns ok, if user can access user", %{user: user} do
      assert {:ok} = Users.allow(user, "write", user)
    end

    test "allow/3 returns error, if user cannot access user", %{user: user} do
      other_user = user_fixture()
      assert {:error, :no_access} = Users.allow(user, "write", other_user)
    end
  end

  describe "user with membership" do
    setup [:user, :user_business, :business]

    test "is_owner/2 returns true if user owns the business", %{
      user: user,
      user_business: business
    } do
      user2 = user_fixture()
      _member2 = member_fixture(user2, business, %{role: :member, confirmed: true})
      user3 = user_fixture()

      assert Users.is_owner?(user, business) == true
      assert Users.is_owner?(user2, business) == false
      assert Users.is_owner?(user3, business) == false
    end

    test "is_member? returns true if the user is owner or member of the business", %{
      user: user,
      user_business: business
    } do
      user2 = user_fixture()
      _member2 = member_fixture(user2, business, %{role: :member, confirmed: true})
      user3 = user_fixture()

      assert Users.is_member?(user, business) == true
      assert Users.is_member?(user2, business) == true
      assert Users.is_member?(user3, business) == false
    end
  end

  defp user(_) do
    user = user_fixture()
    auth = auth_fixture(user, %{})

    {:ok, user: user, auth: auth}
  end

  defp user_business(%{user: user}) do
    business = business_fixture()
    _member = member_fixture(user, business, %{role: :owner, confirmed: true})

    {:ok, user_business: business}
  end

  defp business(_struct) do
    user = user_fixture()
    business = business_fixture()
    _member = member_fixture(user, business, %{role: :owner, confirmed: true})

    {:ok, business: business}
  end
end
