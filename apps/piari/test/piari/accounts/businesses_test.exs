defmodule Piari.Accounts.BusinessesTest do
  use Piari.DataCase

  alias Piari.Accounts.{Businesses, Business}
  @valid_attrs %{name: "some náme", phone: "0983227845"}
  @update_attrs %{
    name: "some updated name",
    phone: "0684852654"
  }
  @invalid_attrs %{name: nil, phone: nil}

  describe "CRUD business" do
    setup [:user, :business, :product]

    test "get!/1 returns the business with given id", %{business: business} do
      assert Businesses.get!(business.id) == business
    end

    test "get!/1 returns the business with given slug", %{business: business} do
      assert Businesses.get!(business.slug) == business
    end

    test "new/2 with valid data creates an business with url-safe slug", %{user: user} do
      assert {:ok, %Business{} = business} = Businesses.new(user, @valid_attrs)

      assert business.name == "some náme"
      assert business.phone == "0983227845"
      assert business.slug == "some-n%c3%a1me"
    end

    test "new/2 with explicit slug does not overwrite slug", %{user: user} do
      attrs = Map.put(@valid_attrs, :slug, "testslug")

      assert {:ok, %Business{} = business} = Businesses.new(user, attrs)

      assert business.slug == "testslug"
    end

    test "new/2 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Businesses.new(user, @invalid_attrs)
    end

    test "new/2 adds a member as owner to the business", %{user: user} do
      assert {:ok, %Business{} = business} = Businesses.new(user, @valid_attrs)

      with_members = business |> Piari.Repo.preload(:members)

      assert member = hd(with_members.members)
      assert member.role == :owner
      assert member.confirmed == true
      assert member.user_id == user.id
      assert member.business_id == business.id
    end

    test "update/2 with valid data updates the business", %{business: business} do
      assert {:ok, %Business{} = business} = Businesses.update(business, @update_attrs)

      assert business.name == "some updated name"
      assert business.phone == "0684852654"
    end

    test "update/2 with invalid data returns error changeset", %{business: business} do
      assert {:error, %Ecto.Changeset{}} = Businesses.update(business, @invalid_attrs)
      assert business == Businesses.get!(business.id)
    end

    test "delete/1 deletes the business and business products", %{
      business: business,
      product: product
    } do
      assert Piari.Offers.get_business_product!(business, product.id) === product
      assert {:ok, %Business{}} = Businesses.delete(business)
      assert_raise Ecto.NoResultsError, fn -> Businesses.get!(business.id) end

      assert_raise Ecto.NoResultsError, fn ->
        Piari.Offers.get_business_product!(business, product.id)
      end
    end

    test "change/1 returns a business changeset", %{business: business} do
      assert %Ecto.Changeset{} = Businesses.change(business)
    end

    test "verify/2 checks verification token and verifies business", %{
      business: business
    } do
      assert business.verified === false

      assert {:ok, %Business{} = business_new} =
               Businesses.verify(business, business.verify_token)

      assert business_new.verified === true
    end
  end

  describe "business with members" do
    setup [:user, :business]

    test "add_member/3 adds a new member to a business", %{business: business} do
      user = user_fixture()
      business = business |> Piari.Repo.preload(:members)

      assert {:ok, %Piari.Accounts.Member{} = member} =
               Businesses.add_member(business, user, %{role: :member})

      assert member.role == :member
      assert member.user_id == user.id
      assert member.business_id == business.id

      business_new = business.id |> Businesses.get!() |> Piari.Repo.preload(:members)
      assert business_new.members |> length == 2
    end

    test "confirm_member/3 confirms a member", %{business: business} do
      assert %{confirmed: false} =
               member =
               member_fixture(user_fixture(), business, %{role: :owner, confirmed: false})

      assert {:ok, %{confirmed: true}} = Businesses.confirm_member(member)
    end

    test "remove_member/2 removes a member from a business", %{business: business, user: _member} do
      user = user_fixture()
      {:ok, _member} = Businesses.add_member(business, user, %{role: :member})
      business_new = business.id |> Businesses.get!(:members)
      assert business_new.members |> length == 2

      assert {:ok, _member} = Businesses.remove_member(business_new, user)

      business_new = business.id |> Businesses.get!(:members)

      assert business_new.members |> length == 1

      assert_raise Ecto.NoResultsError, fn ->
        Businesses.get_by_user!(user, business.id)
      end
    end

    test "remove_member/2 returns error if only 1 member is left", %{
      business: business,
      user: member
    } do
      assert {:error, :last_member} = Businesses.remove_member(business, member)
    end

    test "get_member/2 returns business member", %{business: business, user: user} do
      member = Businesses.get_member(business, user)
      assert member.business_id == business.id
      assert member.user_id == user.id
    end

    test "get_by_user!/2 returns business by user and id", %{user: user, business: business} do
      assert Businesses.get_by_user!(user, business.id) == business
    end

    test "get_by_user!/2 returns business by user and slug", %{user: user, business: business} do
      assert Businesses.get_by_user!(user, business.slug) == business
    end

    test "get_by_user!/2 raises error if user does not belong to business", %{
      business: business
    } do
      user = user_fixture()

      assert_raise Ecto.NoResultsError, fn ->
        Businesses.get_by_user!(user, business.id)
      end
    end

    test "preload/2 preloads resources", %{business: business} do
      assert %{members: %Ecto.Association.NotLoaded{}} = business
      business = Businesses.preload(business, :members)
      assert %{members: [_]} = business
    end

    test "allow/3 returns ok, if the users action is authorized", %{
      user: user,
      business: business
    } do
      assert {:ok} = Businesses.allow(user, "write", business)
    end

    test "allow/3 returns error, if the users action is not authorized", %{
      business: business
    } do
      user = user_fixture()

      assert {:error, :no_access} = Businesses.allow(user, "write", business)
    end
  end

  defp user(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  defp business(%{user: user}) do
    business = business_fixture()
    _member = member_fixture(user, business, %{role: :owner, confirmed: true})

    {:ok, business: business}
  end

  defp product(%{business: business}) do
    product = product_fixture(business, %{})
    {:ok, product: product}
  end
end
