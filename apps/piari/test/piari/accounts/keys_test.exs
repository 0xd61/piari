defmodule Piari.Accounts.KeysTest do
  use Piari.DataCase, async: true

  alias Piari.Accounts.Keys

  setup [:user]

  test "create key and get", %{user: user} do
    assert {:ok, key} = Keys.new(user)
    assert Keys.get(key.secret_first).user_id == user.id
  end

  test "revoke key", %{user: user} do
    assert {:ok, key} = Keys.new(user)
    assert Keys.revoked?(key) === false
    assert {:ok, key} = Keys.revoke(key)
    assert Keys.revoked?(key) === true
  end

  test "update_last_use", %{user: user} do
    assert {:ok, key} = Keys.new(user)

    timestamp = DateTime.utc_now()

    usage_info = %{
      used_at: timestamp,
      user_agent: ["Chrome"],
      ip: {127, 0, 0, 1}
    }

    key = Keys.update_last_use(key, usage_info)

    assert key.last_use.used_at == timestamp
    assert key.last_use.user_agent == "Chrome"
    assert key.last_use.ip == "127.0.0.1"
  end

  defp user(_) do
    user = user_fixture()

    {:ok, user: user}
  end
end
