defmodule Piari.Accounts.AuthsTest do
  use Piari.DataCase
  alias Piari.Accounts.{Auths, Auth, Key}

  alias Piari.TestHelpers.Accounts.Auths.FakeAdapter

  describe "auths" do
    setup [:user, :auth]

    @valid_attrs %{ext_uid: "some ext_uid", provider: :fakeauth}
    @invalid_attrs %{ext_uid: nil, provider: nil}

    test "new/2 with valid data creates a auth", %{user: user} do
      assert {:ok, %Auth{} = auth} = Auths.new(user, @valid_attrs)
      assert auth.ext_uid == "some ext_uid"
      assert auth.provider == :fakeauth
    end

    test "new/2 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Auths.new(user, @invalid_attrs)
    end

    test "delete/1 deletes the auth", %{user: user, auth: auth} do
      assert {:ok, %Auth{}} = Auths.delete(auth)

      assert_raise Ecto.NoResultsError, fn ->
        Auths.get!(user, auth.id)
      end
    end

    test "get!/2 returns auth by user", %{user: user, auth: auth} do
      assert Auths.get!(user, auth.id) == auth
    end

    test "list/1 returns all auths of a user", %{user: user, auth: auth} do
      assert Auths.list(user) == [auth]
    end

    test "request_url/2 creates a login request" do
      url =
        Auths.request_url(:fakeauth,
          redirect_uri: FakeAdapter.redirect_uri()
        )

      assert url =~ FakeAdapter.redirect_uri()
    end

    test "authenticate/3 user with a valid token" do
      assert {:ok, %{email: _, id: _}} =
               Auths.authenticate(:fakeauth, token: FakeAdapter.auth_token())
    end

    test "authenticate/3 user with invalid auth token" do
      assert {:error, :not_authenticated} = Auths.authenticate(:fakeauth, token: "invalid")
    end
  end

  describe "key_auth/2" do
    setup [:user, :key]

    test "authorizes correct key", %{user: user, key: key} do
      assert {:ok, %{user: auth_user, key: auth_key, source: :key}} =
               Auths.key_auth(key.user_secret, %{})

      assert auth_key.id == key.id
      assert auth_user.id == user.id
    end

    test "stores key usage information when used", %{key: key} do
      timestamp = DateTime.utc_now()

      usage_info = %{
        used_at: timestamp,
        user_agent: ["Chrome"],
        ip: {127, 0, 0, 1}
      }

      {:ok, _} = Auths.key_auth(key.user_secret, usage_info)

      key = Repo.get(Key, key.id)
      assert key.last_use.used_at == timestamp
      assert key.last_use.user_agent == "Chrome"
      assert key.last_use.ip == "127.0.0.1"
    end

    test "does not authorize wrong key" do
      assert Auths.key_auth("0123456789abcdef", %{}) == :error
    end

    test "does not authorize revoked key", %{key: key} do
      assert Piari.Accounts.Keys.revoke(key)
      assert Auths.key_auth(key.user_secret, %{}) == :revoked
    end
  end

  defp user(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  defp key(%{user: user}) do
    {:ok, key} = Piari.Accounts.Keys.new(user)

    {:ok, key: key}
  end

  defp auth(%{user: user}) do
    auth = auth_fixture(user, %{})

    {:ok, auth: auth}
  end
end
