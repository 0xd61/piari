defmodule Piari.AccountsTest do
  use Piari.DataCase

  alias Piari.Accounts
  alias Piari.TestHelpers.Accounts.Auths.FakeAdapter

  @email FakeAdapter.user_email()
  @ext_uid FakeAdapter.prefix_encode_id(FakeAdapter.user_email())

  describe "user subcontext" do
    setup [:user, :user_business]

    test "user_is_business_owner?/2 returns true if user owns business", %{
      user: user,
      user_business: business
    } do
      user2 = user_fixture()
      _member2 = member_fixture(user2, business, %{role: :member, confirmed: true})
      user3 = user_fixture()

      assert Accounts.user_is_business_owner?(user, business) == true
      assert Accounts.user_is_business_owner?(user2, business) == false
      assert Accounts.user_is_business_owner?(user3, business) == false
    end

    test "user_is_business_member?/2 returns true if user is a business member", %{
      user: user,
      user_business: business
    } do
      user2 = user_fixture()
      _member2 = member_fixture(user2, business, %{role: :member, confirmed: true})

      user3 = user_fixture()

      assert Accounts.user_is_business_member?(user, business) == true
      assert Accounts.user_is_business_member?(user2, business) == true
      assert Accounts.user_is_business_member?(user3, business) == false
    end

    test "get_or_register_user/3 existing user", %{user: user} do
      assert {:ok, ^user} = Accounts.get_or_register_user(@ext_uid, @email, :fakeauth)
    end

    test "get_or_register_user/3 - register and login new user" do
      assert {:ok, :new, user} =
               Accounts.get_or_register_user("some_uuid", "new@email.com", :fakeauth)

      assert user.email == "new@email.com"
      user = user |> Piari.Repo.preload(:auths)
      Enum.each(user.auths, fn %{ext_uid: id} -> assert id == "some_uuid" end)
    end

    test "get_or_register_user/3 - link existing user with new auth", %{user: user} do
      assert {:ok, :assigned, ^user} = Accounts.get_or_register_user("new_uuid", @email, FakeAuth)
    end

    test "auth_url/2 creates a login request" do
      url =
        Accounts.auth_url(:fakeauth,
          redirect_uri: FakeAdapter.redirect_uri()
        )

      assert url =~ FakeAdapter.redirect_uri()
    end

    test "authenticate/3 user with a valid token" do
      assert {:ok, %{email: _, id: _}} =
               Accounts.authenticate(:fakeauth, token: FakeAdapter.auth_token())
    end

    test "authenticate/3 user with invalid auth token" do
      assert {:error, :not_authenticated} = Accounts.authenticate(:fakeauth, token: "invalid")
    end
  end

  describe "business subcontext" do
    setup [:user, :user_business]

    test "create_business/2 creates and returns a business", %{user: user} do
      assert {:ok, %Accounts.Business{} = business} =
               Accounts.create_business(user, %{name: "testname", phone: "0123456789"})

      business = Piari.Repo.preload(business, members: [:user])
      assert business.name == "testname"
      assert business.phone == "0123456789"
      member = business.members |> hd()
      assert member.user == user
    end

    test "create_business/2 with invalid data returns a changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_business(user, %{})
    end

    test "list_user_businesses/1 returns businesses belonging to a user", %{
      user: user,
      user_business: business
    } do
      assert Accounts.list_user_businesses(user) == [business]
    end

    test "get_user_business!/2 returns a user business by id", %{
      user: user,
      user_business: business
    } do
      assert Accounts.get_user_business!(user, business.id) == business
    end

    test "get_user_business!/2 raises an error if the business does not belong to a user", %{
      user: user
    } do
      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_user_business!(user, 1337)
      end
    end

    test "add_member_to_business/3 assocs a user with a business", %{
      user_business: business
    } do
      user2 = user_fixture()
      assert {:ok, member} = Accounts.add_member_to_business(business, user2)
      assert member.business_id == business.id
      assert member.user_id == user2.id
      assert member.role == :member
    end

    test "remove_member_from_business/2 deletes the assoc of a user with a business", %{
      user_business: business
    } do
      user2 = user_fixture()
      _member = member_fixture(user2, business)

      # reload business
      business = business.id |> Accounts.get_business!(:members)
      assert business.members |> length == 2
      assert {:ok, member} = Accounts.remove_member_from_business(business, user2)

      # reload business
      business = business.id |> Accounts.get_business!(:members)
      assert business.members |> length == 1
      assert member.user_id == user2.id
    end
  end

  defp user(_) do
    user = user_fixture(%{email: @email})
    _auth = auth_fixture(user, %{ext_uid: @ext_uid, provider: :fakeauth})

    {:ok, user: user}
  end

  defp user_business(%{user: user}) do
    business = business_fixture()
    _member = member_fixture(user, business, %{role: :owner, confirmed: true})

    {:ok, user_business: business}
  end
end
