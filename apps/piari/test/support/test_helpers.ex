defmodule Piari.TestHelpers do
  def clear_associations(%{__struct__: struct} = schema) do
    struct.__schema__(:associations)
    |> Enum.reduce(schema, fn association, schema ->
      %{schema | association => build_not_loaded(struct, association)}
    end)
  end

  defp build_not_loaded(struct, association) do
    %{
      cardinality: cardinality,
      field: field,
      owner: owner
    } = struct.__schema__(:association, association)

    %Ecto.Association.NotLoaded{
      __cardinality__: cardinality,
      __field__: field,
      __owner__: owner
    }
  end

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"

    user = %Piari.Accounts.User{
      email: attrs[:email] || "#{username}@example.com",
      name: attrs[:name] || "#{username}"
    }

    {:ok, user} = Piari.Repo.insert(user)

    user
  end

  def auth_fixture(attrs \\ %{}) do
    auth_fixture(user_fixture(), attrs)
  end

  def auth_fixture(%Piari.Accounts.User{} = user, attrs) do
    uid = "uid#{System.unique_integer([:positive])}"

    auth = %Piari.Accounts.Auth{
      user_id: user.id,
      ext_uid: attrs[:ext_uid] || "#{uid}",
      provider: attrs[:provider] || Accounts.Auths.FakeAdapter
    }

    {:ok, auth} = Piari.Repo.insert(auth)

    auth
  end

  def business_fixture(attrs \\ %{}) do
    businessname = "business#{System.unique_integer([:positive])}"

    business = %Piari.Accounts.Business{
      name: attrs[:name] || "#{businessname}",
      phone: attrs[:phone] || generate_random_numbers(10),
      slug: attrs[:slug] || "#{businessname}"
    }

    {:ok, business} = Piari.Repo.insert(business)

    business
  end

  def member_fixture(
        %Piari.Accounts.User{} = user,
        %Piari.Accounts.Business{} = business,
        attrs \\ %{}
      ) do
    member = %Piari.Accounts.Member{
      business_id: business.id,
      user_id: user.id,
      role: attrs[:role] || :member,
      confirmed: attrs[:confirmed] || false
    }

    {:ok, member} = Piari.Repo.insert(member)

    member
  end

  def product_fixture(attrs \\ %{}) do
    product_fixture(business_fixture(), attrs)
  end

  def product_fixture(%Piari.Accounts.Business{} = business, attrs) do
    productname = "product#{System.unique_integer([:positive])}"

    product = %Piari.Offers.Product{
      quantity: attrs[:quantity] || generate_random_numbers(1) |> Integer.parse() |> elem(0),
      expires: attrs[:expires] || ~U[9999-07-18 20:40:21Z],
      accounts_business_id: business.id,
      slug: attrs[:slug] || "#{productname}",
      price: attrs[:price] || generate_random_numbers(6) |> Float.parse() |> elem(0),
      draft: attrs[:draft] || false,
      content: %{
        title: attrs[:title] || "#{productname}-title",
        description: attrs[:description] || "#{productname}-description"
      }
    }

    {:ok, product} = Piari.Repo.insert(product)

    product
  end

  defp generate_random_numbers(length) do
    # credo:disable-for-lines:2
    (:rand.uniform() * :math.pow(10, length))
    |> Kernel.trunc()
    |> to_string
    |> String.pad_leading(length, "0")
  end

  defmodule Accounts.Auths.FakeAdapter do
    use Auth.Adapter, required_config: [:redirect_uri]

    @auth_token "123456"
    def auth_token, do: @auth_token

    @user_email "sample@email.com"
    def user_email, do: @user_email

    @redirect_uri "https://example.com"
    def redirect_uri, do: @redirect_uri

    @impl true
    def authorize_url!(config) do
      redirect_uri = Keyword.fetch!(config, :redirect_uri)
      redirect_uri <> "/login/oauth?id=123456"
    end

    @impl true
    def get_data(params, _config) do
      code = Keyword.fetch!(params, :token)

      if code == @auth_token do
        {:ok, %{email: @user_email, id: @user_email}}
      else
        {:error, :not_found}
      end
    end
  end

  defmodule Accounts.Auths.FakeAuth do
    use Auth.Authenticator,
      otp_app: :auth,
      adapter: Accounts.Auths.FakeAdapter,
      redirect_uri: "will_be_overwritten_by_config"
  end
end
