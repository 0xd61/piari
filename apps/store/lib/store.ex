defmodule Store do
  @moduledoc """
  Documentation for Store.
  """
  defstruct [:uri, :content_type, :size, :hash]

  @path Application.get_env(:store, :path)

  def put(asset_base64, opts \\ %{}) do
    with {:ok, asset} <- Base.decode64(asset_base64),
         {:ok, mime, extension} <- asset_type(asset),
         uri <- unique_uri(extension),
         {:ok, file} <- File.open(uri, [:write]) do
      IO.binwrite(file, asset)
      File.close(file)

      upload =
        %__MODULE__{
          uri: uri,
          hash: hash(uri),
          size: file_size!(uri),
          content_type: mime
        }
        |> optimize(opts)

      {:ok, upload}
    end
  end

  defp file_size!(uri) do
    %{size: size} = File.stat!(uri)
    size
  end

  defp unique_uri(extension) do
    # UUID.uuid4(:hex) <> extension
    random = :crypto.strong_rand_bytes(16) |> Base.url_encode64()
    Path.join(@path, "#{random}-#{System.system_time(:second)}#{extension}")
  end

  # Helper functions to read the binary to determine file type
  defp asset_type(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>),
    do: {:ok, "image/png", ".png"}

  defp asset_type(<<0xFF, 0xD8, _::binary>>), do: {:ok, "image/jpeg", ".jpg"}

  defp asset_type(<<0x25, 0x50, 0x44, 0x46, _::binary>>), do: {:ok, "application/pdf", ".pdf"}

  defp asset_type(_), do: {:error, :type_not_supported}

  def delete(url) do
    File.rm!(url)
  end

  def optimize(upload, opts \\ %{})

  def optimize(%__MODULE__{uri: path, content_type: mime} = upload, opts)
      when mime == "image/jpeg" or mime == "image/png" do
    %{upload | uri: image_crop(path, opts)}
  end

  def optimize(%__MODULE__{} = upload, _opts) do
    upload
  end

  defp image_crop(path, opts) do
    path
    |> Mogrify.open()
    |> Mogrify.custom("crop", "#{opts["width"]}x#{opts["height"]}+#{opts["x"]}+#{opts["y"]}")
    |> Mogrify.save(in_place: true)

    path
  end

  def hash(path) do
    path |> File.stream!([], 2048) |> sha256()
  end

  defp sha256(chunks_enum) do
    chunks_enum
    |> Enum.reduce(
      :crypto.hash_init(:sha256),
      &:crypto.hash_update(&2, &1)
    )
    |> :crypto.hash_final()
    |> Base.encode16()
    |> String.downcase()
  end
end
