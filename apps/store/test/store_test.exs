defmodule StoreTest do
  use ExUnit.Case
  @moduletag :skip

  test "create/1 uploads a file and returns the resource url" do
    assert Store.create() == :ok
  end

  test "delete/1 removes a file" do
    assert Store.delete() == :ok
  end
end
