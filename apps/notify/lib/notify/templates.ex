defmodule Notify.Templates do
  @moduledoc false

  alias Notify.Templates.Render

  def render(template, assigns, locale, formats) do
    Gettext.put_locale(Notify.Gettext, locale)

    formats
    |> Enum.map(fn type ->
      template = filename(template, type)
      {type, Render.render(template, assigns)}
    end)
  end

  defp filename(base, type) do
    base = Atom.to_string(base)
    type = Atom.to_string(type)

    base <> "." <> type
  end
end

defmodule Notify.Templates.Render do
  @moduledoc false

  @template_dir Path.join(:code.priv_dir(:notify), "templates")
  @templates "#{@template_dir}/*.eex" |> Path.expand(__DIR__) |> Path.wildcard()

  import Notify.Gettext
  require EEx

  for template <- @templates do
    # Recompile this module whenever a template file changes
    @external_resource template

    # Convert the template EEX into an Elixir AST
    compiled = EEx.compile_file(template)

    # Convert the full template filepath into a convenient string,
    # such as "template_name.html"
    file =
      template
      |> Path.basename()
      |> String.replace(".eex", "")

    # Defines a render function for each template, taking the filename
    # as the first argument and rendering the EEX within it.
    #
    # A template which looks like this:
    #
    #   # templates/template_name.html.eex
    #   <p>Hello <%= @name %></p>
    #
    # Will compile to this function definition:
    #
    #   def render("template_name.html", assigns) do
    #     "<p>Hello " <> assigns[:name] <> "</p>"
    #   end
    #
    # This will perform very well at runtime because it's only doing
    # string concatenation.

    def render(unquote(file), assigns) do
      unquote(compiled)
    end
  end
end
