defmodule Notify.Adapter.Email do
  use Notify.Adapter, required_config: [:subject, :from], formats: [:text, :html]

  alias Notify.Adapter.Email.Mailer

  def create(config) do
    subject = Keyword.fetch!(config, :subject)
    Mailer.create(subject)
  end

  def deliver(email, recipient, config) do
    from = Keyword.fetch!(config, :from)
    email
    |> Mailer.deliver(recipient, from)
  end

  def put_content(email, [head | tail]) do
    email
    |> Mailer.put_body(head)
    |> put_content(tail)
  end

  def put_content(email, []) do
    email
  end
end
