defmodule Notify.Adapter.Email.Mailer do
  use Bamboo.Mailer, otp_app: :notify
  import Bamboo.Email

  def put_body(email_template, data) do
    case data do
      {:html, data} -> 
        email_template |> html_body(data)
      {:text, data} ->
        email_template |> text_body(data)
      _ -> raise ArgumentError
    end
  end

  def create(subject) do
    new_email()
    |> subject(subject)
  end

  def deliver(email_template, recipient, sender) do
    email_template
    |> from({sender.name, sender.email})
    |> to({recipient.name, recipient.email})
    |> deliver_now()
  end
end
