defmodule Notify.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Notify.Worker.start_link(arg)
      # {Notify.Worker, arg}
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Notify.Router,
        options: [port: 9933],
        transport_options: [num_acceptors: 2, max_connections: 5]
      ),
      {Task.Supervisor, name: Notify.Notifier.TaskSupervisor}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Notify.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
