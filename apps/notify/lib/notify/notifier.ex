defmodule Notify.Notifier do
  @moduledoc """
  Documentation for Notify.Notifier
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      alias Notify.Notifier

      @otp_app Keyword.fetch!(opts, :otp_app)
      @notifier_config opts

      @type config :: Keyword.t()
      @type recipient :: %{optional(any) => any}
      @type notification :: %{:__struct__ => atom(), optional(atom()) => any()}

      @spec template(atom, String.t(), Keyword.t()) :: Keyword.t()
      def template(assigns, name, attrs \\ [])

      def template(assigns, name, attrs) do
        Notifier.template(assigns, name, attrs, parse_config([]))
      end

      @spec deliver(Keyword.t(), recipient, config) :: notification
      def deliver(data, recipient, config \\ [])

      def deliver(data, recipient, config) do
        Notifier.deliver(data, recipient, parse_config(config))
      end

      @spec deliver_async([String.t()], recipient, config) :: Task.t()
      def deliver_async(data, recipient, config \\ [])

      def deliver_async(data, recipient, config) do
        Notifier.deliver_async(data, recipient, parse_config(config))
      end

      @on_load :validate_dependency

      @doc false
      def validate_dependency do
        adapter = Keyword.get(parse_config([]), :adapter)
        Notifier.validate_dependency(adapter)
      end

      defp parse_config(config) do
        Notifier.parse_config(@otp_app, __MODULE__, @notifier_config, config)
      end
    end
  end

  @type config :: Keyword.t()
  @type notification :: %{:__struct__ => atom(), optional(atom()) => any()}
  @type recipient :: %{optional(any) => any}

  @doc """
  Send async notification. Calls `deliver/3` in a task.
  """

  @spec deliver_async([String.t()], recipient, config) :: Task.t()
  def deliver_async(data, _recipient, _config) when is_list(data) === false do
    raise ArgumentError, message: "expected [list], got: [#{inspect(data)}]\n"
  end

  def deliver_async(data, recipient, config) do
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    :ok = adapter.validate_formats(Keyword.keys(data))

    Task.Supervisor.async_nolink(Notify.Notifier.TaskSupervisor, fn ->
      deliver(data, recipient, config)
    end)
  end

  @doc """
  Send notification to recipient. Recipient Keys are different for different adapters.

  If the adapter does not support the `data`-format, a `ArgumentError` is raised.

  ## Examples

      iex> [html: "<p>data</p>", text: "data"]
            |> Notifier.deliver(%{name: someone, email: someone@example.com})
  """

  @spec deliver(Keyword.t(), recipient, config) :: notification
  def deliver(data, _recipient, _config) when is_list(data) === false do
    raise ArgumentError, message: "expected [list], got: [#{inspect(data)}]\n"
  end

  def deliver(data, recipient, config) do
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    :ok = adapter.validate_formats(Keyword.keys(data))

    config
    |> adapter.create()
    |> adapter.put_content(data)
    |> adapter.deliver(recipient, config)
  end

  @doc """
  Renders template. Templates location: priv/templates

  If the template does not exist, a `FunctionClauseError` is raised.

  If the adapter does not support the format, a `ArgumentError` is raised.

  ## Examples

      iex> [data: "data"] |> Notifier.template(:login, locale: "de", formats:
      [:html, :text])
      [
        html: "<p>Hier ist dein Login Link: data</p>",
        text: "Hier ist dein Login Link: data",
      ]

  iex> [] |> Notifier.template(:login, locale: "de", formats:
  [:html, :text])
  [
    html: "<p>Hier ist dein Login Link: </p>",
    text: "Hier ist dein Login Link: ",
  ]
  """

  @spec template(atom, String.t(), Keyword.t(), Keyword.t()) :: Keyword.t()
  def template(assigns, name, attrs, config) do
    adapter = Keyword.fetch!(config, :adapter)
    locale = Keyword.get(attrs, :locale, "en")
    formats = Keyword.get(attrs, :formats, adapter.list_formats)
    :ok = adapter.validate_formats(formats)

    Notify.Templates.render(name, assigns, locale, formats)
  end

  @doc """
  Parse configs in the following order, later ones taking priority:
  1. mix configs
  2. compiled configs in Notifier module
  3. dynamic configs passed into the function
  """

  def parse_config(otp_app, notifier, notifier_config, dynamic_config) do
    # credo:disable-for-lines:3
    Application.get_env(otp_app, notifier, [])
    |> Keyword.merge(notifier_config)
    |> Keyword.merge(dynamic_config)
  end

  @doc false
  def validate_dependency(adapter) do
    require Logger

    with adapter when not is_nil(adapter) <- adapter,
         {:module, _} <- Code.ensure_loaded(adapter),
         true <- function_exported?(adapter, :validate_dependency, 0),
         :ok <- adapter.validate_dependency() do
      :ok
    else
      no_match when no_match in [nil, false] ->
        :ok

      {:error, :nofile} ->
        Logger.error("#{adapter} does not exist")
        :abort

      {:error, deps} when is_list(deps) ->
        Logger.error(Notify.Notifier.missing_deps_message(adapter, deps))
        :abort
    end
  end

  @doc false
  def missing_deps_message(adapter, deps) do
    deps =
      deps
      |> Enum.map(fn
        {lib, module} -> "#{module} from #{inspect(lib)}"
        {module} -> inspect(module)
      end)
      |> Enum.map(&"\n- #{&1}")

    """
    The following dependencies are required to use #{inspect(adapter)}:
    #{deps}
    """
  end
end
