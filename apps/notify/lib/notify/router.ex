defmodule Notify.Router do
  use Plug.Router

  plug :match
  plug :dispatch

  forward "/email/sent", to: Bamboo.SentEmailViewerPlug
end
