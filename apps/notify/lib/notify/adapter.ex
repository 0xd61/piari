defmodule Notify.Adapter do
  @moduledoc ~S"""
  Specification of the notification adapter.
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      @required_config opts[:required_config] || []
      @formats opts[:formats] || []

      @behaviour Notify.Adapter

      def validate_config(config) do
        Notify.Adapter.validate_config(@required_config, config)
      end

      def validate_formats(data) do
        Notify.Adapter.validate_formats(@formats, data)
      end

      def list_formats do
        @formats
      end
    end
  end

  @type config :: Keyword.t()
  @type params :: Keyword.t()
  @type notification :: %{:__struct__ => atom(), optional(atom()) => any()}
  @type recipient :: %{optional(any) => any}

  @callback validate_config(config) :: :ok | no_return
  @callback validate_formats([atom]) :: :ok | no_return

  @callback list_formats :: [atom]

  @callback create(config) :: notification

  @callback put_content(notification, Keyword.t()) :: notification

  @callback deliver(notification, recipient, config) :: no_return | notification

  @spec validate_config([atom], config) :: :ok | no_return
  def validate_config(required_config, config) do
    missing_keys =
      Enum.reduce(required_config, [], fn key, missing_keys ->
        if config[key] in [nil, ""],
          do: [key | missing_keys],
          else: missing_keys
      end)

    raise_on_missing_config(missing_keys, config)
  end

  @spec validate_formats([atom], [atom]) :: :ok | no_return
  def validate_formats(available_formats, formats) do
    unavailable_formats =
      Enum.reduce(formats, [], fn format, unavailable_formats ->
        if format in available_formats,
          do: unavailable_formats,
          else: [format | unavailable_formats]
      end)

    raise_on_unavailable_format(unavailable_formats, available_formats)
  end

  defp raise_on_unavailable_format([], _available), do: :ok

  defp raise_on_unavailable_format(format, available) do
    raise ArgumentError, """
    expected #{inspect(available)} as format, got: #{inspect(format)}
    """
  end

  defp raise_on_missing_config([], _config), do: :ok

  defp raise_on_missing_config(key, config) do
    raise ArgumentError, """
    expected #{inspect(key)} to be set, got: #{inspect(config)}
    """
  end
end
