defmodule Notify.Adapter.EmailTest do
  use ExUnit.Case
  use Bamboo.Test
  import Bamboo.Email

  alias Notify.Adapter.Email

  @recipient %{name: "someone", email: "someone@example.com"}
  @from %{name: "anotherone", email: "anotherone@example.com"}

  test "create/1 creates a new email" do
    email = Email.create(subject: "Test Email")
    assert %Bamboo.Email{} = email
    assert email.subject == "Test Email"
  end

  test "deliver/3 sends email to recipient" do
    email =
      new_email(
        subject: "Welcome!",
        text_body: "Welcome to the app",
        html_body: "<html><strong>Welcome to the app</strong></html>"
      )

    email |> Email.deliver(@recipient, from: @from)

    assert_delivered_email(
      new_email(
        from: {@from.name, @from.email},
        to: {@recipient.name, @recipient.email},
        subject: "Welcome!",
        text_body: "Welcome to the app",
        html_body: "<html><strong>Welcome to the app</strong></html>"
      )
    )
  end

  test "put_content/2 adds the body to a message" do
    content = [
      html: ~s(<html><strong>Welcome to the app</strong></html>),
      text: ~s(Welcome to the app)
    ]

    email =
      new_email(
        subject: "Welcome!",
        from: {@from.name, @from.email},
        to: {@recipient.name, @recipient.email}
      )

    email = email |> Email.put_content(content)

    assert email.html_body == content[:html]
    assert email.text_body == content[:text]
  end

  test "put_content/2 overwrites body with latest string if provided multiple times" do
    content = [
      text: ~s(Welcome to the app1),
      text: ~s(Welcome to the app2),
      text: ~s(Welcome to the app3)
    ]

    email =
      new_email(
        subject: "Welcome!",
        from: {@from.name, @from.email},
        to: {@recipient.name, @recipient.email}
      )

    email = email |> Email.put_content(content)

    assert email.html_body == nil
    assert email.text_body == "Welcome to the app3"
  end
end
