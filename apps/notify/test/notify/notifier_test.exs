defmodule Notify.NotifierTest do
  use ExUnit.Case, async: true

  @data "123456"
  @recipient %{name: "someone", email: "someone@example.com"}
  @from %{name: "anotherone", email: "anotherone@example.com"}

  defmodule FakeAdapter do
    use Notify.Adapter, required_config: [:from], formats: [:text]

    defstruct data: "", recipient: %{}, from: %{}

    @impl true
    def create(_config) do
      %FakeAdapter{}
    end

    @impl true
    def put_content(notification, data) do
      content = Keyword.fetch!(data, :text)
      %FakeAdapter{notification | data: content}
    end

    @impl true
    def deliver(notification, recipient, config) do
      %FakeAdapter{notification | recipient: recipient, from: Keyword.fetch!(config, :from)}
    end
  end

  defmodule FakeNotifier do
    use Notify.Notifier,
      otp_app: :notify,
      adapter: FakeAdapter,
      from: "foo@bar.com"
  end

  test "template/3 returns localized template" do
    assert [text: template] =
             [name: "teststring"]
             |> FakeNotifier.template(:welcome)

    assert template =~ "teststring"
  end

  test "template/3 raises an error if template not exists" do
    assert_raise FunctionClauseError,  fn ->
      FakeNotifier.template([], :does_not_exist)
    end
  end

  test "template/3 raises an error, if format is not available" do
    assert_raise ArgumentError, ~r/expected \[.*\] as format, got: \[.*\]\n/, fn ->
      FakeNotifier.template([], :welcome, formats: [:html])
    end
  end

  test "deliver/3 raises an error if data is not a list" do
    assert_raise ArgumentError, ~r/expected \[.*\], got: \[.*\]\n/, fn ->
      FakeNotifier.deliver(@data, @recipient)
    end
  end

  test "deliver/3 creates a notification and adds data" do
    assert %{data: data, recipient: recipient, from: from} =
             FakeNotifier.deliver([text: @data], @recipient)

    assert data == @data
    assert recipient == @recipient
  end

  test "deliver/3 raises an error, if format is not available" do
    assert_raise ArgumentError, ~r/expected \[.*\] as format, got: \[.*\]\n/, fn ->
      FakeNotifier.deliver([undefined: @data], @recipient)
    end
  end

  test "deliver_async/3 creates a task to deliver notification" do
    task = FakeNotifier.deliver_async([text: @data], @recipient)
    ref = Process.monitor(task.pid)
    assert_receive {:DOWN, ^ref, :process, _, :normal}, 5000
  end

  test "function config overwrites module config" do
    notification = FakeNotifier.deliver([text: @data], @recipient)
    assert notification.from == "foo@bar.com"

    notification = FakeNotifier.deliver([text: @data], @recipient, from: @from)
    assert notification.from == @from
  end

  test "Module with missing keys in config raises argument error" do
    defmodule FakeNotifierError do
      use Notify.Notifier,
        otp_app: :notify,
        adapter: FakeAdapter
    end

    assert_raise ArgumentError, ~r/expected \[.*\] to be set, got: \[.*\]\n/, fn ->
      FakeNotifierError.deliver([text: @data], @recipient)
    end
  end

  test "Module with missing adapter config raises key error" do
    defmodule FakeNotifierAdapterError do
      use Notify.Notifier,
        otp_app: :notify
    end

    assert_raise KeyError, fn ->
      FakeNotifierAdapterError.deliver([text: @data], @recipient)
    end
  end
end
