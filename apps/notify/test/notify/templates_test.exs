defmodule Notify.TemplatesTest do
  use ExUnit.Case

  alias Notify.Templates

  @data "my test data"

  test "render/4 renders data into template" do
    templates = Templates.render(:welcome, [name: @data], "en", [:html, :text])

    Enum.each(templates, fn {_, template} ->
      assert template =~ "Welcome to Piari, #{@data}"
    end)
  end

  test "render/4 renders data into temlpate using gettext" do
    templates = Templates.render(:welcome, [name: @data], "de", [:html, :text])

    Enum.each(templates, fn {_, template} ->
      assert template =~ "Willkommen bei Piari, #{@data}"
    end)
  end
end
