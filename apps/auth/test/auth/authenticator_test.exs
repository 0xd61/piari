defmodule Auth.AuthenticatorTest do
  use ExUnit.Case, async: true

  defmodule FakeAdapter do
    use Auth.Adapter, required_config: [:redirect_uri]

    @impl true
    def authorize_url!(config) do
      redirect_uri = Keyword.fetch!(config, :redirect_uri)
      redirect_uri <> "/login/oauth?id=123456"
    end

    @impl true
    def get_data(params, _config) do
      code = Keyword.fetch!(params, :code)

      if code == "123456" do
        {:ok, %{email: "sample@email.com", id: 13}}
      else
        {:error, :not_found}
      end
    end
  end

  defmodule FakeAuth do
    use Auth.Authenticator,
      otp_app: :auth,
      adapter: FakeAdapter,
      redirect_uri: "https://example.com"
  end

  test "authorize_url!/0 returns url for authentication with test adapter" do
    FakeAuth.authorize_url!()
    assert FakeAuth.authorize_url!() == "https://example.com/login/oauth?id=123456"
  end

  test "authenticate/1 returns ok and email if authenticated with test adapter" do
    assert FakeAuth.authenticate(code: "123456") ==
             {:ok, %{email: "sample@email.com", id: Base.encode64("fakeadapter.13")}}
  end

  test "authenticate/1 returns error if not authenticated with test adapter" do
    assert FakeAuth.authenticate(code: "bad_value") == {:error, :not_authenticated}
  end

  test "Module with missing keys in config raises argument error" do
    defmodule FakeAuthError do
      use Auth.Authenticator,
        otp_app: :auth,
        adapter: FakeAdapter
    end

    assert_raise ArgumentError, ~r/expected \[.*\] to be set, got: \[.*\]\n/, fn ->
      FakeAuthError.authorize_url!()
    end
  end

  test "Module with missing adapter config raises key error" do
    defmodule FakeAuthError do
      use Auth.Authenticator,
        otp_app: :auth
    end

    assert_raise KeyError, fn ->
      FakeAuthError.authorize_url!()
    end
  end
end
