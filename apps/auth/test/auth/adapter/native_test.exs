defmodule Auth.Adapter.NativeTest do
  use ExUnit.Case, async: true

  alias Auth.Adapter.Native

  @config [
    redirect_uri: "http://oauth.test/sessions",
    email: "someone@example.com"
  ]

  setup cache do
    _ = start_supervised!({Auth.Cache, name: cache.test})
    %{cache: cache.test}
  end

  test "authorize_url!/1 returns github oauth url" do
    assert Native.authorize_url!(@config) =~ "http://oauth.test/sessions?code="
  end

  test "get_data/2 returns cached data based on token" do
    url = Native.authorize_url!(@config)

    %{"code" => token} =
      URI.parse(url).query
      |> URI.decode_query()

    assert {:ok, %{email: "someone@example.com", id: "someone@example.com"}} =
             Native.get_data([code: token], [])
  end


  test "get_data/2 returns error, if token is invalid" do
    assert  {:error, :key_not_found} =
      Native.get_data([code: "invalid_token"], [])
  end
end
