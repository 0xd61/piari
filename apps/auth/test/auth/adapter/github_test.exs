defmodule Auth.Adapter.GithubTest do
  use ExUnit.Case, async: true

  alias Auth.Adapter.Github

  @testport 25_903

  @config [
    site: "http://localhost:#{@testport}",
    authorize_url: "http://localhost:#{@testport}/login/oauth/authorize",
    token_url: "http://localhost:#{@testport}/login/oauth/access_token",
    # only test data
    client_id: "79ae4408b65f2e1a416a",
    client_secret: "0c53eaf1292bdc149fcb54e3cf1fdac63add22c9",
    redirect_uri: "https://google.com"
  ]

  test "authorize_url!/1 returns github oauth url" do
    assert Github.authorize_url!(@config) ==
             "http://localhost:#{@testport}/login/oauth/authorize?client_id=79ae4408b65f2e1a416a&redirect_uri=https%3A%2F%2Fgoogle.com&response_type=code&scope=user"
  end

  describe "get_data/2" do
    setup do
      bypass = Bypass.open(port: @testport)

      Bypass.expect(bypass, "POST", "/login/oauth/access_token", fn conn ->
        {:ok, body, conn} = Plug.Conn.read_body(conn)

        return_val =
          if body ==
               ~s<client_id=79ae4408b65f2e1a416a&client_secret=0c53eaf1292bdc149fcb54e3cf1fdac63add22c9&code=123456&grant_type=authorization_code&redirect_uri=https%3A%2F%2Fgoogle.com> do
            ~s<{"access_token":"e72e16c7e42f292c6912e7710c838347ae178b4a", "scope":"repo,gist", "token_type":"bearer"}>
          else
            ~s<{"access_token":"null", "scope":"repo,gist", "token_type":"bearer"}>
          end

        Plug.Conn.resp(
          conn,
          200,
          return_val
        )
      end)

      Bypass.expect(bypass, "GET", "/user/emails", fn conn ->
        Plug.Conn.resp(conn, 200, ~s<[
          {
            "email": "someone1@example.com",
            "primary": false,
            "verified": true,
            "visibility": null
          },
          {
            "email": "someone2@example.com",
            "primary": true,
            "verified": true,
            "visibility": "private"
          },
          {
            "email": "someone3@example.com",
            "primary": false,
            "verified": true,
            "visibility": null
          }
        ]
>)
      end)

      Bypass.expect(bypass, "GET", "/user", fn conn ->
        Plug.Conn.resp(conn, 200, ~s<{
          "login": "username",
          "id": 8351869,
          "node_id": "MDQ6VXNlcjgzNTE4Njk=",
          "avatar_url": "https://avatars3.githubusercontent.com/u/8351869?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/username",
          "html_url": "https://github.com/username",
          "followers_url": "https://api.github.com/users/username/followers",
          "following_url": "https://api.github.com/users/username/following{/other_user}",
          "gists_url": "https://api.github.com/users/username/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/username/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/username/subscriptions",
          "organizations_url": "https://api.github.com/users/username/orgs",
          "repos_url": "https://api.github.com/users/username/repos",
          "events_url": "https://api.github.com/users/username/events{/privacy}",
          "received_events_url": "https://api.github.com/users/username/received_events",
          "type": "User",
          "site_admin": false,
          "name": null,
          "company": null,
          "blog": "",
          "location": "Germany",
          "email": null,
          "hireable": null,
          "bio": null,
          "public_repos": 23,
          "public_gists": 9,
          "followers": 1,
          "following": 11,
          "created_at": "2014-08-04T13:19:18Z",
          "updated_at": "2019-03-16T14:38:18Z",
          "private_gists": 8,
          "total_private_repos": 2,
          "owned_private_repos": 3,
          "disk_usage": 46090,
          "collaborators": 0,
          "two_factor_authentication": true,
          "plan": {
            "name": "pro",
            "space": 976562499,
            "collaborators": 0,
            "private_repos": 9999
          }
      }>)
      end)

      {:ok, bypass: bypass}
    end

    test "returns user data" do
      {:ok, data} = Github.get_data([code: "123456"], @config)
      assert %{email: "someone2@example.com", id: 8_351_869} == data
    end
  end
end
