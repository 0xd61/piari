defmodule Auth.Authenticator do
  @moduledoc """
  Documentation for Auth.Client
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      alias Auth.Authenticator

      @otp_app Keyword.fetch!(opts, :otp_app)
      @authenticator_config opts

      # API

      def authorize_url!(config \\ [])

      def authorize_url!(config) do
        Authenticator.authorize_url!(parse_config(config))
      end

      def authenticate(params, config \\ [])

      def authenticate(params, config) do
        Authenticator.authenticate(params, parse_config(config))
      end

      @on_load :validate_dependency

      @doc false
      def validate_dependency do
        adapter = Keyword.get(parse_config([]), :adapter)
        Authenticator.validate_dependency(adapter)
      end

      defp parse_config(config) do
        Authenticator.parse_config(@otp_app, __MODULE__, @authenticator_config, config)
      end
    end
  end

  def authorize_url!(config) do
    adapter = Keyword.fetch!(config, :adapter)

    :ok = adapter.validate_config(config)
    adapter.authorize_url!(config)
  end

  def authenticate(params, config) do
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)

    with {:ok, ret} <- adapter.get_data(params, config),
         %{email: _, id: _} = ret do
      {:ok, %{ret | id: adapter.prefix_encode_id(ret.id)}}
    else
      _ -> {:error, :not_authenticated}
    end
  end

  @doc """
  Parse configs in the following order, later ones taking priority:
  1. mix configs
  2. compiled configs in Authenticator module
  3. dynamic configs passed into the function
  """

  def parse_config(otp_app, authenticator, authenticator_config, dynamic_config) do
    # credo:disable-for-lines:3
    Application.get_env(otp_app, authenticator, [])
    |> Keyword.merge(authenticator_config)
    |> Keyword.merge(dynamic_config)
  end

  @doc false
  def validate_dependency(adapter) do
    require Logger

    with adapter when not is_nil(adapter) <- adapter,
         {:module, _} <- Code.ensure_loaded(adapter),
         true <- function_exported?(adapter, :validate_dependency, 0),
         :ok <- adapter.validate_dependency() do
      :ok
    else
      no_match when no_match in [nil, false] ->
        :ok

      {:error, :nofile} ->
        Logger.error("#{adapter} does not exist")
        :abort

      {:error, deps} when is_list(deps) ->
        Logger.error(Auth.Authenticator.missing_deps_message(adapter, deps))
        :abort
    end
  end

  @doc false
  def missing_deps_message(adapter, deps) do
    deps =
      deps
      |> Enum.map(fn
        {lib, module} -> "#{module} from #{inspect(lib)}"
        {module} -> inspect(module)
      end)
      |> Enum.map(&"\n- #{&1}")

    """
    The following dependencies are required to use #{inspect(adapter)}:
    #{deps}
    """
  end
end
