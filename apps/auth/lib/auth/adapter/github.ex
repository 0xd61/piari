defmodule Auth.Adapter.Github do
  use OAuth2.Strategy
  use Auth.Adapter, required_config: [:redirect_uri, :client_id, :client_secret]

  # Public API
  @defaults %{
    site: Application.get_env(:auth, __MODULE__)[:site],
    authorize_url: Application.get_env(:auth, __MODULE__)[:authorize_url],
    token_url: Application.get_env(:auth, __MODULE__)[:token_url]
  }

  def client(opts) do
    %{
      redirect_uri: redirect_uri,
      site: site,
      authorize_url: authorize_url,
      token_url: token_url,
      client_id: client_id,
      client_secret: client_secret
    } = Enum.into(opts, @defaults)

    OAuth2.Client.new(
      strategy: __MODULE__,
      client_id: client_id,
      client_secret: client_secret,
      redirect_uri: redirect_uri,
      site: site,
      authorize_url: authorize_url,
      token_url: token_url
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  @impl Auth.Adapter
  def authorize_url!(config) do
    OAuth2.Client.authorize_url!(client(config), scope: "user")
  end

  @doc """
  Gets primary email adress from Github.
  OAuth token is provided in "params" with the "code" Key.
  """
  @impl Auth.Adapter
  def get_data(params, config) do
    with client <- OAuth2.Client.get_token!(client(config), params),
         {:ok, email} <- client |> wrapper_get!("/user/emails") |> get_email(),
         {:ok, id} <- client |> wrapper_get!("/user") |> get_id() do
      {:ok, %{email: email, id: id}}
    end
  end

  defp wrapper_get!(client, url) do
    OAuth2.Client.get!(client, url).body
  end

  defp get_email(response) do
    ret =
      response
      |> Enum.filter(fn
        %{"primary" => true, "verified" => true} -> true
        _ -> false
      end)

    case ret do
      [%{"email" => email}] ->
        if email |> String.match?(~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/) do
          {:ok, email}
        else
          {:error, :email_wrong_format}
        end

      _ ->
        {:error, :email_not_found}
    end
  end

  defp get_id(response) do
    case response do
      %{"id" => id} -> {:ok, id}
      _ -> {:error, :id_not_found}
    end
  end

  # Strategy Callbacks

  @impl OAuth2.Strategy
  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  @impl OAuth2.Strategy
  def get_token(client, params, headers) do
    client
    |> put_param(:client_secret, client.client_secret)
    |> put_header("accept", "application/json")
    |> OAuth2.Strategy.AuthCode.get_token(params, headers)
  end
end
