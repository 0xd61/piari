defmodule Auth.Adapter.Native do
  use Auth.Adapter, required_config: [:redirect_uri, :email]
  alias Auth.Cache

  @impl Auth.Adapter
  def authorize_url!(config) do
    token = Auth.Helpers.gen_key()

    Cache.put(token, config)

    config[:redirect_uri]
    |> build_uri(code: token, email: config[:email])
    |> URI.to_string()
  end

  defp build_uri(base, query) do
    base
    |> URI.parse()
    |> URI.merge("?" <> URI.encode_query(query))
  end

  @doc """
  Gets email adress from token.
  Token is provided in "params" with the "code" Key.
  """

  @impl Auth.Adapter
  def get_data(params, _config) do
    with {:ok, token} = Keyword.fetch(params, :code),
         {:ok, ret} <- token |> URI.decode() |> Cache.fetch(),
         {:ok, email} <- ret |> Keyword.fetch(:email),
         _ <- Cache.drop(token) do
      {:ok, Enum.into(ret, %{id: email})}
    end
  end
end
