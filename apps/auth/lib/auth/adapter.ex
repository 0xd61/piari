defmodule Auth.Adapter do
  @moduledoc ~S"""
  Specification of the authentication adapter.
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      @required_config opts[:required_config] || []

      @behaviour Auth.Adapter

      def validate_config(config) do
        Auth.Adapter.validate_config(@required_config, config)
      end

      @spec prefix_encode_id(atom(), String.t()) :: String.t()
      def prefix_encode_id(prefix \\ __MODULE__, id)

      def prefix_encode_id(prefix, id) do
        Auth.Adapter.prefix_encode_id(prefix, id)
      end
    end
  end

  @type config :: Keyword.t()
  @type params :: Keyword.t()
  @type id :: String.t()
  @type prefix :: atom()

  @callback validate_config(config) :: :ok | no_return
  @callback prefix_encode_id(prefix, id) :: String.t()

  @callback authorize_url!(config) :: String.t()
  @callback get_data(params, config) ::
              {:ok,
               %{
                 required(:email) => String.t(),
                 required(:id) => Integer
               }}
              | {:error, :not_found}

  @spec validate_config([atom], config) :: :ok | no_return
  def validate_config(required_config, config) do
    missing_keys =
      Enum.reduce(required_config, [], fn key, missing_keys ->
        if config[key] in [nil, ""],
          do: [key | missing_keys],
          else: missing_keys
      end)

    raise_on_missing_config(missing_keys, config)
  end

  def prefix_encode_id(prefix, id) do
    prefix =
      prefix
      |> to_string()
      |> String.split(".")
      |> Enum.take(-1)
      |> List.to_string()
      |> String.downcase()

    (prefix <> "." <> to_string(id))
    |> Base.encode64()
  end

  defp raise_on_missing_config([], _config), do: :ok

  defp raise_on_missing_config(key, config) do
    raise ArgumentError, """
    expected #{inspect(key)} to be set, got: #{inspect(config)}
    """
  end
end
