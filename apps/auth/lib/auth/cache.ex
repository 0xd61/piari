defmodule Auth.Cache do
  use GenServer

  @clear_interval :timer.minutes(10)
  @expires_after :timer.minutes(30) / 1000

  def start_link(opts) do
    opts = Keyword.put_new(opts, :name, __MODULE__)
    GenServer.start_link(__MODULE__, opts, name: opts[:name])
  end

  def put(name \\ __MODULE__, key, value) do
    true = :ets.insert(tab_name(name), {key, value, DateTime.utc_now() |> DateTime.to_unix()})
    :ok
  end

  def fetch(name \\ __MODULE__, key) do
    {:ok, :ets.lookup_element(tab_name(name), key, 2)}
  rescue
    ArgumentError -> {:error, :key_not_found}
  end

  def drop(name \\ __MODULE__, key) do
    :ets.delete(tab_name(name), key)
    :ok
  end

  def init(opts) do
    state = %{
      inverval: opts[:clear_interval] || @clear_interval,
      expires_after: opts[:expires_after] || @expires_after,
      timer: nil,
      table: new_table(opts[:name])
    }

    {:ok, schedule_clear(state)}
  end

  def handle_info(:clear, state) do
    timestamp = DateTime.utc_now() |> DateTime.to_unix()

    :ets.select_delete(state.table, [
      {{:_, :_, :"$1"}, [{:<, :"$1", timestamp - state.expires_after}], [true]}
    ])

    {:noreply, schedule_clear(state)}
  end

  defp schedule_clear(state) do
    %{state | timer: Process.send_after(self(), :clear, state.inverval)}
  end

  defp new_table(name) do
    name
    |> tab_name()
    |> :ets.new([:set, :named_table, :public, read_concurrency: true, write_concurrency: true])
  end

  defp tab_name(name), do: :"#{name}_cache"
end
