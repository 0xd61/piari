defmodule Auth do
  @moduledoc """
  Documentation for Auth.
  """

  def gen_key() do
    Auth.Helpers.gen_key()
  end
end
