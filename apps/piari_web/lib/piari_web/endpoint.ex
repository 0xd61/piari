defmodule PiariWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :piari_web

  socket "/socket", PiariWeb.UserSocket,
    websocket: [timeout: 45_000],
    longpoll: false

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :piari_web,
    gzip: true,
    only: ~w(css fonts images js),
    only_matching: ~w(sw manifest cache_manifest favicon robots)

  # TODO: Only tmp for testing (will be changed to s3)
  plug Plug.Static, at: "/media/uploads", from: "/media/uploads"

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_piari_web_key",
    signing_salt: "QI8jX4Zg",
    encryption_salt: "ouVcluRx"

  plug Corsica,
    origins: "*",
    allow_headers: ["content-type", "authorization"],
    allow_methods: ["GET", "POST", "DELETE"]

  plug PiariWeb.Router
end
