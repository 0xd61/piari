defimpl Phoenix.Param, for: Piari.Accounts.Business do
  def to_param(%{slug: slug}) do
    "#{slug}"
  end
end

defimpl Phoenix.Param, for: Piari.Offers.Product do
  def to_param(%{slug: slug, id: id}) do
    "#{id}-#{slug}"
  end
end
