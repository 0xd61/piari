defmodule PiariWeb.SessionController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias PiariWeb.AuthHelpers

  defp redirect_wrapper(url, conn) do
    conn
    |> redirect(external: url)
  end

  def create(conn, %{"provider" => "github"}) do
    Accounts.auth_url(:github, redirect_uri: Routes.session_url(conn, :callback, "github"))
    |> redirect_wrapper(conn)
  end

  def create(conn, _params) do
    conn
    |> render_error(400,
      code: "invalid_provider",
      message: "selected provider is invalid"
    )
  end

  def callback(conn, %{"provider" => "github", "code" => code}) do
    Accounts.authenticate(:github, [code: code],
      redirect_uri: Routes.session_url(conn, :callback, "github")
    )
    |> handle_callback(conn, :github)
  end

  def handle_callback(ret, conn, provider) do
    case ret do
      {:ok, %{email: email, id: auth_id}} ->
        conn
        |> login(auth_id, email, provider)

      _ ->
        conn
        |> render_error(400)
    end
  end

  defp login(conn, auth_id, email, provider) do
    case Accounts.get_or_register_user(auth_id, email, provider) do
      {:ok, user} ->
        conn
        |> AuthHelpers.login(user)
        |> client_redirect(provider)

      {:ok, :new, user} ->
        conn
        |> AuthHelpers.login(user)
        |> client_redirect(provider)

      {:ok, :assigned, user} ->
        conn
        |> AuthHelpers.login(user)
        |> client_redirect(provider)
    end
  end

  defp temp_token(conn, user) do
    token =
      Phoenix.Token.sign(
        conn,
        Application.fetch_env!(:piari_web, PiariWeb.Endpoint)[:token_salt],
        user.id
      )

    token
  end

  defp client_redirect(conn, provider) do
    user = conn.assigns.current_user
    token = temp_token(conn, user)
    url = client_uri(provider, %{temp_token: token, email: user.email, locale: user.locale})
    redirect(conn, external: url)
  end

  defp client_uri(provider, params) when is_atom(provider) do
    client_uri(Atom.to_string(provider), params)
  end

  defp client_uri(provider, params) when is_binary(provider) do
    query = URI.encode_query(params)

    uri =
      Application.get_env(:piari_web, PiariWeb.Endpoint)[:client_callback_url]
      |> Path.join(provider)

    uri <> "?" <> query
  end
end
