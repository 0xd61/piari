defmodule PiariWeb.ControllerHelpers do
  import Plug.Conn
  import Phoenix.Controller
  alias Piari.Accounts
  alias Piari.Offers

  def render_error(conn, status, assigns \\ []) do
    conn
    |> put_status(status)
    |> put_layout(false)
    |> put_view(PiariWeb.ErrorView)
    |> render(:"#{status}", assigns)
    |> halt()
  end

  def changeset_error(conn, %Ecto.Changeset{} = changeset) do
    errors = traverse_errors(changeset)
    render_error(conn, 422, errors: errors)
  end

  defp traverse_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  def fetch_user(conn, _opts) do
    if param = conn.params["user"] do
      user = Accounts.get_user!(param)
      assign(conn, :user, user)
    else
      assign(conn, :user, nil)
    end
  end

  def fetch_business(conn, _opts) do
    if param = conn.params["business"] do
      business =
        case Integer.parse(param) do
          {int, ""} -> Accounts.get_business!(int)
          _ -> Accounts.get_business!(param)
        end

      assign(conn, :business, business)
    else
      assign(conn, :business, nil)
    end
  end

  def fetch_business_offer(conn, opts) do
    conn = fetch_business(conn, opts)
    business = conn.assigns.business

    if param = conn.params["offer"] do
      offer = Offers.get_business_product!(business, param, [:attachments, :image])
      assign(conn, :offer, offer)
    else
      assign(conn, :offer, nil)
    end
  end
end
