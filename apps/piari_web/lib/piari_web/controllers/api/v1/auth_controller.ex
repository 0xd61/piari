defmodule PiariWeb.Api.V1.AuthController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias PiariWeb.AuthHelpers

  plug :fetch_user

  plug :authorize,
       [action: "read", funs: &AuthHelpers.user_access/3]
       when action in [:index]

  plug :authorize,
       [action: "write", funs: &AuthHelpers.user_access/3]
       when action in [:delete]

  def index(conn, _params) do
    user = conn.assigns.user
    auths = Accounts.list_auths(user)

    render(conn, "index.json", auths: auths, user: user)
  end

  def delete(conn, %{"auth" => id}) do
    user = conn.assigns.user
    auth = Accounts.get_auth!(user, id)

    {:ok, _auth} = Accounts.delete_auth(auth)
    render(conn, "delete.json")
  end
end
