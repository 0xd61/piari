defmodule PiariWeb.Api.V1.KeyController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias PiariWeb.AuthHelpers, as: Auth

  @max_age 5 * 60

  plug :authorize when action in [:delete]

  def create(conn, %{"temp_token" => token}) do
    with {:ok, user_id} <- verify_token(conn, token),
         user <- Accounts.get_user!(user_id),
         {:ok, key} <- Accounts.create_key(user) do
      render(conn, "create.json", token: key.user_secret, user: user)
    else
      {:error, reason} ->
        Auth.error(conn, {:error, reason})
    end
  end

  def delete(conn, _params) do
    case Accounts.revoke_key(conn.assigns.key) do
      nil ->
        render_error(500,
          code: "internal_error",
          message: "key has not been revoked"
        )

      _ ->
        render(conn, "delete.json")
    end
  end

  defp verify_token(conn, token) do
    Phoenix.Token.verify(
      conn,
      Application.fetch_env!(:piari_web, PiariWeb.Endpoint)[:token_salt],
      token,
      max_age: @max_age
    )
  end
end
