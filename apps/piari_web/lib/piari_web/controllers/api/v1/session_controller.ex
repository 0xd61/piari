defmodule PiariWeb.Api.V1.SessionController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias Piari.Notifications

  def create(conn, %{"provider" => "email", "email" => email}) do
    url =
      Accounts.auth_url(:email,
        redirect_uri: Routes.session_url(conn, :callback, "email"),
        email: email
      )

    [url: url]
    |> Notifications.Email.template(:login)
    |> Notifications.Email.deliver(%{name: email, email: email},
      subject: gettext("Your Piari Sign In Link")
    )

    render(conn, "create.json", url: url, provider: :email)
  end

  def create(conn, _params) do
    conn
    |> render_error(400,
      code: "invalid_provider",
      message: "selected provider is invalid"
    )
  end
end
