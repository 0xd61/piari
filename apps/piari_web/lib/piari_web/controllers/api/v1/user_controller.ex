defmodule PiariWeb.Api.V1.UserController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias Piari.Notifications
  alias PiariWeb.AuthHelpers

  plug :fetch_user

  plug :authorize,
       [action: "read", funs: &AuthHelpers.user_access/3]
       when action in [:show]

  plug :authorize,
       [action: "write", funs: &AuthHelpers.user_access/3]
       when action in [:update, :delete]

  def show(conn, _params) do
    user = conn.assigns.user
    render(conn, "show.json", user: user)
  end

  def update_email(conn, %{"code" => token, "email" => email}) do
    with {:ok, %{email: email, user_id: user_id}} <-
           Accounts.authenticate(
             :email,
             [code: token],
             email: email,
             redirect_uri: Routes.api_v1_user_url(conn, :update_email)
           ),
         {:ok, user} <-
           user_id
           |> Accounts.get_user!()
           |> Accounts.update_user_email(email) do
      render(conn, "update.json", user: user)
    else
      _ ->
        render_error(conn, 400)
    end
  end

  def update(conn, %{"params" => user_params}) do
    user = conn.assigns.user

    notify_on_email_change(conn, user, user_params)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        render(conn, "update.json", user: user, params: user_params)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  defp notify_on_email_change(conn, user, %{"email" => email}) do
    if Accounts.user_has_email?(user, email) === false do
      url =
        Accounts.auth_url(:email,
          redirect_uri: Routes.api_v1_user_url(conn, :update_email),
          email: email,
          user_id: user.id
        )

      [url: url]
      |> Notifications.Email.template(:verify_email,
        locale: Gettext.get_locale(PiariWeb.Gettext)
      )
      |> Notifications.Email.deliver(%{name: user.name, email: email},
        subject: gettext("Verify Your New Email")
      )
    end
  end

  def delete(conn, _params) do
    user = conn.assigns.user

    case Accounts.delete_user(user) do
      {:ok, _user} ->
        conn
        |> AuthHelpers.logout()
        |> render("delete.json")

      {:error, :is_owner} ->
        render_error(
          conn,
          400,
          code: "business_owner",
          message:
            "You must remove yourself, or delete your businesses, before you can delete your user"
        )
    end
  end
end
