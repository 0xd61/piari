defmodule PiariWeb.Api.V1.OfferController do
  use PiariWeb, :controller
  alias PiariWeb.AuthHelpers
  alias Piari.Offers
  @max_title_length 50

  plug :fetch_business_offer

  plug :authorize,
       [action: "read", funs: &AuthHelpers.offer_access/3, allow_unconfirmed: true]
       when action in [:index]

  plug :authorize,
       [action: "write", funs: &AuthHelpers.offer_access/3, allow_unconfirmed: true]
       when action in [:create, :update, :delete]

  plug :authorize,
       [action: "publish", funs: &AuthHelpers.offer_access/3]
       when action in [:publish, :unpublish]

  def index(conn, _params) do
    business = conn.assigns.business
    offers = Offers.list_products(business)

    render(conn, "index.json", offers: offers)
  end

  # For simplicity start by uploading a base64 image string
  def create(conn, %{"params" => %{"image" => image, "meta" => meta}}) do
    business = conn.assigns.business

    offer = %{
      quantity: 1,
      price: 0.0,
      content: %{
        title:
          (meta["filename"] || "")
          |> Path.basename(".jpg")
          |> Path.basename(".png")
          |> String.split_at(@max_title_length)
          |> elem(0)
      }
    }

    case Offers.create_product(business, image, offer) do
      {:ok, product} ->
        render(conn, "create.json", offer: product)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def update(conn, %{"params" => offer_params}) do
    offer = conn.assigns.offer

    case Offers.update_product(offer, offer_params) do
      {:ok, offer} ->
        render(conn, "create.json", offer: offer)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def publish(conn, %{"days" => days}) do
    offer =
      conn.assigns.offer
      |> Offers.publish_product!(days)

    render(conn, "publish.json", offer: offer)
  end

  def unpublish(conn, _params) do
    offer =
      conn.assigns.offer
      |> Offers.unpublish_product!()

    render(conn, "unpublish.json", offer: offer)
  end

  def delete(conn, _params) do
    offer = conn.assigns.offer

    case Offers.delete_product(offer) do
      {:ok, _} ->
        render(conn, "delete.json", offer: offer)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end
end
