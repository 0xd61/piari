defmodule PiariWeb.Api.V1.SearchController do
  use PiariWeb, :controller
  alias Search.Query

  @result_size 50

  def index(conn, %{"q" => query, "c" => categories} = params) do
    offset = Map.get(params, "offset", "0")
    index_handler(conn, query, categories, offset)
  end

  def index(conn, %{"q" => query} = params) do
    offset = Map.get(params, "offset", "0")
    index_handler(conn, query, "", offset)
  end

  defp index_handler(conn, query, categories, offset)
       when is_binary(query) and is_binary(categories) do
    query = query <> " " <> categories

    categories =
      case categories |> String.split(" ") do
        [""] -> []
        categories -> categories
      end

    offset = String.to_integer(offset)

    results =
      query
      |> search(offset, @result_size)
      |> Map.put_new(:next_offset, offset + @result_size)
      |> Map.put_new(:query, query)
      |> Map.put_new(:categories, categories)

    render(conn, "index.json", results)
  end

  defp search(query, offset, size) when is_integer(offset) and is_integer(size) do
    [result] =
      Piari.Searches.Offers
      |> Query.on()
      |> Query.where(default: true, size: size, offset: offset)
      |> Query.search(query)
      |> Piari.Searches.search()

    result
  end
end
