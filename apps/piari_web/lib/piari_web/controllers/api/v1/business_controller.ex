defmodule PiariWeb.Api.V1.BusinessController do
  use PiariWeb, :controller
  alias Piari.Accounts
  alias PiariWeb.AuthHelpers

  plug :fetch_business

  plug :authorize when action in [:index, :create]

  plug :authorize,
       [action: "write", funs: &AuthHelpers.business_access/3, allow_unconfirmed: true]
       when action in [:update, :delete]

  def index(conn, _params) do
    user = conn.assigns.current_user
    businesses = Accounts.list_user_businesses(user)
    render(conn, "index.json", businesses: businesses)
  end

  def create(conn, %{"params" => business_params}) do
    user = conn.assigns.current_user

    case Accounts.create_business(user, business_params) do
      {:ok, business} ->
        render(conn, "create.json", business: business)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def update(conn, %{"params" => business_params}) do
    business = conn.assigns.business

    case Accounts.update_business(business, business_params) do
      {:ok, business} ->
        render(conn, "update.json", business: business)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end

  def delete(conn, _params) do
    business = conn.assigns.business

    case Accounts.delete_business(business) do
      {:ok, business} ->
        render(conn, "delete.json", business: business)

      {:error, %Ecto.Changeset{} = changeset} ->
        changeset_error(conn, changeset)
    end
  end
end
