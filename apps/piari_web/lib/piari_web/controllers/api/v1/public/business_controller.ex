defmodule PiariWeb.Api.V1.Public.BusinessController do
  use PiariWeb, :controller
  alias Piari.Accounts

  def show(conn, %{"business" => id}) do
    business = Accounts.get_business!(id)
    render(conn, "show.json", business: business)
  end
end
