defmodule PiariWeb.Api.V1.Public.OfferController do
  use PiariWeb, :controller
  alias Piari.Offers

  plug :fetch_business when action in [:index]

  def index(conn, _params) do
    offers =
      conn.assigns.business
      |> Offers.list_public_products([:image, :attachments])

    render(conn, "index.json", offers: offers)
  end

  def show(conn, %{"offer" => id}) do
    offer = Offers.get_public_product!(id, [:image, :attachments])
    render(conn, "show.json", offer: offer)
  end
end
