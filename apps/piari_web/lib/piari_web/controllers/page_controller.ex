defmodule PiariWeb.PageController do
  use PiariWeb, :controller

  def index(conn, _params) do
    render(conn)
  end
end
