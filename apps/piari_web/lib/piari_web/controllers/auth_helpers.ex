defmodule PiariWeb.AuthHelpers do
  import Plug.Conn
  import PiariWeb.ControllerHelpers
  alias Piari.Accounts
  alias Piari.Bouncer

  def login(conn, user) do
    conn
    |> fetch_session()
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    conn
    |> fetch_session()
    |> configure_session(drop: true)
  end

  def usage_info(conn) do
    %{
      used_at: DateTime.utc_now(),
      ip: conn.remote_ip,
      user_agent: conn.assigns.user_agent
    }
  end

  def authenticate(conn, _opts) do
    cond do
      user = conn.assigns[:current_user] ->
        {:ok, %{user: user}}

      key = fetch_key(conn) ->
        key_auth(key, conn)

      true ->
        {:error, :missing}
    end
  end

  defp fetch_key(conn) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> key] ->
        key

      _ ->
        nil
    end
  end

  defp key_auth(key, conn) do
    case Accounts.key_auth(key, usage_info(conn)) do
      {:ok, result} -> {:ok, result}
      :error -> {:error, :key}
      :revoked -> {:error, :revoked_key}
    end
  end

  def error(conn, error) do
    case error do
      {:error, :missing} ->
        unauthorized(conn, "missing authentication information")

      {:error, :invalid} ->
        unauthorized(conn, "invalid authentication information")

      {:error, :expired} ->
        unauthorized(conn, "expired authentication information")

      {:error, :phone_unverified} ->
        forbidden(conn, "unverified phone number")

      {:error, :key} ->
        unauthorized(conn, "invalid API key")

      {:error, :revoked_key} ->
        unauthorized(conn, "API key revoked")

      {:error, :resource} ->
        forbidden(conn, "cannot access resource")

      {:error, :domain} ->
        forbidden(conn, "key not authorized for this action")
    end
  end

  def unauthorized(conn, reason) do
    render_error(conn, 401, code: "unauthorized", message: reason)
  end

  def forbidden(conn, reason) do
    render_error(conn, 403, code: "forbidden", message: reason)
  end

  def authorize(conn, actor, opts) do
    action = Keyword.get(opts, :action)
    funs = Keyword.get(opts, :funs) |> List.wrap()

    cond do
      not verified_business?(conn.assigns[:business], opts) ->
        error(conn, {:error, :phone_unverified})

      funs ->
        Enum.find_value(funs, fn fun ->
          case fun.(conn, actor, action) do
            true -> nil
            false -> error(conn, {:error, :resource})
          end
        end) || conn

      true ->
        conn
    end
  end

  defp verified_business?(nil, _opts) do
    true
  end

  defp verified_business?(%Piari.Accounts.Business{} = business, opts) do
    allow_unconfirmed = Keyword.get(opts, :allow_unconfirmed, false)
    allow_unconfirmed || (business && business.verified)
  end

  def user_access(%Plug.Conn{} = conn, user, action) do
    user_access(conn.assigns.user, user, action)
  end

  def user_access(%Piari.Accounts.User{} = user, current_user, action) do
    Bouncer.can?(current_user, action, Piari.Accounts.Users, user)
  end

  def business_access(%Plug.Conn{} = conn, user, action) do
    business_access(conn.assigns.business, user, action)
  end

  def business_access(%Piari.Accounts.Business{} = business, user, action) do
    Bouncer.can?(user, action, Piari.Accounts.Businesses, business)
  end

  def offer_access(%Plug.Conn{} = conn, user, action) do
    offer_access(%{business: conn.assigns.business, offer: conn.assigns.offer}, user, action)
  end

  def offer_access(%{business: _business, offer: _offer} = data, user, action) do
    Bouncer.can?(user, action, Piari.Offers, data)
  end
end
