defmodule PiariWeb.Gettext do
  @moduledoc """
  A module providing Internationalization with a gettext-based API.

  By using [Gettext](https://hexdocs.pm/gettext),
  your module gains a set of macros for translations, for example:

      import PiariWeb.Gettext

      # Simple translation
      gettext("Here is the string to translate")

      # Plural translation
      ngettext("Here is the string to translate",
               "Here are the strings to translate",
               3)

      # Domain-based translation
      dgettext("errors", "Here is the error message to translate")

  See the [Gettext Docs](https://hexdocs.pm/gettext) for detailed usage.
  """
  use Gettext, otp_app: :piari_web
  import Plug.Conn, only: [get_req_header: 2]

  def supported_locales do
    known = Gettext.known_locales(PiariWeb.Gettext)

    allowed = Piari.Translations.list_languages()

    MapSet.intersection(Enum.into(known, MapSet.new()), Enum.into(allowed, MapSet.new()))
    |> MapSet.to_list()
  end

  def extract_locale(conn) do
    # Prioritise locale in session if set
    conn
    |> extract_accept_language()
    # Filter for only known locales
    |> Enum.filter(fn locale -> Enum.member?(supported_locales(), locale) end)
  end

  defp extract_accept_language(conn) do
    case conn |> get_req_header("accept-language") do
      [value | _] ->
        value
        |> String.split(",")
        |> Enum.map(&parse_language_option/1)
        |> Enum.sort(&(&1.quality > &2.quality))
        |> Enum.map(& &1.tag)

      _ ->
        []
    end
  end

  defp parse_language_option(string) do
    captures =
      ~r/^(?<tag>[\w\-]+)(?:;q=(?<quality>[\d\.]+))?$/i
      |> Regex.named_captures(string)

    quality =
      case Float.parse(captures["quality"] || "1.0") do
        {val, _} -> val
        _ -> 1.0
      end

    %{tag: captures["tag"], quality: quality}
  end
end
