defmodule PiariWeb.Api.V1.SessionView do
  use PiariWeb, :view

  def render("create.json", %{url: url, provider: provider}) do
    %{url: url, provider: provider}
  end
end
