defmodule PiariWeb.Api.V1.UserView do
  use PiariWeb, :view
  @derive Jason.Encoder
  defstruct [:id, :email, :name, :locale, :timezone]

  def render("show.json", %{user: user}) do
    render_one(user, __MODULE__, "user.json")
  end

  def render("update.json", %{user: user}) do
    render_one(user, __MODULE__, "user.json")
  end

  def render("delete.json", _assigns) do
    %{success: true}
  end

  def render("user.json", %{user: %Piari.Accounts.User{} = user}) do
    attrs = Map.from_struct(user)

    struct(__MODULE__, attrs)
  end
end
