defmodule PiariWeb.Api.V1.AuthView do
  use PiariWeb, :view

  def render("index.json", %{auths: auths, user: user}) do
    %{user_id: user.id, auths: render_many(auths, __MODULE__, "auth.json")}
  end

  def render("delete.json", _params) do
    %{success: true}
  end

  def render("auth.json", %{auth: %Piari.Accounts.Auth{} = auth}) do
    %{id: auth.id, provider: auth.provider}
  end
end
