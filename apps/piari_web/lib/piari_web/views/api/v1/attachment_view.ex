defmodule PiariWeb.Api.V1.AttachmentView do
  use PiariWeb, :view
  @derive Jason.Encoder
  defstruct [:uri, :size, :hash, :content_type]

  def render("attachment.json", %{attachment: %Piari.Offers.Attachment{} = attachment}) do
    attrs = Map.from_struct(attachment)
    struct(__MODULE__, attrs)
  end
end
