defmodule PiariWeb.Api.V1.SearchView do
  use PiariWeb, :view
  @derive Jason.Encoder
  defstruct [:index, :time, :total, :next_offset, :query, :categories, :data]

  def render("index.json", assigns) do
    struct(__MODULE__, assigns)
    |> Map.put(:data, render_many(assigns.data, __MODULE__, "result.json"))
  end

  def render("result.json", %{search: %Piari.Offers.Product{} = result}) do
    %{
      id: result.id,
      slug: result.slug,
      title: :erlang.float_to_binary(result.price, decimals: 2),
      subtitle: Piari.Translations.localize(result.content, "en", :title),
      quantity: result.quantity,
      image: result.image && result.image.uri
    }
  end

  def render("result.json", _fallback) do
    %{}
  end
end
