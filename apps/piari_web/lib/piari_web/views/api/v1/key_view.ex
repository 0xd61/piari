defmodule PiariWeb.Api.V1.KeyView do
  use PiariWeb, :view

  def render("create.json", %{token: token, user: user}) do
    %{token: token, user_id: user.id}
  end

  def render("delete.json", _params) do
    %{success: true}
  end
end
