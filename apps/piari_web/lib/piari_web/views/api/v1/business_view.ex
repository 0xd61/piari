defmodule PiariWeb.Api.V1.BusinessView do
  use PiariWeb, :view
  @derive Jason.Encoder
  defstruct [:id, :name, :phone, :verified, :slug]

  def render("index.json", %{businesses: businesses}) do
    render_many(businesses, __MODULE__, "business.json")
  end

  def render("create.json", %{business: business}) do
    render_one(business, __MODULE__, "business.json")
  end

  def render("update.json", %{business: business}) do
    render_one(business, __MODULE__, "business.json")
  end

  def render("delete.json", _params) do
    %{success: true}
  end

  def render("business.json", %{business: %Piari.Accounts.Business{} = business}) do
    attrs = Map.from_struct(business)

    struct(__MODULE__, attrs)
  end
end
