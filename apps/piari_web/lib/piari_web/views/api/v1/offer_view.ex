defmodule PiariWeb.Api.V1.OfferView do
  use PiariWeb, :view
  alias PiariWeb.Api.V1.AttachmentView
  @derive Jason.Encoder
  defstruct [
    :id,
    :title,
    :price,
    :quantity,
    :expires,
    :slug,
    :description,
    :image,
    :attachments,
    :draft
  ]

  def render("index.json", %{offers: offers}) do
    render_many(offers, __MODULE__, "offer.json")
  end

  def render("create.json", %{offer: offer}) do
    render_one(offer, __MODULE__, "offer.json")
  end

  def render("update.json", %{offer: offer}) do
    render_one(offer, __MODULE__, "offer.json")
  end

  def render("publish.json", %{offer: offer}) do
    render_one(offer, __MODULE__, "offer.json")
  end

  def render("unpublish.json", %{offer: offer}) do
    render_one(offer, __MODULE__, "offer.json")
  end

  def render("delete.json", _params) do
    %{success: true}
  end

  def render("offer.json", %{
        offer: %Piari.Offers.Product{content: content} = offer
      }) do
    attrs =
      Map.from_struct(offer)
      |> Map.put(:title, content.title)
      |> Map.put(:description, content.description)
      |> Map.put(:image, render_one(offer.image, AttachmentView, "attachment.json"))
      |> Map.put(:attachments, render_many(offer.attachments, AttachmentView, "attachment.json"))

    struct(__MODULE__, attrs)
  end
end
