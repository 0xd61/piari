defmodule PiariWeb.Api.V1.Public.BusinessView do
  use PiariWeb, :view
  alias PiariWeb.Api.V1.BusinessView, as: Business

  def render("show.json", %{business: business}) do
    render_one(business, __MODULE__, "business.json")
  end

  def render("business.json", %{business: business}) do
    business
    |> render_one(Business, "business.json")
    |> Map.take(~w(id name phone slug)a)
  end
end
