defmodule PiariWeb.Api.V1.Public.OfferView do
  use PiariWeb, :view
  alias PiariWeb.Api.V1.OfferView, as: Offer

  def render("index.json", %{offers: offers}) do
    render_many(offers, __MODULE__, "offer.json")
  end

  def render("show.json", %{offer: offer}) do
    render_one(offer, __MODULE__, "offer.json")
  end

  def render("offer.json", %{offer: offer}) do
    offer
    |> render_one(Offer, "offer.json")
    |> Map.take(~w(id title price quantity expires slug description image attachments)a)
  end
end
