defmodule PiariWeb.Plugs do
  import Plug.Conn
  alias PiariWeb.AuthHelpers, as: Auth
  alias Piari.Accounts
  alias PiariWeb.Gettext, as: Locale

  def user_agent(conn, _opts) do
    case get_req_header(conn, "user-agent") do
      [value | _] ->
        assign(conn, :user_agent, value)

      [] ->
        assign(conn, :user_agent, "missing")
    end
  end

  def login_session(conn, _opts) do
    user_id = get_session(conn, :user_id)

    cond do
      user = conn.assigns[:current_user] ->
        assign(conn, :current_user, user)

      user = user_id && Accounts.get_user!(user_id, :businesses) ->
        assign(conn, :current_user, user)

      true ->
        assign(conn, :current_user, nil)
    end
  end

  def authenticate(conn, opts) do
    case Auth.authenticate(conn, opts) do
      {:ok, %{key: key, user: user}} ->
        conn
        |> assign(:key, key)
        |> assign(:current_user, user)

      {:ok, %{user: user}} ->
        conn
        |> assign(:key, nil)
        |> assign(:current_user, user)

      {:error, :missing} ->
        conn
        |> assign(:key, nil)
        |> assign(:current_user, nil)

      {:error, _} = error ->
        Auth.error(conn, error)
    end
  end

  def fetch_locale(conn, default) do
    locale = conn.params["lang"]

    cond do
      locale in Locale.supported_locales() ->
        # use if valid lang parameter exists
        conn |> assign_locale!(locale)

      locale = Map.get(fetch_cookies(conn).cookies, "locale") ->
        # use if no valid lang parameter exists
        conn |> assign_locale!(locale)

      True ->
        # use if no cookie exists
        locale = List.first(Locale.extract_locale(conn)) || default
        conn |> assign_locale!(locale)
    end
  end

  defp assign_locale!(conn, value) do
    # Apply the locale as a process var and continue

    Gettext.put_locale(Locale, value)

    conn
    |> assign(:locale, value)
    |> put_resp_cookie("locale", value, http_only: false, secure: false)
  end

  def authorize(conn, opts) do
    user = conn.assigns[:current_user]

    if user do
      Auth.authorize(conn, user, opts)
    else
      Auth.error(conn, {:error, :missing})
    end
  end
end
