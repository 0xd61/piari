defmodule PiariWeb.Router do
  @moduledoc """
    Contains all application Routes.

    To prevent CSRF Attacks, ONLY use "get" Methods for the browser scrope.
    Everything else should be handled via API.

    Browser Pipeline is authenticated via session.
    API Pipeline is authenticated via access-header.

  Signleton resources use a singular uri (e.g. session/account)
  """
  use PiariWeb, :router
  use Plug.ErrorHandler

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(:user_agent)
    plug(:fetch_locale, "en")
    plug(:login_session)
  end

  pipeline :api do
    plug(:accepts, ["json"])
    plug(:user_agent)
    plug(:fetch_locale, "en")
    plug(:authenticate)
  end

  pipeline :dashboard do
  end

  scope "/api", PiariWeb, as: :api do
    pipe_through :api

    scope "/v1", Api.V1, as: :v1 do
      get("/searches", SearchController, :index)

      post("/auths", KeyController, :create)
      delete("/auths", KeyController, :delete)

      post("/session/:provider", SessionController, :create)

      get("/users/email", UserController, :update_email)
      get("/users/:user", UserController, :show)
      post("/users/:user", UserController, :update)
      delete("/users/:user", UserController, :delete)

      get("/users/:user/auths", AuthController, :index)
      delete("/users/:user/auths/:auth", AuthController, :delete)

      get("/businesses", BusinessController, :index)
      post("/businesses", BusinessController, :create)
      post("/businesses/:business", BusinessController, :update)
      delete("/businesses/:business", BusinessController, :delete)

      get("/businesses/:business/offers", OfferController, :index)
      post("/businesses/:business/offers", OfferController, :create)
      post("/businesses/:business/offers/:offer", OfferController, :update)
      post("/businesses/:business/offers/:offer/publish", OfferController, :publish)
      post("/businesses/:business/offers/:offer/unpublish", OfferController, :unpublish)
      delete("/businesses/:business/offers/:offer", OfferController, :delete)

      scope "/public", Public, as: :public do
        get("/offers/:offer", OfferController, :show)
        get("/businesses/:business/offers", OfferController, :index)
        get("/businesses/:business", BusinessController, :show)
      end
    end
  end

  scope "/", PiariWeb do
    pipe_through(:browser)
    get("/", PageController, :index)

    get("/session/oauth/:provider", SessionController, :create)
    get("/session/:provider", SessionController, :callback)
  end

  defp handle_errors(conn, %{kind: kind, reason: reason, stack: stacktrace}) do
    if report?(kind, reason) do
      conn = maybe_fetch_params(conn)
      url = "#{conn.scheme}://#{conn.host}:#{conn.port}#{conn.request_path}"
      user_ip = conn.remote_ip |> :inet.ntoa() |> List.to_string()
      headers = conn.req_headers |> Map.new() |> filter_headers()
      params = filter_params(conn.params)
      endpoint_url = PiariWeb.Endpoint.config(:url)

      conn_data = %{
        "request" => %{
          "url" => url,
          "user_ip" => user_ip,
          "headers" => headers,
          "params" => params,
          "method" => conn.method
        },
        "server" => %{
          "host" => endpoint_url[:host],
          "root" => endpoint_url[:path]
        }
      }

      Rollbax.report(kind, reason, stacktrace, %{}, conn_data)
    end
  end

  defp report?(:error, exception), do: Plug.Exception.status(exception) == 500
  defp report?(_kind, _reason), do: true

  defp maybe_fetch_params(conn) do
    try do
      Plug.Conn.fetch_query_params(conn)
    rescue
      _ ->
        %{conn | params: "[UNFETCHED]"}
    end
  end

  @filter_headers ~w(authorization cookie)

  defp filter_headers(headers) do
    Map.drop(headers, @filter_headers)
  end

  @filter_params ~w(body)

  defp filter_params(%Plug.Upload{} = params) do
    filter_params(Map.from_struct(params))
  end

  defp filter_params(params) when is_map(params) do
    Map.new(params, fn {key, value} ->
      if key in @filter_params do
        {key, "[FILTERED]"}
      else
        {key, filter_params(value)}
      end
    end)
  end

  defp filter_params(params) when is_list(params) do
    Enum.map(params, &filter_params/1)
  end

  defp filter_params(other) do
    other
  end

  def list_routes() do
    __MODULE__.__routes__()
    |> Enum.map(&Map.take(&1, [:helper, :path]))
  end
end
