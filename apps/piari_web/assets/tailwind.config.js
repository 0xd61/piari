module.exports = {
  theme: {
    extend: {
      inset: {
        px: "1px"
      }
    }
  },
  variants: {},
  plugins: [require("@tailwindcss/custom-forms")]
};
