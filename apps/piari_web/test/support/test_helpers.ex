defmodule PiariWeb.TestHelpers do
  alias Piari.Accounts

  def user_fixture(attrs \\ %{}) do
    username = "user#{System.unique_integer([:positive])}"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: attrs[:email] || "#{username}@example.com",
        name: attrs[:name] || "#{username}",
        birthdate: attrs[:birthdate] || DateTime.utc_now(),
        auths: [
          %{
            ext_uid: attrs[:ext_uid] || "auth_#{username}",
            provider: :only_for_test
          }
        ]
      })
      |> Accounts.register_user()

    user
  end

  def business_fixture(%Accounts.User{} = user, attrs) do
    name = "business#{System.unique_integer([:positive])}"

    {:ok, business} =
      Accounts.create_business(
        user,
        attrs
        |> Enum.into(%{
          name: attrs[:name] || "#{name}",
          phone: attrs[:phone] || "0983227845",
          slug: attrs[:slug] || nil
        })
      )

    if attrs[:verified] do
      {:ok, business} = Accounts.verify_business(business, business.verify_token)
      business
    else
      business
    end
  end

  def business_fixture(attrs \\ %{}) do
    business_fixture(user_fixture(), attrs)
  end

  def product_fixture(attrs \\ %{}) do
    product_fixture(business_fixture(), attrs)
  end

  def product_fixture(%Piari.Accounts.Business{} = business, attrs) do
    productname = "product#{System.unique_integer([:positive])}"

    product = %Piari.Offers.Product{
      quantity: attrs[:quantity] || generate_random_numbers(1) |> Integer.parse() |> elem(0),
      expires: attrs[:expires] || ~U[9999-07-18 20:40:21Z],
      accounts_business_id: business.id,
      slug: attrs[:slug] || "#{productname}",
      price: attrs[:price] || generate_random_numbers(6) |> Float.parse() |> elem(0),
      draft: attrs[:draft] || false,
      content: %{
        title: attrs[:title] || "#{productname}-title",
        description: attrs[:description] || "#{productname}-description",
        translations:
          attrs[:translations] ||
            %{
              "es" => %{
                title: "#{productname}-título",
                description: "#{productname}-descripción"
              },
              "de" => %{
                title: "#{productname}-titel",
                description: "#{productname}-beschreibung"
              }
            }
      }
    }

    product = Piari.Repo.insert!(product) |> Piari.Repo.preload([:attachments, :image])

    product
  end

  defp generate_random_numbers(length) do
    # credo:disable-for-lines:2
    (:rand.uniform() * :math.pow(10, length))
    |> Kernel.trunc()
    |> to_string
    |> String.pad_leading(length, "0")
  end
end
