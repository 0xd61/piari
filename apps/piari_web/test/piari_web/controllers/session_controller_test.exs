defmodule PiariWeb.SessionControllerTest do
  use PiariWeb.ConnCase
  alias Piari.TestHelpers.Accounts.Auths.FakeAdapter
  alias Piari.Accounts
  alias PiariWeb.SessionController

  describe "create" do
    test "returns redirect to provider", %{conn: conn} do
      conn = get(conn, Routes.session_path(conn, :create, "github"))

      assert redirected_to(conn) =~
               "https://github.com/login/oauth/authorize?client_id=79ae4408b65f2e1a416a&redirect_uri=http%3A%2F%2Flocalhost%3A4002%2Fsession%2Fgithub&response_type=code&scope=user"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = get(conn, Routes.session_path(conn, :create, "unknown"))
      assert html_response(conn, 400) =~ "provider is invalid"
    end
  end

  describe "provider callback" do
    setup %{conn: conn} do
      conn =
        conn
        |> bypass_through(PiariWeb.Router, :browser)
        |> get("/")

      {:ok, %{conn: conn}}
    end

    test "handle_callback for existing user by provider and valid token", %{conn: conn} do
      user =
        user_fixture(%{
          email: FakeAdapter.user_email(),
          ext_uid: FakeAdapter.prefix_encode_id(FakeAdapter.user_email())
        })

      conn =
        Accounts.authenticate(:fakeauth, [token: FakeAdapter.auth_token()], [])
        |> SessionController.handle_callback(conn, :fakeauth)

      # returns redirect with temp token as parameter
      assert redirected_to(conn) =~
               Application.get_env(:piari_web, PiariWeb.Endpoint)[:client_callback_url]

      assert redirected_to(conn) =~
               "email=#{URI.encode(user.email, &(&1 != ?@))}"

      assert redirected_to(conn) =~
               "locale=#{URI.encode(user.locale)}"

      assert Piari.Repo.preload(conn.assigns.current_user, :auths) == user
    end

    test "handle_callback for new user by provider and valid token", %{conn: conn} do
      conn =
        Accounts.authenticate(:fakeauth, [token: FakeAdapter.auth_token()], [])
        |> SessionController.handle_callback(conn, :fakeauth)

      # returns redirect with temp token as parameter
      assert redirected_to(conn) =~
               Application.get_env(:piari_web, PiariWeb.Endpoint)[:client_callback_url]

      assert redirected_to(conn) =~
               "email=#{FakeAdapter.user_email() |> URI.encode(&(&1 != ?@))}"

      assert conn.assigns.current_user.email == FakeAdapter.user_email()
    end

    test "handle_callback for user by provider and invalid token", %{conn: conn} do
      conn =
        Accounts.authenticate(:fakeauth, [token: "invalid"], [])
        |> SessionController.handle_callback(conn, :fakeauth)

      assert html_response(conn, 400) =~ "Bad request"

      assert_raise MatchError, fn ->
        %{"temp_token" => _} = conn.cookies
      end

      refute get_session(conn, :user_id)
    end
  end
end
