defmodule PiariWeb.Api.V1.SessionControllerTest do
  use PiariWeb.ConnCase

  describe "create" do
    test "returns redirect to provider", %{conn: conn} do
      conn =
        post(conn, Routes.api_v1_session_path(conn, :create, "email"), email: "test@email.com")

      assert %{"url" => _, "provider" => "email"} = json_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        post(conn, Routes.api_v1_session_path(conn, :create, "unknown"), email: "test@email.com")

      assert %{"error" => %{"code" => "invalid_provider"}} = json_response(conn, 400)
    end
  end
end
