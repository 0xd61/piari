defmodule PiariWeb.Api.V1.AuthControllerTest do
  use PiariWeb.ConnCase, async: true
  import Plug.Test
  alias Piari.Accounts

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      user = user_fixture()

      Enum.each(
        [
          get(conn, Routes.api_v1_auth_path(conn, :index, user.id)),
          delete(conn, Routes.api_v1_auth_path(conn, :delete, user.id, 123))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users" do
    setup [:login]

    test "can only access their own user", %{conn: conn} do
      user = user_fixture()

      Enum.each(
        [
          get(conn, Routes.api_v1_auth_path(conn, :index, user.id)),
          delete(conn, Routes.api_v1_auth_path(conn, :delete, user.id, 123))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "index" do
    setup [:login]

    test "returns list of user auths", %{conn: conn, user: user} do
      conn = get(conn, Routes.api_v1_auth_path(conn, :index, user))

      assert %{"auths" => [_], "user_id" => _} = json_response(conn, 200)
    end
  end

  describe "delete" do
    setup [:login]

    test "deletes authentication method from user", %{conn: conn, user: user} do
      auth = user.auths |> List.first()
      conn = delete(conn, Routes.api_v1_auth_path(conn, :delete, user, auth.id))

      assert json_response(conn, 200) == %{"success" => true}

      assert_raise Ecto.NoResultsError, fn ->
        Accounts.get_auth!(user, auth.id)
      end
    end
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end
end
