defmodule PiariWeb.Api.V1.KeyControllerTest do
  use PiariWeb.ConnCase, async: true
  alias Piari.Accounts
  alias PiariWeb.AuthHelpers

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      Enum.each(
        [
          delete(conn, Routes.api_v1_key_path(conn, :delete))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "create" do
    setup %{conn: conn} do
      user = user_fixture()

      token =
        Phoenix.Token.sign(
          PiariWeb.Endpoint,
          Application.fetch_env!(:piari_web, PiariWeb.Endpoint)[:token_salt],
          user.id
        )

      {:ok, conn: conn, user: user, token: token}
    end

    test "auth key with valid temp token", %{conn: conn, token: token} do
      conn = post(conn, Routes.api_v1_key_path(conn, :create, temp_token: token))

      assert %{"token" => _, "user_id" => _} = json_response(conn, 200)
    end

    test "renders error with invalid temp token", %{conn: conn} do
      conn = post(conn, Routes.api_v1_key_path(conn, :create, temp_token: "invalid"))

      assert %{
               "error" => %{
                 "code" => "unauthorized",
                 "message" => _,
                 "status" => 401
               }
             } = json_response(conn, 401)
    end
  end

  describe "delete" do
    setup %{conn: conn} do
      user = user_fixture()
      {:ok, key} = Accounts.create_key(user)

      conn =
        conn
        |> put_req_header("authorization", "Bearer #{key.user_secret}")

      {:ok, conn: conn, user: user, key: key}
    end

    test "revokes user auth key", %{conn: conn, key: key} do
      conn = delete(conn, Routes.api_v1_key_path(conn, :delete))

      assert json_response(conn, 200) == %{"success" => true}
      assert Accounts.key_auth(key.user_secret, AuthHelpers.usage_info(conn)) == :revoked
    end
  end
end
