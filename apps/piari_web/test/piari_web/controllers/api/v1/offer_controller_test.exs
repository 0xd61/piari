defmodule PiariWeb.Api.V1.Offer.OfferControllerTest do
  use PiariWeb.ConnCase
  import Plug.Test
  alias Piari.Offers
  alias Piari.Accounts

  @image "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="

  @create_attrs %{
    image: @image,
    meta: %{filename: "whatever"}
  }
  @update_attrs %{
    expires: ~U[9999-07-18 20:40:21Z],
    quantity: 5,
    price: 25_000,
    content: %{
      title: "Example Title"
    }
  }
  @invalid_attrs %{expires: nil, quantity: nil}

  describe "logged out users" do
    setup [:business, :offer]

    test "requires user authentication on actions", %{
      conn: conn,
      business: business,
      offer: offer
    } do
      Enum.each(
        [
          get(conn, Routes.api_v1_offer_path(conn, :index, business)),
          post(conn, Routes.api_v1_offer_path(conn, :create, business, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :update, business, offer, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :publish, business, offer, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :unpublish, business, offer, %{})),
          delete(conn, Routes.api_v1_offer_path(conn, :delete, business, offer))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users" do
    setup [:business, :login, :offer]

    test "cannot access offers", %{conn: conn, business: business, offer: offer} do
      Enum.each(
        [
          get(conn, Routes.api_v1_offer_path(conn, :index, business)),
          post(conn, Routes.api_v1_offer_path(conn, :create, business, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :update, business, offer, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :publish, business, offer, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :unpublish, business, offer, %{})),
          delete(conn, Routes.api_v1_offer_path(conn, :delete, business, offer))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "business members" do
    setup [:business, :member, :login, :offer]

    test "can only read, publish and unpublish offers", %{
      conn: conn,
      business: business,
      offer: offer
    } do
      Enum.each(
        [
          post(conn, Routes.api_v1_offer_path(conn, :create, business, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :update, business, offer, %{})),
          delete(conn, Routes.api_v1_offer_path(conn, :delete, business, offer))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "business owners" do
    setup [:unverified_business, :owner, :login, :offer]

    test "need a verified business to publish and unpublish offers", %{
      conn: conn,
      business: business,
      offer: offer
    } do
      Enum.each(
        [
          post(conn, Routes.api_v1_offer_path(conn, :publish, business, offer, %{})),
          post(conn, Routes.api_v1_offer_path(conn, :unpublish, business, offer, %{}))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "create offer" do
    setup [:business, :owner, :login]

    test "returns created offer", %{
      conn: conn,
      business: business
    } do
      conn = post(conn, Routes.api_v1_offer_path(conn, :create, business), params: @create_attrs)
      res = json_response(conn, 200)
      assert res["title"] == @create_attrs.meta.filename
    end
  end

  describe "update offer" do
    setup [:business, :owner, :login, :offer]

    test "redirects when data is valid", %{conn: conn, offer: offer, business: business} do
      attrs = put_in(@update_attrs, [:content, :id], offer.content.id)

      conn = post(conn, Routes.api_v1_offer_path(conn, :update, business, offer), params: attrs)

      res = json_response(conn, 200)
      assert res["title"] == @update_attrs.content.title
    end

    test "renders errors when data is invalid", %{conn: conn, offer: offer, business: business} do
      conn =
        post(conn, Routes.api_v1_offer_path(conn, :update, business, offer),
          params: @invalid_attrs
        )

      assert %{
               "error" => %{
                 "errors" => %{
                   "quantity" => [_]
                 }
               }
             } = json_response(conn, 422)
    end
  end

  describe "delete offer" do
    setup [:business, :owner, :login, :offer]

    test "deletes chosen offer", %{conn: conn, offer: offer, business: business} do
      conn = delete(conn, Routes.api_v1_offer_path(conn, :delete, business, offer))

      assert %{"success" => true} = json_response(conn, 200)

      assert_error_sent(404, fn ->
        get(conn, Routes.api_v1_public_offer_path(conn, :show, offer))
      end)
    end
  end

  describe "publish offer" do
    setup [:business, :member, :login, :offer]

    test "publishes offer for a timespan", %{conn: conn, offer: offer, business: business} do
      conn = post(conn, Routes.api_v1_offer_path(conn, :publish, business, offer), days: 5)

      assert %{"draft" => false} = json_response(conn, 200)
    end

    test "raises on error", %{conn: conn, offer: offer, business: business} do
      assert_raise ArgumentError, fn ->
        post(conn, Routes.api_v1_offer_path(conn, :publish, business, offer), days: "abc")
      end
    end

    test "unpublishes offer", %{conn: conn, offer: offer, business: business} do
      Offers.publish_product!(offer, 5)
      conn = post(conn, Routes.api_v1_offer_path(conn, :unpublish, business, offer))

      assert %{"draft" => true} = json_response(conn, 200)
    end
  end

  def unverified_business(_) do
    user = user_fixture()

    business =
      business_fixture(user, %{slug: "user_business", phone: "+123986548766", verified: false})

    {:ok, business: business}
  end

  def business(_) do
    user = user_fixture()

    business =
      business_fixture(user, %{slug: "test_business", phone: "+123986548765", verified: true})

    {:ok, business: business}
  end

  def login(%{conn: conn, user: user}) do
    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end

  def offer(%{business: business}) do
    {:ok, offer} = Offers.create_product(business, @image, @update_attrs)
    {:ok, offer: offer}
  end

  def owner(%{business: business}) do
    user = user_fixture()
    {:ok, member} = Accounts.add_member_to_business(business, user, %{role: :owner})
    Accounts.confirm_business_member(member)
    {:ok, user: user}
  end

  def member(%{business: business}) do
    user = user_fixture()
    {:ok, member} = Accounts.add_member_to_business(business, user, %{role: :member})
    Accounts.confirm_business_member(member)
    {:ok, user: user}
  end
end
