defmodule PiariWeb.Api.V1.Public.BusinessControllerTest do
  use PiariWeb.ConnCase

  describe "show" do
    setup [:business]

    test "renders public business", %{conn: conn, business: business} do
      conn = get(conn, Routes.api_v1_public_business_path(conn, :show, business))
      response = json_response(conn, 200)

      assert response["id"] == business.id
      assert response["name"] == business.name
      assert response["phone"] == business.phone
      assert response["slug"] == business.slug
    end
  end

  def business(_) do
    user = user_fixture()
    business = business_fixture(user, %{phone: "+123986548765"})
    {:ok, business: business}
  end
end
