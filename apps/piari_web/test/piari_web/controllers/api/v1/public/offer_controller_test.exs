defmodule PiariWeb.Api.V1.Public.OfferControllerTest do
  use PiariWeb.ConnCase

  describe "index" do
    setup [:business, :offer]

    test "renders public business offers", %{conn: conn, business: business, offer: offer} do
      conn = get(conn, Routes.api_v1_public_offer_path(conn, :index, business))
      assert [resp] = json_response(conn, 200)
      assert resp["id"] == offer.id
    end
  end

  describe "show" do
    setup [:offer]

    test "renders public offer", %{conn: conn, offer: offer} do
      conn = get(conn, Routes.api_v1_public_offer_path(conn, :show, offer))
      response = json_response(conn, 200)

      assert response["id"] == offer.id
      assert response["title"] == offer.content.title
      assert response["draft"] == nil
      assert response["slug"] == offer.slug
      assert response["image"] == offer.image
    end
  end

  def offer(%{business: business}) do
    offer = product_fixture(business, %{})
    {:ok, offer: offer}
  end

  def offer(_) do
    offer = product_fixture()
    {:ok, offer: offer}
  end

  def business(_) do
    business = business_fixture(phone: "0654987852")
    {:ok, business: business}
  end
end
