defmodule PiariWeb.Api.V1.SearchControllerTest do
  use PiariWeb.ConnCase, async: true
  alias Piari.Accounts

  describe "index" do
    setup [:authenticate]

    test "show search results", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.api_v1_search_path(conn, :index, q: "result", c: "")
        )

      assert %{"categories" => [], "data" => [_]} = json_response(conn, 200)
    end

    test "search with categories", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.api_v1_search_path(conn, :index, q: "result", c: "cat1 cat2", offset: "10")
        )

      assert %{"categories" => ["cat1", "cat2"], "data" => [_]} = json_response(conn, 200)
    end
  end

  defp authenticate(%{conn: conn}) do
    user = user_fixture()
    {:ok, key} = Accounts.create_key(user)

    conn =
      conn
      |> assign(:current_user, user)
      |> put_req_header("authorization", key.user_secret)

    {:ok, conn: conn, user: user, key: key}
  end
end
