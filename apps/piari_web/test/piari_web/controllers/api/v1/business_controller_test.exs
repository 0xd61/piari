defmodule PiariWeb.Api.V1.BusinessControllerTest do
  use PiariWeb.ConnCase
  import Plug.Test
  alias Piari.Accounts

  @create_attrs %{name: "some name", phone: "0228654684"}
  @update_attrs %{name: "some updated name", phone: "0365284965"}
  @invalid_attrs %{name: nil, phone: nil, verified: nil}

  describe "logged out users" do
    setup [:business]

    test "requires user authentication on actions", %{conn: conn, business: business} do
      Enum.each(
        [
          get(conn, Routes.api_v1_business_path(conn, :index)),
          post(conn, Routes.api_v1_business_path(conn, :create, %{})),
          post(conn, Routes.api_v1_business_path(conn, :update, business, %{})),
          delete(conn, Routes.api_v1_business_path(conn, :delete, business))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users" do
    setup [:business, :login]

    test "can only read business info or create new business", %{conn: conn, business: business} do
      Enum.each(
        [
          post(conn, Routes.api_v1_business_path(conn, :update, business, %{})),
          delete(conn, Routes.api_v1_business_path(conn, :delete, business))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "business members" do
    setup [:business, :member, :login]

    test "can only read business info", %{conn: conn, business: business} do
      Enum.each(
        [
          post(conn, Routes.api_v1_business_path(conn, :update, business, %{})),
          delete(conn, Routes.api_v1_business_path(conn, :delete, business))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "index" do
    setup [:business, :member, :login]

    test "lists all businesses of logged in user", %{conn: conn, business: business} do
      conn = get(conn, Routes.api_v1_business_path(conn, :index))

      assert json_response(conn, 200) == [
               %{
                 "id" => business.id,
                 "name" => business.name,
                 "phone" => business.phone,
                 "slug" => business.slug,
                 "verified" => business.verified
               }
             ]
    end
  end

  describe "create business" do
    setup [:login]

    test "renders business when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_v1_business_path(conn, :create), params: @create_attrs)

      business = json_response(conn, 200)
      assert business["name"] == @create_attrs.name
      assert business["phone"] == @create_attrs.phone
      assert business["slug"] == @create_attrs.name |> String.replace(" ", "-")
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_v1_business_path(conn, :create), params: @invalid_attrs)

      assert %{
               "error" => %{
                 "errors" => %{
                   "name" => [_],
                   "phone" => [_],
                   "slug" => [_]
                 }
               }
             } = json_response(conn, 422)
    end
  end

  describe "update business" do
    setup [:business, :owner, :login]

    test "updates user business with valid data", %{conn: conn, business: business} do
      conn =
        post(conn, Routes.api_v1_business_path(conn, :update, business), params: @update_attrs)

      business = json_response(conn, 200)
      assert business["name"] == @update_attrs.name
      assert business["phone"] == @update_attrs.phone
      assert business["verified"] == false
    end

    test "renders errors with invalid data", %{conn: conn, business: business} do
      conn =
        post(conn, Routes.api_v1_business_path(conn, :update, business), params: @invalid_attrs)

      assert %{
               "error" => %{
                 "errors" => %{
                   "name" => [_],
                   "phone" => [_]
                 }
               }
             } = json_response(conn, 422)
    end
  end

  describe "delete business" do
    setup [:business, :owner, :login]

    test "deletes chosen business", %{conn: conn, business: business} do
      conn = delete(conn, Routes.api_v1_business_path(conn, :delete, business))

      assert %{"success" => true} = json_response(conn, 200)

      assert_error_sent(404, fn ->
        get(conn, Routes.api_v1_public_business_path(conn, :show, business))
      end)
    end
  end

  def login(%{conn: conn, user: user}) do
    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end

  def business(_) do
    user = user_fixture()
    business = business_fixture(user, %{phone: "+123986548765", verified: false})
    {:ok, business: business}
  end

  def owner(%{business: business}) do
    user = user_fixture()
    {:ok, member} = Accounts.add_member_to_business(business, user, %{role: :owner})
    Accounts.confirm_business_member(member)
    {:ok, user: user}
  end

  def member(%{business: business}) do
    user = user_fixture()
    {:ok, member} = Accounts.add_member_to_business(business, user, %{role: :member})
    Accounts.confirm_business_member(member)
    {:ok, user: user}
  end
end
