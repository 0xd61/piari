defmodule PiariWeb.Api.V1.UserControllerTest do
  use PiariWeb.ConnCase
  use Bamboo.Test, shared: true
  import Plug.Test
  alias Piari.Accounts

  @auth %{ext_uid: "testauth", provider: :test_auth}
  @update_attrs %{email: "anotherone@example.com", name: "some name"}
  @invalid_attrs %{birthdate: nil, email: "invalid email", name: nil, auths: [@auth]}

  describe "logged out users" do
    test "requires user authentication on actions", %{conn: conn} do
      user = user_fixture()

      Enum.each(
        [
          get(conn, Routes.api_v1_user_path(conn, :show, user.id)),
          post(conn, Routes.api_v1_user_path(conn, :update, user.id, %{})),
          delete(conn, Routes.api_v1_user_path(conn, :delete, user.id))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "unauthorized",
                     "message" => _,
                     "status" => 401
                   }
                 } = json_response(conn, 401)

          assert conn.halted
        end
      )
    end
  end

  describe "logged in users" do
    setup [:login]

    test "can only access their own user", %{conn: conn} do
      user = user_fixture()

      Enum.each(
        [
          get(conn, Routes.api_v1_user_path(conn, :show, user.id)),
          post(conn, Routes.api_v1_user_path(conn, :update, user.id, %{})),
          delete(conn, Routes.api_v1_user_path(conn, :delete, user.id))
        ],
        fn conn ->
          assert %{
                   "error" => %{
                     "code" => "forbidden",
                     "message" => _,
                     "status" => 403
                   }
                 } = json_response(conn, 403)

          assert conn.halted
        end
      )
    end
  end

  describe "show user" do
    setup [:login]

    test "return user", %{conn: conn, user: user} do
      conn = get(conn, Routes.api_v1_user_path(conn, :show, user))
      assert %{} = json_response(conn, 200)
    end
  end

  describe "update user" do
    setup [:login]

    test "return updated user", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :update, user), params: @update_attrs)
      assert %{} = json_response(conn, 200)
    end

    test "update does not update address and sends email to update", %{conn: conn, user: user} do
      _conn = post(conn, Routes.api_v1_user_path(conn, :update, user), params: @update_attrs)
      assert user.email == Accounts.get_user!(user.id).email
      assert_email_delivered_with(subject: "Verify Your New Email")
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = post(conn, Routes.api_v1_user_path(conn, :update, user), params: @invalid_attrs)
      assert %{"error" => %{"errors" => %{}}} = json_response(conn, 422)
    end
  end

  describe "email update callback" do
    test "update with valid token updates email address", %{conn: conn} do
      user = user_fixture()

      url =
        Accounts.auth_url(:email,
          redirect_uri: Routes.api_v1_user_url(PiariWeb.Endpoint, :update_email),
          email: "mynew@email.com",
          user_id: user.id
        )

      assert user.email != "mynew@email.com"
      _conn = get(conn, url)
      user = Accounts.get_user!(user.id)
      assert user.email == "mynew@email.com"
    end

    test "update with invalid token does nothing", %{conn: conn} do
      user = user_fixture()

      url =
        Routes.api_v1_user_url(PiariWeb.Endpoint, :update_email) <>
          "/?code=invalidToken&email=someemail"

      _conn = get(conn, url)
      user_new = Accounts.get_user!(user.id)
      assert user_new.email == user.email
    end
  end

  describe "delete user" do
    setup [:login, :business]

    test "deletes current user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.api_v1_user_path(conn, :delete, user))

      assert %{"success" => true} = json_response(conn, 200)

      assert private:
               %Plug.Conn{
                 private: %{
                   plug_session_info: :drop
                 }
               } = conn
    end

    test "renders error if current user is a business owner", %{
      conn: conn,
      user: user,
      business: business
    } do
      _member = Piari.Accounts.add_member_to_business(business, user, %{role: :owner})
      conn = delete(conn, Routes.api_v1_user_path(conn, :delete, user))
      assert %{"error" => %{}} = json_response(conn, 400)
      assert conn.assigns.current_user == user
    end
  end

  def login(%{conn: conn}) do
    user = user_fixture()

    conn =
      conn
      |> init_test_session(user_id: user.id)
      |> assign(:current_user, user)

    {:ok, conn: conn, user: user}
  end

  def business(_) do
    user = user_fixture()
    business = business_fixture(user, %{})

    {:ok, business: business}
  end
end
