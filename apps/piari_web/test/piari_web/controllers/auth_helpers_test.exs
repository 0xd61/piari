defmodule PiariWeb.AuthHelpersTest do
  use PiariWeb.ConnCase, async: true
  alias PiariWeb.AuthHelpers
  alias Piari.Accounts

  setup %{conn: conn} do
    conn =
      conn
      |> bypass_through(PiariWeb.Router, :browser)
      |> get("/")

    {:ok, %{conn: conn}}
  end

  describe "login/2" do
    setup [:user]

    test "puts the user in the session", %{conn: conn, user: user} do
      login_conn =
        conn
        |> AuthHelpers.login(user)
        |> send_resp(:ok, "")

      next_conn = get(login_conn, "/")
      assert get_session(next_conn, :user_id) == user.id
    end
  end

  describe "logout/1" do
    setup [:user]

    test "drops the session", %{conn: conn, user: user} do
      logout_conn =
        conn
        |> put_session(:user_id, user.id)
        |> AuthHelpers.logout()
        |> send_resp(:ok, "")

      next_conn = get(logout_conn, "/")
      refute get_session(next_conn, :user_id)
    end
  end

  describe "usage_info/1" do
    test "parses user info from conn", %{conn: conn} do
      conn = get(conn, "/")

      assert %{
               ip: {127, 0, 0, 1},
               used_at: _,
               user_agent: "missing"
             } = AuthHelpers.usage_info(conn)
    end
  end

  describe "authenticate/2" do
    setup [:user, :key]

    test "returns user if already assigned", %{conn: conn, user: user} do
      assert {:ok, %{user: ^user}} =
               conn
               |> Plug.Conn.assign(:current_user, user)
               |> AuthHelpers.authenticate([])
    end

    test "returns user and auth information", %{conn: conn, user: user, key: key} do
      assert {:ok, response} =
               conn
               |> Plug.Conn.put_req_header("authorization", "Bearer #{key.user_secret}")
               |> AuthHelpers.authenticate([])

      assert response.key.id === key.id
      assert response.user.id === user.id
    end

    test "returns error on invalid key", %{conn: conn} do
      assert {:error, :key} =
               conn
               |> Plug.Conn.put_req_header("authorization", "Bearer invalid")
               |> AuthHelpers.authenticate([])
    end

    test "returns error on revoked key", %{conn: conn, key: key} do
      Accounts.revoke_key(key)

      assert {:error, :revoked_key} =
               conn
               |> Plug.Conn.put_req_header("authorization", "Bearer #{key.user_secret}")
               |> AuthHelpers.authenticate([])
    end

    test "returns error if no header exists", %{conn: conn} do
      assert {:error, :missing} =
               conn
               |> AuthHelpers.authenticate([])

      assert {:error, :missing} =
               conn
               |> Plug.Conn.put_req_header("authorization", "something else")
               |> AuthHelpers.authenticate([])
    end
  end

  describe "error handler" do
    test "error/2 renders error based on error tuple", %{conn: conn} do
      err = AuthHelpers.error(conn, {:error, :missing})
      assert html_response(err, 401) =~ "missing authentication information"

      err = AuthHelpers.error(conn, {:error, :invalid})
      assert html_response(err, 401) =~ "invalid authentication information"

      err = AuthHelpers.error(conn, {:error, :key})
      assert html_response(err, 401) =~ "invalid API key"

      err = AuthHelpers.error(conn, {:error, :revoked_key})
      assert html_response(err, 401) =~ "API key revoked"
    end

    test "unauthorized/2 renders unauthorized error", %{conn: conn} do
      err = AuthHelpers.unauthorized(conn, "test message")
      assert html_response(err, 401) =~ "test message"
    end

    test "forbidden/2 renders forbidden error", %{conn: conn} do
      err = AuthHelpers.forbidden(conn, "test message")
      assert html_response(err, 403) =~ "test message"
    end
  end

  describe "authorize/3" do
    setup [:user, :business]

    test "checks if user can access resources", %{conn: conn, user: user} do
      conn = AuthHelpers.authorize(conn, user, action: "authorized", funs: &access/3)
      refute conn.halted
    end

    test "runs multiple authorization functions", %{conn: conn, user: user} do
      conn =
        AuthHelpers.authorize(conn, user,
          action: "authorized",
          funs: [&access/3, &no_access/3, &access/3]
        )

      assert html_response(conn, 403) =~ "forbidden"
      assert conn.halted
    end

    test "renders error if user has no access", %{conn: conn, user: user} do
      conn = AuthHelpers.authorize(conn, user, action: "not authorized", funs: &no_access/3)
      assert html_response(conn, 403) =~ "forbidden"
      assert conn.halted
    end

    test "returns error if business is not verified", %{
      conn: conn,
      business: business,
      user: user
    } do
      conn =
        conn
        |> assign(:business, business)
        |> AuthHelpers.authorize(user, action: "access", funs: &access/3)

      assert html_response(conn, 403) =~ "forbidden"
      assert conn.halted
    end

    defp access(_conn, _actor, _action) do
      true
    end

    defp no_access(_conn, _actor, _action) do
      false
    end
  end

  describe "access functions" do
    setup [:user]

    test "user_access/3 returns true if user can access the fetched user", %{
      conn: conn,
      user: user
    } do
      conn = Plug.Conn.assign(conn, :user, user)
      assert AuthHelpers.user_access(conn, user, "write") === true
    end

    test "user_access/3 returns false if user can not access the fetched user", %{
      conn: conn,
      user: user
    } do
      other_user = user_fixture()
      conn = Plug.Conn.assign(conn, :user, other_user)
      assert AuthHelpers.user_access(conn, user, "write") === false
    end

    test "business_access/3 returns true if user can access the business", %{
      conn: conn,
      user: user
    } do
      business = business_fixture(user, %{})

      conn = Plug.Conn.assign(conn, :business, business)
      assert AuthHelpers.business_access(conn, user, "write") === true
    end

    test "business_access/3 returns false if user can not access the business", %{
      conn: conn,
      user: user
    } do
      business = business_fixture()

      conn = Plug.Conn.assign(conn, :business, business)
      assert AuthHelpers.business_access(conn, user, "write") === false
    end

    test "offer_access/3 returns true if user can access the offer", %{
      conn: conn,
      user: user
    } do
      business = business_fixture(user, %{})
      offer = product_fixture(business, %{})

      conn =
        conn
        |> Plug.Conn.assign(:business, business)
        |> Plug.Conn.assign(:offer, offer)

      assert AuthHelpers.offer_access(conn, user, "write") === true
    end

    test "offer_access/3 returns false if user can not access the offer", %{
      conn: conn,
      user: user
    } do
      business = business_fixture(user, %{phone: "0432984567"})
      offer = product_fixture()

      conn =
        conn
        |> Plug.Conn.assign(:business, business)
        |> Plug.Conn.assign(:offer, offer)

      assert AuthHelpers.offer_access(conn, user, "write") === false
    end
  end

  def user(_) do
    user = user_fixture()

    {:ok, user: user}
  end

  def business(_) do
    business = business_fixture()
    {:ok, business: business}
  end

  def key(%{user: user}) do
    {:ok, key} = Accounts.create_key(user)

    {:ok, key: key}
  end
end
