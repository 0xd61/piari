defmodule PiariWeb.ControllerHelpersTest do
  use PiariWeb.ConnCase, async: true
  alias PiariWeb.ControllerHelpers, as: Helpers
  import Phoenix.Controller, only: [accepts: 2]

  test "render_error/3 renders json error from error view", %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> accepts(["json"])
      |> Helpers.render_error(400, code: "foo", message: "bar")

    assert %{
             "error" => %{"code" => "foo", "message" => "bar", "status" => 400}
           } = json_response(conn, 400)

    assert conn.halted
  end

  test "render_error/3 renders html error from error view", %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "text/html")
      |> accepts(["html"])
      |> Helpers.render_error(400, code: "foo", message: "bar")

    assert html_response(conn, 400) =~ "bar"
    assert conn.halted
  end

  test "changeset_error/3 renders all errors from a changeset", %{conn: conn} do
    user = user_fixture()
    {:error, changeset} = Piari.Accounts.update_user(user, %{locale: "invalid_locale"})

    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> accepts(["json"])
      |> Helpers.changeset_error(changeset)

    assert %{"error" => %{"errors" => %{"locale" => [_]}}} = json_response(conn, 422)
  end

  describe "fetch" do
    test "_user/2 fetches user from params and puts in assigns", %{conn: conn} do
      user = user_fixture()

      conn =
        conn
        |> get(Routes.api_v1_user_path(conn, :show, user))
        |> Helpers.fetch_user([])

      assert conn.assigns.user.id == user.id
    end

    test "_user/2 assigns nil if param user not found", %{conn: conn} do
      conn =
        conn
        |> get("/")
        |> Helpers.fetch_user([])

      assert conn.assigns.user == nil
    end

    test "_user/2 raises if user does not exist", %{conn: conn} do
      assert_raise Ecto.NoResultsError, fn ->
        conn
        |> get(Routes.api_v1_user_path(conn, :show, 123))
        |> Helpers.fetch_user([])
      end
    end

    test "_business/2 fetches business from params and puts in assigns", %{conn: conn} do
      business = business_fixture()

      conn =
        conn
        |> post(Routes.api_v1_business_path(conn, :update, business), params: %{})
        |> Helpers.fetch_business([])

      assert conn.assigns.business.id == business.id
    end

    test "_business/2 assigns nil if param business not found", %{conn: conn} do
      conn =
        conn
        |> get("/")
        |> Helpers.fetch_business([])

      assert conn.assigns.business == nil
    end

    test "_business/2 raises if business does not exist", %{conn: conn} do
      assert_raise Ecto.NoResultsError, fn ->
        conn
        |> post(Routes.api_v1_business_path(conn, :update, 123), params: %{})
        |> Helpers.fetch_business([])
      end
    end

    test "_business_offer/2 fetches business offer from params and puts in assigns", %{conn: conn} do
      business = business_fixture()
      offer = product_fixture(business, %{})

      conn =
        conn
        |> post(Routes.api_v1_offer_path(conn, :update, business, offer), params: %{})
        |> Helpers.fetch_business_offer([])

      assert conn.assigns.offer.id == offer.id
    end

    test "_business_offer/2 assigns nil if param business offer not found", %{conn: conn} do
      conn =
        conn
        |> get("/")
        |> Helpers.fetch_business_offer([])

      assert conn.assigns.offer == nil
    end

    test "_business_offer/2 raises if business offer does not exist", %{conn: conn} do
      business = business_fixture()

      assert_raise Ecto.NoResultsError, fn ->
        conn
        |> post(Routes.api_v1_offer_path(conn, :update, business, 123), params: %{})
        |> Helpers.fetch_business_offer([])
      end
    end
  end
end
