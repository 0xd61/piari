defmodule PiariWeb.PlugsTest do
  use PiariWeb.ConnCase, async: true
  alias PiariWeb.Plugs
  import Phoenix.ConnTest, except: [conn: 0]

  setup %{conn: conn} do
    conn =
      conn
      |> bypass_through(PiariWeb.Router, :browser)
      |> get("/")

    {:ok, %{conn: conn}}
  end

  test "user_agent/2 puts user agent in conn", %{conn: conn} do
    conn =
      conn
      |> Plugs.user_agent([])

    assert conn.assigns.user_agent === "missing"
  end

  describe "login_session/2" do
    test "places user form session into assigns", %{conn: conn} do
      user = user_fixture()

      conn =
        conn
        |> put_session(:user_id, user.id)
        |> Plugs.login_session([])

      assert conn.assigns.current_user.id == user.id
    end

    test "with no session sets current_user assign to nil", %{conn: conn} do
      conn =
        conn
        |> Plugs.login_session([])

      assert conn.assigns.current_user == nil
    end
  end

  describe "authenticate/2" do
    test "checks authentication and places user into assigns", %{conn: conn} do
      user = user_fixture()
      {:ok, key} = Piari.Accounts.create_key(user)

      conn =
        conn
        |> Plug.Conn.put_req_header("authorization", "Bearer #{key.user_secret}")
        |> Plugs.authenticate([])

      assert conn.assigns.key.id == key.id
      assert conn.assigns.current_user.id == user.id
    end

    test "with existing session (current user in assigns) places user into assigns", %{conn: conn} do
      user = user_fixture()

      conn =
        conn
        |> put_session(:user_id, user.id)
        |> Plugs.login_session([])
        |> Plugs.authenticate([])

      assert conn.assigns[:key] == nil
      assert conn.assigns.current_user.id == user.id
    end

    test "with invalid authentication halts connection with error message", %{
      conn: conn
    } do
      conn =
        conn
        |> Plug.Conn.put_req_header("authorization", "Bearer invalid")
        |> Plugs.authenticate([])

      assert html_response(conn, 401) =~ "invalid API key"
    end

    test "without auth header sets current user and key to nil", %{
      conn: conn
    } do
      conn =
        conn
        |> Plugs.authenticate([])

      assert conn.assigns.key == nil
      assert conn.assigns.current_user == nil
    end
  end

  describe "fetch_locale/2" do
    setup do
      conn = build_conn()

      {:ok, %{conn: conn}}
    end

    test "use locale from parameters", %{conn: conn} do
      conn
      |> get("/?lang=de")

      assert Gettext.get_locale(PiariWeb.Gettext) == "de"
    end

    test "use locale from cookie", %{conn: conn} do
      conn
      |> put_resp_cookie("locale", "de")
      |> get("/")

      assert Gettext.get_locale(PiariWeb.Gettext) == "de"
    end

    test "use locale from browser accept_languages", %{conn: conn} do
      %{conn | req_headers: [{"accept-language", "de,en-US;q=0.7,en;q=0.3"}]}
      |> get("/")

      assert Gettext.get_locale(PiariWeb.Gettext) == "de"
    end

    test "use fallback language if nothing provided", %{conn: conn} do
      conn
      |> get("/")

      assert Gettext.get_locale(PiariWeb.Gettext) == "en"
    end
  end
end
