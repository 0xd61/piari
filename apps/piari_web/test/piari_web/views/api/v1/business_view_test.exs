defmodule PiariWeb.Api.V1.BusinessViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.BusinessView

  test "renders index.json" do
    business = business_fixture()
    assert [%@view{}] = render(@view, "index.json", %{businesses: [business]})
  end

  test "renders create.json" do
    business = business_fixture()

    assert %@view{} = render(@view, "create.json", %{business: business})
  end

  test "renders update.json" do
    business = business_fixture()

    assert %@view{} = render(@view, "update.json", %{business: business})
  end

  test "renders delete.json" do
    assert render(@view, "delete.json", []) ==
             %{success: true}
  end

  test "renders business.json" do
    business = business_fixture()

    assert render(@view, "business.json", %{business: business}) ==
             %@view{
               id: business.id,
               name: business.name,
               phone: business.phone,
               verified: business.verified,
               slug: business.slug
             }
  end
end
