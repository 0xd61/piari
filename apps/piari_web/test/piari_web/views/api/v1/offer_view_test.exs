defmodule PiariWeb.Api.V1.OfferViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.OfferView

  test "renders index.json" do
    offer = product_fixture()
    assert [%@view{}] = render(@view, "index.json", %{offers: [offer]})
  end

  test "renders create.json" do
    offer = product_fixture()

    assert %@view{} = render(@view, "create.json", %{offer: offer})
  end

  test "renders update.json" do
    offer = product_fixture()

    assert %@view{} = render(@view, "update.json", %{offer: offer})
  end

  test "renders delete.json" do
    assert render(@view, "delete.json", []) ==
             %{success: true}
  end

  test "renders offer.json" do
    offer = product_fixture()

    assert render(@view, "offer.json", %{offer: offer}) ==
             %@view{
               id: offer.id,
               title: offer.content.title,
               description: offer.content.description,
               price: offer.price,
               slug: offer.slug,
               expires: offer.expires,
               image: offer.image,
               attachments: offer.attachments,
               quantity: offer.quantity,
               draft: offer.draft
             }
  end
end
