defmodule PiariWeb.Api.V1.AuthViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  alias Piari.Accounts
  @view PiariWeb.Api.V1.AuthView

  test "renders index.json" do
    user = user_fixture()
    auths = Accounts.list_auths(user)

    assert %{auths: [%{id: _, provider: :only_for_test}], user_id: _} =
             render(@view, "index.json", auths: auths, user: user)
  end

  test "renders delete.json" do
    assert render(@view, "delete.json", []) ==
             %{success: true}
  end
end
