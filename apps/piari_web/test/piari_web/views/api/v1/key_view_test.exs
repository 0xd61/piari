defmodule PiariWeb.Api.V1.KeyViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.KeyView

  test "renders create.json" do
    user = user_fixture()

    assert render(@view, "create.json", token: "testtoken", user: user) ==
             %{token: "testtoken", user_id: user.id}
  end

  test "renders delete.json" do
    assert render(@view, "delete.json", []) ==
             %{success: true}
  end
end
