defmodule PiariWeb.Api.V1.Public.BusinessViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.Public.BusinessView

  test "renders show.json" do
    business = business_fixture()

    result = render(@view, "show.json", %{business: business})
    assert result.id == business.id
    assert result.name == business.name
    assert result[:verified] == nil
  end
end
