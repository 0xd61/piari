defmodule PiariWeb.Api.V1.Public.OfferViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.Public.OfferView

  test "renders index.json" do
    offer = product_fixture()

    assert render(@view, "index.json", %{offers: [offer]}) ==
             [
               %{
                 id: offer.id,
                 title: offer.content.title,
                 description: offer.content.description,
                 price: offer.price,
                 slug: offer.slug,
                 expires: offer.expires,
                 image: offer.image,
                 attachments: offer.attachments,
                 quantity: offer.quantity
               }
             ]
  end

  test "renders show.json" do
    offer = product_fixture()

    assert render(@view, "show.json", %{offer: offer}) ==
             %{
               id: offer.id,
               title: offer.content.title,
               description: offer.content.description,
               price: offer.price,
               slug: offer.slug,
               expires: offer.expires,
               image: offer.image,
               attachments: offer.attachments,
               quantity: offer.quantity
             }
  end
end
