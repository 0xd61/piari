defmodule PiariWeb.Api.V1.SearchViewTest do
  use PiariWeb.ConnCase
  import Phoenix.View
  @view PiariWeb.Api.V1.SearchView

  test "renders index.json" do
    assert render(@view, "index.json", %{data: []}) == %@view{data: []}
  end

  test "renders result.json" do
    product = product_fixture()

    assert render(@view, "result.json", %{search: product}) ==
             %{
               id: product.id,
               slug: product.slug,
               image: nil,
               quantity: product.quantity,
               subtitle: product.content.title,
               title: :erlang.float_to_binary(product.price, decimals: 2)
             }
  end

  test "renders result.json fallback" do
    assert render(@view, "result.json", %{foo: "bar"}) == %{}
  end
end
