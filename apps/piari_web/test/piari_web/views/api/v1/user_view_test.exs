defmodule PiariWeb.Api.V1.UserViewTest do
  use PiariWeb.ConnCase, async: true
  import Phoenix.View
  @view PiariWeb.Api.V1.UserView

  test "renders show.json" do
    user = user_fixture()

    assert %@view{} = render(@view, "show.json", %{user: user})
  end

  test "renders update.json" do
    user = user_fixture()

    assert %@view{} = render(@view, "update.json", %{user: user})
  end

  test "renders delete.json" do
    assert render(@view, "delete.json", []) ==
             %{success: true}
  end
end
