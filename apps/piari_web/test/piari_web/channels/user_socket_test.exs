defmodule PiariWeb.Channels.UserSocketTest do
  use PiariWeb.ChannelCase, async: true

  alias PiariWeb.UserSocket
  @salt Application.get_env(:piari_web, PiariWeb.Endpoint)[:token_salt]

  test "socket authentication with valid token" do
    token = Phoenix.Token.sign(@endpoint, @salt, "123")

    assert {:ok, socket} = connect(UserSocket, %{"token" => token})
    assert socket.assigns.user_id == "123"
  end

  test "socket authentication with invalid token" do
    assert :error = connect(UserSocket, %{"token" => "1313"})
    assert :error = connect(UserSocket, %{})
  end

  test "socket authentication without token" do
    assert :error = connect(UserSocket, %{"token" => ""})
    # assert socket.assigns[:user_id] == nil
    # assert socket.assigns[:session_id] != nil
  end
end
