defmodule Mix.Tasks.Search.Index.Swap do
  @moduledoc """
    Builds Elasticsearch indexes using a zero-downtime, hot-swap technique.
    1. Build an index for the given `alias`, with a timestamp: `alias-12323123`
    2. Bulk upload data to that index using `store` and `sources`.
    3. Alias the `alias` to `alias-12323123`.
    4. Remove old indexes beginning with `alias`.
    5. Refresh `alias-12323123`.
    For a functional version of this approach, see
    `Elasticsearch.Index.hot_swap/4`.
  """
  use Mix.Task

  @switches []

  @default_opts []

  @impl Mix.Task
  @doc false
  def run(args) do
    Mix.Task.run("app.start", [])
    {index, alias, _opts} = build(args)
    swap(index, alias)
  end

  @doc false
  def build(args) do
    {opts, parsed, _} = parse_opts(args)
    [index, alias] = parsed

    {index, alias, opts}
  end

  def swap(index, alias) do
    :ok = Search.swap_index(alias, index)
  end

  defp parse_opts(args) do
    {opts, parsed, invalid} = OptionParser.parse(args, switches: @switches)

    merged_opts =
      @default_opts
      |> Keyword.merge(opts)

    {merged_opts, parsed, invalid}
  end
end
