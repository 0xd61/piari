defmodule Mix.Tasks.Search.Index do
  use Mix.Task

  # TODO: REFACTOR (this is only a temp solution)

  @switches [new: :boolean]

  @default_opts [new: false]

  @impl Mix.Task
  @doc false
  def run(args) do
    Mix.Task.run("app.start", [])
    {index, schema, opts} = build(args)
    index(index, schema, opts)
  end

  @doc false
  def build(args) do
    {opts, parsed, _} = parse_opts(args)
    [index_name, schema_name] = parsed

    {index_name, String.to_atom(schema_name), opts}
  end

  def index(index, schema, opts) do
    new? = Keyword.get(opts, :new, false)

    if new? do
      case Search.create_index(index) do
        {:ok, index} ->
          sync(index, schema)

        {:error, error} ->
          raise "#{inspect(error)}"
      end
    else
      sync(index, schema)
    end

    [{pid, _}] = Registry.lookup(Search.Registry, {index, schema})
    ref = Process.monitor(pid)

    receive do
      {:DOWN, ^ref, :process, ^pid, :shutdown} -> :ok
    end
  end

  defp sync(index, schema) do
    Search.sync(index, schema, max_timeout: 500, max_wait: 100, max_batch_size: 1000)
  end

  defp parse_opts(args) do
    {opts, parsed, invalid} = OptionParser.parse(args, switches: @switches)

    merged_opts =
      @default_opts
      |> Keyword.merge(opts)

    {merged_opts, parsed, invalid}
  end
end
