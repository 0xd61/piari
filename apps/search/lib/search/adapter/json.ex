defmodule Search.Adapter.JSON do
  alias Jason
  @behaviour Elastix.JSON.Codec

  def decode(data, opts \\ []), do: Jason.decode(data, opts)
  def encode!(data), do: Jason.encode!(data)
end
