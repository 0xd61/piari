defmodule Search.Adapter.Elastic.Index do
  def create_index(config, index) do
    case config[:host]
         |> Elastix.Index.create(
           index,
           %{}
         )
         |> handle_resp() do
      {:ok, %{"index" => index}} ->
        index

      error ->
        error
    end
  end

  def swap_indexes(config, alias, new, old \\ nil) do
    case swap(config, alias, new, old) do
      {:ok, _} -> {:ok, alias}
      error -> error
    end
  end

  defp swap(config, alias, new, nil) do
    config[:host]
    |> Elastix.Alias.post([
      %{add: %{index: new, alias: alias}}
    ])
    |> handle_resp()
  end

  defp swap(config, alias, new, old) do
    config[:host]
    |> Elastix.Alias.post([
      %{add: %{index: new, alias: alias}},
      %{remove: %{index: old, alias: alias}}
    ])
    |> handle_resp()
  end

  def get_index_by_alias(config, name) do
    case config[:host]
         |> Elastix.Index.get(name)
         |> handle_resp() do
      {:ok, body} ->
        [index] = Map.keys(body)
        index

      _ ->
        nil
    end
  end

  defp handle_resp(resp) do
    case resp do
      {:ok, %{status_code: 200, body: body}} ->
        {:ok, body}

      error ->
        {:error, error}
    end
  end
end
