defmodule Search.Adapter.Elastic.Result do
  import Search.Result

  def parse_resp(resp, index) do
    total = get_in(resp, ["hits", "total"]) || get_in(resp, ["_shards", "successful"]) || 0
    time = get_in(resp, ["took"]) || 0
    meta = meta(total: total, time: time, index: index)

    cond do
      error = get_in(resp, ["error"]) || get_in(resp, ["errors"]) ->
        {:error, error}

      search = get_in(resp, ["hits", "hits"]) ->
        {:ok, meta, search |> build_results()}

      true ->
        {:ok, meta, []}
    end
  end

  defp build_results(items) when is_list(items) do
    items
    |> Enum.map(fn item ->
      result(
        type: String.to_atom(item["_type"]),
        id: String.to_integer(item["_id"]),
        score: item["_score"]
      )
    end)
  end

  defp build_results(item) do
    build_results([item])
  end
end
