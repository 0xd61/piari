defmodule Search.Adapter.Elastic.Query do
  alias Search.Query, as: Q

  def build(%Q{search: nil, offset: offset, size: size}) do
    %{from: offset, size: size}
  end

  def build(%Q{fields: fields, search: search, offset: offset, size: size})
      when length(fields) > 0 do
    %{
      query: %{
        multi_match: %{
          query: search,
          fields: build_fields(fields),
          fuzziness: "AUTO"
        }
      },
      from: offset,
      size: size
    }
  end

  def build(%Q{search: search, offset: offset, size: size}) do
    %{
      query: %{multi_match: %{query: search, fuzziness: "AUTO"}},
      from: offset,
      size: size
    }
  end

  def parse_type(%Q{type: type}) do
    type || ""
  end

  def parse_index(%Q{on: index}) do
    index
  end

  def parse_id(%Q{id: id}) do
    id || ""
  end

  def parse_data(%Q{data: data}) do
    data
  end

  defp build_fields(fields) when is_list(fields) do
    fields
    |> Enum.map(&build_field(&1))
  end

  defp build_field({name, boost}) do
    name <> "^" <> to_string(boost)
  end

  defp build_field(name) do
    name
  end
end
