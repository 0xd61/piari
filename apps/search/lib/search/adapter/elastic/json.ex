defmodule Search.Adapter.Elastic.JSON do
  @behaviour Elastix.JSON.Codec

  def encode!(data), do: Search.Adapter.JSON.encode!(data)
  def decode(json, _opts \\ []), do: Search.Adapter.JSON.decode(json)
end
