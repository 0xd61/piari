defmodule Search.Adapter.Elastic do
  @moduledoc ~S"""
  Adapter for Elasticsearch API

  ```
  config :myApp, MyApp.Search,
  host: "http://localhost:9200",
  index: :twitter,
  type: :tweet
  ```

  """

  use Search.Adapter, required_config: [:host]
  alias Elastix
  alias Search.Adapter.Elastic.{Query, Result, Index}
  alias Search.Dispatcher

  def search_data!(config, %Search.Query{} = query) do
    with {:ok, %{status_code: 200, body: body}} <-
           Elastix.Search.search(
             config[:host],
             Query.parse_index(query),
             query |> Query.parse_type() |> handle_type(),
             Query.build(query),
             filter_path:
               "took,hits.hits._id,hits.hits._score,hits.hits._type,hits.total,hits.max_score"
           ),
         {:ok, meta, results} <- Result.parse_resp(body, Query.parse_index(query)) do
      {meta, results}
    else
      error ->
        raise "encountered an error while searching: #{inspect(error)}"
    end
  end

  defp handle_type(type) when not is_list(type) do
    [type]
  end

  defp handle_type(type) do
    type
  end

  def create_index(config, name) do
    Index.create_index(config, name)
  end

  def swap_index(config, alias, index) do
    with old_index <- Index.get_index_by_alias(config, alias),
         {:ok, _} <- Index.swap_indexes(config, alias, index, old_index) do
      :ok
    end
  end

  def index_data!(config, query) do
    index = Query.parse_index(query)
    type = Query.parse_type(query)

    d =
      Dispatcher.get_or_register(
        {index, type},
        dispatcher_callback(config[:host], index, type),
        config
      )

    :ok = Dispatcher.put(d, [%{index: %{_id: Query.parse_id(query)}}, Query.parse_data(query)])
  end

  def delete_data!(config, query) do
    index = Query.parse_index(query)
    type = Query.parse_type(query)

    d =
      Dispatcher.get_or_register(
        {index, type},
        dispatcher_callback(config[:host], index, type),
        config
      )

    :ok = Dispatcher.put(d, [%{delete: %{_id: Query.parse_id(query)}}])
  end

  def dispatcher_callback(host, index, type) do
    fn events ->
      events = List.flatten(events)

      case Elastix.Bulk.post(host, events,
             index: index,
             type: type,
             httpoison_options: [timeout: 180_000]
           ) do
        {:ok, %{status_code: 200}} ->
          :ok

        error ->
          raise "expected {:ok, %{status_code: 200}}, got: #{inspect(error)}"
      end
    end
  end
end
