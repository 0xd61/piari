defmodule Search.Indexes do
  @type query :: Search.Query.IndexExpr.t()
  @type opts :: Keyword.t()

  @callback build() :: query
  @callback defaults(query) :: query

  def build(index) do
    index.build()
  end

  def defaults(index, query) do
    index.defaults(query)
  end
end
