defmodule Search.Adapter do
  @moduledoc ~S"""
  Specification of the authentication adapter.
  """

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      @required_config opts[:required_config] || []

      @behaviour Search.Adapter

      def validate_config(config) do
        Search.Adapter.validate_config(@required_config, config)
      end
    end
  end

  @type config :: Keyword.t()
  @type name :: String.t()
  @type alias :: String.t()
  @type id :: integer
  @type score :: float
  @type data_type :: atom
  @type result :: [{data_type, id, score}]

  @callback validate_config(config) :: :ok | no_return

  @callback create_index(config, name) :: {:ok, String.t()} | {:error, any}

  @callback swap_index(config, alias, name) :: :ok | {:error, any}

  @callback index_data!(config, data :: map()) :: :ok

  @callback search_data!(config, Search.Query.t()) ::
              {Search.Result.meta(), [Search.Result.result()]}
              | no_return

  @callback delete_data!(config, id) :: :ok

  @spec validate_config([atom], config) :: :ok | no_return
  def validate_config(required_config, config) do
    missing_keys =
      Enum.reduce(required_config, [], fn key, missing_keys ->
        if config[key] in [nil, ""],
          do: [key | missing_keys],
          else: missing_keys
      end)

    raise_on_missing_config(missing_keys, config)
  end

  defp raise_on_missing_config([], _config), do: :ok

  defp raise_on_missing_config(key, config) do
    raise ArgumentError, """
    expected #{inspect(key)} to be set, got: #{inspect(config)}
    """
  end
end
