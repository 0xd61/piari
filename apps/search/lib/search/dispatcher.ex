defmodule Search.Dispatcher do
  use GenServer
  require Logger

  @max_wait 1000
  @max_batch_size 50
  @max_timeout :timer.seconds(30)
  @registry Search.Registry
  @supervisor Search.DynamicSupervisor

  @type name :: atom | String.t()
  @type events :: list(any)
  @type callback :: (events -> no_return)
  @type opts :: Keyword.t()
  @type dispatcher :: tuple()

  @spec get_or_register(name, callback, opts) :: dispatcher
  def get_or_register(name, callback, opts \\ []) do
    case Registry.lookup(@registry, name) do
      [] ->
        opts =
          opts
          |> Keyword.put_new(:name, name)
          |> Keyword.put_new(:callback, callback)

        {:ok, _dispatcher} =
          DynamicSupervisor.start_child(
            @supervisor,
            {__MODULE__, opts}
          )

        Logger.debug("Starting dispatcher: #{inspect(name)}")

        via_tuple(name)

      [_] ->
        via_tuple(name)
    end
  end

  @spec put(dispatcher, event :: any, timeout :: integer) :: :ok
  def put(dispatcher, event, timeout \\ 5000) do
    GenServer.call(dispatcher, {:put, event}, timeout)
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: via_tuple(opts[:name]))
  end

  def init(opts) do
    state = %{
      max_wait: opts[:max_wait] || @max_wait,
      max_batch_size: opts[:max_batch_size] || @max_batch_size,
      max_timeout: opts[:max_timeout] || @max_timeout,
      name: opts[:name],
      timer: nil,
      callback: opts[:callback],
      queue: :queue.new(),
      len: 0
    }

    {:ok, schedule_send(state), state.max_timeout}
  end

  def handle_call({:put, event}, _from, state) do
    state =
      state
      |> queue_event(event)
      |> schedule_send()

    {:reply, :ok, state, state.max_timeout}
  end

  def handle_info(:send, state) do
    state =
      state
      |> Map.put(:timer, nil)
      |> dispatch_events()

    {:noreply, state, state.max_timeout}
  end

  def handle_info(:timeout, %{name: name}) do
    Logger.debug("Stopping dispatcher: #{inspect(name)}, reason: timeout")
    :ok = DynamicSupervisor.terminate_child(@supervisor, self())
    {:stop, :normal, []}
  end

  defp schedule_send(%{timer: nil} = state) do
    %{state | timer: Process.send_after(self(), :send, state.max_wait)}
  end

  defp schedule_send(state) do
    state
  end

  defp queue_event(%{queue: queue, len: len, max_batch_size: max} = state, event)
       when len < max do
    %{state | queue: :queue.in(event, queue), len: len + 1}
  end

  defp queue_event(state, event) do
    state
    |> dispatch_events()
    |> queue_event(event)
  end

  defp dispatch_events(%{queue: queue, callback: callback} = state) do
    case :queue.to_list(queue) do
      [] ->
        state

      events ->
        Logger.debug("Dispatching events from queue: #{inspect(events)}")

        Task.Supervisor.start_child(
          Search.TaskSupervisor,
          fn -> callback.(events) end,
          shutdown: :brutal_kill,
          restart: :transient
        )

        %{state | queue: :queue.new(), len: 0}
    end
  end

  defp via_tuple(name) do
    {:via, Registry, {@registry, name}}
  end
end
