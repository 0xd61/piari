defmodule Search.Query do
  defstruct [:on, :module, :data, :id, :type, :search, fields: [], offset: 0, size: 10]

  @type t() :: %Search.Query{
          on: atom | String.t(),
          fields: [{field :: String.t(), boost :: integer}] | [field :: String.t()],
          search: String.t(),
          data: map | list,
          id: term,
          type: atom,
          offset: integer,
          size: integer
        }

  @mod __MODULE__

  @spec on(index :: atom) :: t()
  def on(index) when is_atom(index) do
    query = Search.Indexes.build(index)
    %@mod{query | module: index}
  end

  @spec on(index :: String.t()) :: t()
  def on(index) when is_binary(index) do
    %@mod{on: index}
  end

  def where(query, kw) when is_list(kw) do
    build_where(query, kw)
  end

  defp build_where(%@mod{module: nil}, [{:default, true} | _t]) do
    raise RuntimeError,
          "defaults can only be used with an Search.Indexes behaviour implementation"
  end

  defp build_where(query, [{:default, true} | t]) do
    build_where(Search.Indexes.defaults(query.module, query), t)
  end

  defp build_where(query, [{:size, expr} | t]) when is_integer(expr) do
    build_where(%@mod{query | size: expr}, t)
  end

  defp build_where(query, [{:offset, expr} | t]) when is_integer(expr) do
    build_where(%@mod{query | offset: expr}, t)
  end

  defp build_where(query, [{:field, field} | t]) when is_binary(field) do
    build_where(%@mod{query | fields: [field | query.fields]}, t)
  end

  defp build_where(query, [{:field, {field, boost}} | t])
       when is_binary(field) and is_integer(boost) do
    build_where(%@mod{query | fields: [{field, boost} | query.fields]}, t)
  end

  defp build_where(query, []) do
    query
  end

  def search(query, term, opts \\ []) do
    type = opts[:type]
    %@mod{query | search: term, type: type}
  end

  def put(query, data, opts \\ []) do
    type = opts[:type] || Search.Searchable.type(data)
    id = data |> Search.Searchable.id()
    data = data |> Search.Searchable.to_index()
    %@mod{query | data: data, id: id, type: type}
  end

  def delete(query, data, opts \\ []) do
    type = opts[:type] || Search.Searchable.type(data)
    id = data |> Search.Searchable.id()
    data = data |> Search.Searchable.to_index()
    %@mod{query | data: data, id: id, type: type}
  end
end
