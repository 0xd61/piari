defmodule Search.Result do
  require Record
  Record.defrecord(:meta, index: nil, total: 0, time: 0)
  Record.defrecord(:result, type: nil, id: nil, score: nil)
  @type meta :: record(:meta, index: atom, total: integer, time: integer)
  @type result :: record(:result, type: atom, id: integer | String.t(), score: float)
end
