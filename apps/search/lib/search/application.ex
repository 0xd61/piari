defmodule Search.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Search.Worker.start_link(arg)
      # {Search.Worker, arg}
      {Task.Supervisor, name: Search.TaskSupervisor},
      {DynamicSupervisor, strategy: :one_for_one, name: Search.DynamicSupervisor},
      {Registry, keys: :unique, name: Search.Registry}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Search.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
