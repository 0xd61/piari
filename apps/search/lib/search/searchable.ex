defmodule Search.Searchable do
  @callback query(term) :: [term]
  @callback to_index(struct()) :: map()
  @callback id(struct()) :: term
  @callback type() :: atom
  @callback stream() :: Enumerable.t()
  @callback transaction(fun) :: any

  defmacrop raise_undefined_behaviour(exception, module, top) do
    quote do
      exception =
        case __STACKTRACE__ do
          [unquote(top) | _] ->
            reason = "#{inspect(unquote(module))} does not implement the Searchable behaviour"
            %{unquote(exception) | reason: reason}

          _ ->
            unquote(exception)
        end

      reraise exception, __STACKTRACE__
    end
  end

  def query(module, ids) do
    module.query(ids)
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :query, [^ids], _})
  end

  def to_index(%module{} = struct) do
    module.to_index(struct)
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :to_index, [^struct], _})
  end

  def id(%module{} = struct) do
    module.id(struct)
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :id, [^struct], _})
  end

  def type(%module{}) do
    module.type()
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :type, [], _})
  end

  def stream(%module{}) do
    module.stream()
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :stream, [], _})
  end

  def stream(module) do
    module.stream()
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :stream, [], _})
  end

  def transaction(%module{}, fun) do
    module.transaction(fun)
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :transaction, [^fun], _})
  end

  def transaction(module, fun) do
    module.transaction(fun)
  rescue
    exception in UndefinedFunctionError ->
      raise_undefined_behaviour(exception, module, {^module, :transaction, [^fun], _})
  end
end
