defmodule Search do
  alias Search.Query
  import Search.Result

  def create_index(name, config \\ []) do
    config = parse_config(config)
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    adapter.create_index(config, name)
  end

  def swap_index(alias, name, config \\ []) do
    config = parse_config(config)
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    adapter.swap_index(config, alias, name)
  end

  def sync(index, searchable, config \\ []) do
    Search.Searchable.transaction(searchable, fn ->
      Search.Searchable.stream(searchable)
      |> Stream.map(
        &(Query.on(index)
          |> Query.put(&1)
          |> Search.index(config))
      )
      |> Stream.run()
    end)
  end

  def index(query, config \\ [])

  def index(query, config) when is_list(query) do
    config = parse_config(config)
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    Enum.each(query, &adapter.index_data!(config, &1))
  end

  def index(query, config) do
    index([query], config)
  end

  @doc """
  Searches the given query. Returns a tuple with a list of meta information and a list of all results.
  The meta information is in the same order as the queries supplied in the query input argument.
  """
  def search(query, config \\ [])

  def search(query, config) when is_list(query) do
    config = parse_config(config)
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    timeout = config[:timeout] || 10_000

    query
    |> Enum.map(&async(adapter, :search_data!, [config, &1]))
    |> Task.yield_many(timeout)
    |> Enum.map(fn {task, res} -> res || Task.shutdown(task, :brutal_kill) end)
    |> Enum.map(fn
      {:ok, {meta_rec, data_rec}} ->
        scores =
          data_rec
          |> Enum.map(&{result(&1, :id), result(&1, :score)})
          |> Map.new(fn {key, val} -> {key, val} end)

        data =
          data_rec
          |> Enum.group_by(&result(&1, :type), &result(&1, :id))
          |> Enum.reduce([], fn {type, ids}, acc ->
            [Search.Searchable.query(type, ids) | acc]
          end)
          |> List.flatten()
          |> Enum.sort(&(scores[&1.id] >= scores[&2.id]))

        meta_rec |> meta() |> Map.new() |> Map.put(:data, data)

      _ ->
        []
    end)
    |> List.flatten()
  end

  def search(query, config) do
    search([query], config)
  end

  def delete(query, config \\ [])

  def delete(query, config) when is_list(query) do
    config = parse_config(config)
    adapter = Keyword.fetch!(config, :adapter)
    :ok = adapter.validate_config(config)
    Enum.each(query, &adapter.delete_data!(config, &1))
  end

  def delete(query, config) do
    delete([query], config)
  end

  def async(adapter, fun, args \\ [])

  def async(adapter, fun, args) when is_list(args) do
    Task.Supervisor.async_nolink(Search.TaskSupervisor, adapter, fun, args, shutdown: :brutal_kill)
  end

  def async(_, _, args) do
    raise ArgumentError, "#{args} must be a list"
  end

  def parse_config(dynamic_config) do
    # credo:disable-for-lines:3
    Application.get_all_env(:search)
    |> Keyword.merge(dynamic_config)
  end

  @doc false
  def validate_dependency(adapter) do
    require Logger

    with adapter when not is_nil(adapter) <- adapter,
         {:module, _} <- Code.ensure_loaded(adapter),
         true <- function_exported?(adapter, :validate_dependency, 0),
         :ok <- adapter.validate_dependency() do
      :ok
    else
      no_match when no_match in [nil, false] ->
        :ok

      {:error, :nofile} ->
        Logger.error("#{adapter} does not exist")
        :abort

      {:error, deps} when is_list(deps) ->
        Logger.error(Search.missing_deps_message(adapter, deps))
        :abort
    end
  end

  @doc false
  def missing_deps_message(adapter, deps) do
    deps =
      deps
      |> Enum.map(fn
        {lib, module} -> "#{module} from #{inspect(lib)}"
        {module} -> inspect(module)
      end)
      |> Enum.map(&"\n- #{&1}")

    """
    The following dependencies are required to use #{inspect(adapter)}:
    #{deps}
    """
  end
end
