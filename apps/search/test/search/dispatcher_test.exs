defmodule Search.DispatcherTest do
  use ExUnit.Case, async: true
  alias Search.Dispatcher
  @registry Search.Registry

  test "get_or_register/3 returns a via tuple of dispatcher" do
    name = "test#{System.unique_integer([:positive])}"
    assert {:via, _, _} = Dispatcher.get_or_register(name, & &1)
    assert [{pid, _}] = Registry.lookup(@registry, name)
    assert Process.alive?(pid) == true
  end

  test "put/3 adds event to dispatcher queue and dispatches as batch to callback" do
    name = "test#{System.unique_integer([:positive])}"
    pid = self()

    via = Dispatcher.get_or_register(name, &send(pid, &1), max_wait: 100)

    assert :ok = Dispatcher.put(via, :foo)
    assert :ok = Dispatcher.put(via, :bar)

    receive do
      msg ->
        assert [:foo, :bar] = msg
    end
  end

  test "dispatcher sends message after max_wait" do
    name = "test#{System.unique_integer([:positive])}"
    pid = self()

    via = Dispatcher.get_or_register(name, &send(pid, &1), max_batch_size: 100, max_wait: 100)

    assert :ok = Dispatcher.put(via, :foo)
    assert :ok = Dispatcher.put(via, :bar)

    receive do
      msg ->
        assert [:foo, :bar] = msg
    end
  end

  test "dispatcher sends message if queue larger than batch_size" do
    name = "test#{System.unique_integer([:positive])}"
    pid = self()

    via = Dispatcher.get_or_register(name, &send(pid, &1), max_batch_size: 2, max_wait: 10000)

    assert :ok = Dispatcher.put(via, :foo)
    assert :ok = Dispatcher.put(via, :bar)
    assert :ok = Dispatcher.put(via, :foobar)

    receive do
      msg ->
        assert [:foo, :bar] = msg
    end
  end

  test "dispatcher terminates itself after timeout" do
    name = "test#{System.unique_integer([:positive])}"
    _via = Dispatcher.get_or_register(name, & &1, max_timeout: 100)

    [{pid, _}] = Registry.lookup(Search.Registry, name)
    ref = Process.monitor(pid)

    receive do
      msg ->
        assert {:DOWN, ^ref, :process, ^pid, :shutdown} = msg
    end
  end
end
