defmodule Search.Adapter.ElasticTest do
  use ExUnit.Case
  @moduletag :external

  alias Search.Adapter.Elastic
  alias Search.TestHelpers.Searchable, as: Searchable

  @host Application.get_env(:search, :host)
  @index "test"
  @config [host: @host, max_batch_size: 1]
  @data %{
    id: 1,
    text: "this is only a test"
  }

  def query_fixture(opts \\ []) do
    %Search.Query{
      on: opts[:on] || @index,
      module: opts[:module] || __MODULE__,
      offset: opts[:offset] || 0,
      size: opts[:size] || 100,
      data: opts[:data] || %{id: 1, type: Searchable},
      id: opts[:id] || 1,
      type: opts[:type] || Searchable,
      search: opts[:search] || nil
    }
  end

  def search(_) do
    query = query_fixture(id: @data.id, data: @data)
    Elastic.create_index(@config, @index)
    Elastic.index_data!(@config, query)
    :timer.sleep(500)
    {:ok, search: query}
  end

  setup [:search]

  test "search_data!/2 searches an index and returns results" do
    query = query_fixture(search: "test")

    assert {{:meta, @index, _, _}, [{:result, _, 1, _}]} = Elastic.search_data!(@config, query)
  end

  test "search_data!/2 searches returns all data if no query is given" do
    query = query_fixture(search: nil)

    assert {{:meta, @index, _, _}, [{:result, _, 1, 1.0}]} = Elastic.search_data!(@config, query)
  end

  test "search_data!/2 raises an exception if elastic returns an error" do
    query = query_fixture(size: 100_000)

    assert_raise RuntimeError, fn ->
      Elastic.search_data!(@config, query)
    end
  end

  test "search_data!/2 returns an empty data set if nothing is found" do
    query = query_fixture(search: "undefined")

    assert {{:meta, @index, 0, _}, []} = Elastic.search_data!(@config, query)
  end

  test "index_data!/2 indexes a dataset" do
    query = query_fixture(id: @data.id, data: @data)

    assert :ok = Elastic.index_data!(@config, query)
  end

  test "delete_data/2 deletes a dataset from an index" do
    query = query_fixture(id: @data.id, data: @data)

    assert :ok = Elastic.delete_data!(@config, query)
  end

  test "create_index/2 creates a new index" do
    assert Elastic.create_index(@config, "new_index-#{System.unique_integer([:positive])}") =~
             "new_index"
  end

  test "create_index/2 returns error on failue" do
    assert {:error, _} = Elastic.create_index(@config, "new/:index")
  end

  test "swap_index/3 adds alias to new index if old does not exist" do
    index = Elastic.create_index(@config, "new_index-#{System.unique_integer([:positive])}")

    assert :ok = Elastic.swap_index(@config, "alias", index)
  end

  test "swap_index/3 moves alias from old to new index" do
    index_old = Elastic.create_index(@config, "new_index-#{System.unique_integer([:positive])}")
    index_new = Elastic.create_index(@config, "new_index-#{System.unique_integer([:positive])}")

    :ok = Elastic.swap_index(@config, "alias", index_old)
    assert :ok = Elastic.swap_index(@config, "alias", index_new)
  end

  test "swap_index/3 returns error on failure" do
    assert {:error, _} = Elastic.swap_index(@config, "alias", "does_not_exist")
  end

  test "dispatcher_callback/3 sends bulk request with events" do
    query = query_fixture(id: @data.id, data: @data)
    callback = Elastic.dispatcher_callback(@config[:host], query.on, query.type)

    assert :ok = callback.([%{index: %{_id: query.id}}, @data])
  end

  test "dispatcher_callback/3 sends bulk request with events raises on error" do
    query = query_fixture(id: @data.id, data: @data)
    callback = Elastic.dispatcher_callback(@config[:host], query.on, query.type)

    assert_raise RuntimeError, fn ->
      callback.([%{index: %{_id: query.id}}, [@data, @data]])
    end
  end
end
