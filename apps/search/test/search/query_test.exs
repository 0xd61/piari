defmodule Search.QueryTest do
  use Search.QueryCase, async: true

  alias Search.Query
  alias Search.TestHelpers.Index
  alias Search.TestHelpers.Searchable, as: Result

  test "on/2 returns search query on an index" do
    assert Query.on(Index) == %Query{on: Index.name(), module: Index}
    assert Query.on("test") == %Query{on: "test", module: nil}
  end

  test "where/2 with default keyword applies index defaults" do
    query = Query.on(Index)

    assert %Query{fields: [{"bar", 1}, "foo"]} = Query.where(query, default: true)
  end

  test "where/2 with default keyword raises if Search.Indexes is not implemented" do
    query = Query.on("test")

    assert_raise RuntimeError, fn ->
      Query.where(query, default: true)
    end
  end

  test "where/2 with size keyword sets the size variable" do
    query = Query.on(Index)
    assert %Query{size: 123} = Query.where(query, size: 123)
  end

  test "where/2 with offset keyword sets the offset variable" do
    query = Query.on(Index)
    assert %Query{offset: 123} = Query.where(query, offset: 123)
  end

  test "where/2 with field keyword adds a field the fields list" do
    query = Query.on(Index)

    assert %Query{fields: [{"def", 5}, "abc"]} =
             Query.where(query, field: "abc", field: {"def", 5})
  end

  test "search/3 sets the necessary variables to search an index" do
    query = Query.on(Index)
    assert %Query{search: "test", type: Result} = Query.search(query, "test", type: Result)
  end

  test "put/3 sets the necessary variables to index a document" do
    query = Query.on(Index)

    assert %Query{type: Result, id: 5, data: %{id: 5, text: "foo"}} =
             Query.put(query, %Result{id: 5, text: "foo"})
  end

  test "delete_data/3 sets the necessary variables to delete a document" do
    query = Query.on(Index)

    assert %Query{type: Result, id: 5, data: %{id: 5, text: "foo"}} =
             Query.delete(query, %Result{id: 5, text: "foo"})
  end
end
