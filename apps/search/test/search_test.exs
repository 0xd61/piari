defmodule SearchTest do
  use Search.QueryCase

  alias Search.TestHelpers.Searchable, as: Result
  alias Search.Query

  test "create_index/2 calls create_index from adapter" do
    assert :ok = Search.create_index("test")
  end

  test "swap_index/3 calls swap_index from adapter" do
    assert :ok = Search.swap_index("alias", "test")
  end

  test "sync/3 streams searchables and indexes them" do
    assert :ok = Search.sync("test", Result)
  end

  test "index/2 calls index from adapter" do
    assert :ok = Search.index("run")
  end

  test "index/2 calls index from adapter for each query" do
    assert :ok = Search.index(["run", "run"])
  end

  test "delete/2 calls delete from adapter" do
    assert :ok = Search.delete("run")
  end

  test "delete/2 calls delete from adapter for each query" do
    assert :ok = Search.delete(["run", "run"])
  end

  test "search/2 creates a new task for each query backend and returns all results" do
    assert [%{data: [%Result{}]}] = Search.search(%Query{search: "result"})
  end

  test "search/2 creates a new task for each query" do
    assert [%{data: [%Result{}]}, %{data: [%Result{}]}] = Search.search([%Query{search: "result"}, %Query{search: "result"}])
  end

  test "search/2 with no results" do
    assert [%{data: [], total: 0}] = Search.search(%Query{search: "none"})
  end

  @tag :capture_log
  test "search/3 discards backend errors" do
    assert [] = Search.search(%Query{search: "boom"})
  end

  test "search/3 with timeout returns no results" do
    assert [] = Search.search(%Query{search: "timeout"}, timeout: 500)
  end

  test "search/3 with result and timeout returns result" do
    assert [%{data: [%Result{}], total: 1}] = Search.search([%Query{search: "timeout"}, %Query{search: "result"}], timeout: 500)
  end
end
