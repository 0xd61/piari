defmodule Search.TestHelpers.Searchable do
  defstruct [:id, :text]
  @behaviour Search.Searchable

  @impl Search.Searchable
  def query(ids) do
    Enum.map(ids, &%__MODULE__{id: &1})
  end

  @impl Search.Searchable
  def to_index(struct) do
    %{id: struct.id, text: struct.text}
  end

  @impl Search.Searchable
  def id(struct) do
    struct.id
  end

  @impl Search.Searchable
  def type() do
    __MODULE__
  end

  @impl Search.Searchable
  def stream() do
    Stream.map(1..5, &%__MODULE__{id: &1})
  end

  @impl Search.Searchable
  def transaction(fun) do
    result = fun.()
    result
  end
end

defmodule Search.TestHelpers.Index do
  @behaviour Search.Indexes
  alias Search.Query, as: Query

  def name(), do: :test

  @impl true
  def build() do
    %Query{on: name()}
  end

  @impl true
  def defaults(query) do
    query
    |> Query.where(field: "foo", field: {"bar", 1})
  end
end

defmodule Search.TestHelpers.Adapter do
  use Search.Adapter, required_config: [:host]
  import Search.Result
  alias Search.TestHelpers.Searchable, as: Result

  def search_data!(_config, %{search: "timeout"}) do
    :timer.sleep(:infinity)
  end

  def search_data!(_config, %{search: "none"}) do
    {meta(total: 0, time: 5), []}
  end

  def search_data!(_config, %{search: "boom"}) do
    raise "boom!"
  end

  def search_data!(_config, %{search: _}) do
    {meta(total: 1, time: 5), [result(id: 1, score: 6, type: Result)]}
  end

  def index_data!(_config, "boom") do
    raise "boom!"
  end

  def index_data!(_config, _query) do
    :ok
  end

  def delete_data!(_config, "boom") do
    raise "boom!"
  end

  def delete_data!(_config, _query) do
    :ok
  end

  def create_index(_config, _index) do
    :ok
  end

  def swap_index(_config, _alias, _index) do
    :ok
  end
end
