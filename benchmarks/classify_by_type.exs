types = ~w(type1 type2 type3 type4 type5 type6)

build_items = fn list ->
  Enum.map(list, fn id ->
    %{"_id" => "#{id}", "_score" => 1, "_type" => Enum.random(types), "_index" => "some_index"}
  end)
end

Benchee.run(
  %{
    # "get_single_record" => fn input -> input |> product_list.() |> query_single.() end,
    "classify" => fn input -> Search.Adapter.Elastic.Result.classify_by_type(input) end,
    "classify_flow" => fn input -> Search.Adapter.Elastic.Result.classify_by_type_flow(input) end
  },
  time: 5,
  warmup: 5,
  memory_time: 2,
  inputs: %{
    "Small" => Enum.to_list(1..100) |> build_items.(),
    "Medium" => Enum.to_list(1..1_000) |> build_items.(),
    "Bigger" => Enum.to_list(1..10_000) |> build_items.(),
    "Biggest" => Enum.to_list(1..100_000) |> build_items.()
  }
)
