require Ecto.Query

product_list = fn list ->
  Enum.map(list, fn id ->
    %{id: id, type: Piari.Offers.Product}
  end)
end

query_single = fn list ->
  Enum.each(
    list,
    &(Ecto.Query.from(o in &1.type,
        where: o.id == ^&1.id,
        left_join: details in assoc(o, :details),
        preload: [details: details]
      )
      |> Piari.Repo.one!())
  )
end

query_in = fn map ->
  Enum.each(map, fn {type, ids} ->
    Ecto.Query.from(o in type,
      where: o.id in ^ids
    )
    |> Piari.Repo.all()
    |> Piari.Repo.preload(:details)
    |> Enum.chunk_every(10)
    |> Enum.each(& &1)
  end)
end

query_stream = fn map, callback ->
  Enum.each(map, fn {type, ids} ->
    {:ok, t} =
      Piari.Repo.transaction(fn ->
        Ecto.Query.from(o in type,
          where: o.id in ^ids
        )
        |> Piari.Repo.stream()
        |> callback.()
      end)
  end)
end

callback = fn stream ->
  stream
  |> Stream.chunk_every(100)
  |> Stream.map(&Piari.Repo.preload(&1, :details))
  |> Enum.chunk_every(10)
  |> Enum.each(& &1)
end

filter_list = fn list ->
  Enum.reduce(list, %{}, fn item, types ->
    type = item.type

    case types do
      %{^type => list} ->
        Map.put(types, type, [item.id | list])

      _ ->
        Map.put_new(types, type, [item.id])
    end
  end)
end

Benchee.run(
  %{
    # "get_single_record" => fn input -> input |> product_list.() |> query_single.() end,
    "sort_select_in" => fn input -> input |> product_list.() |> filter_list.() |> query_in.() end,
    "sort_select_in_stream" => fn input ->
      input |> product_list.() |> filter_list.() |> query_stream.(callback)
    end
  },
  time: 15,
  warmup: 5,
  memory_time: 2,
  inputs: %{
    "Small" => Enum.to_list(1..100),
    "Medium" => Enum.to_list(1..1_000),
    "Bigger" => Enum.to_list(1..10_000)
  }
)
