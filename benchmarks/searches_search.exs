alias Piari.Searches

query = "example"

Benchee.run(
  %{
    # "get_single_record" => fn input -> input |> product_list.() |> query_single.() end,
    "search" => fn input -> Searches.search(query, 1, size: input) |> Enum.each(& &1) end,
    "search_lazy" => fn input ->
      Searches.search_lazy(query, 1, size: input) |> Enum.each(& &1)
    end
  },
  time: 15,
  warmup: 5,
  memory_time: 2,
  inputs: %{
    "Small" => 100,
    "Medium" => 1_000,
    "Bigger" => 10_000
  }
)
