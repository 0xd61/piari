build_score_tuples = fn list ->
  Enum.map(list, fn id ->
    {id, :rand.uniform()}
  end)
end

build_score_keywords = fn list ->
  Enum.reduce(list, %{}, fn id, acc ->
    Map.put(acc, id, :rand.uniform())
  end)
end

build_results = fn list ->
  import Ecto.Query, warn: false

  from(p in Piari.Offers.Product,
    join: b in Piari.Accounts.Business,
    as: :business,
    where: p.id in ^list,
    where: p.accounts_business_id == b.id,
    where: p.expires >= ^DateTime.utc_now(),
    select: p,
    select_merge: %{accounts_business_slug: b.slug}
  )
  |> preload(:image)
  |> Piari.Repo.all()
end

build_items = fn list ->
  score_tuple = build_score_tuples.(list) |> Enum.shuffle()
  score_keyword = build_score_keywords.(list)
  results = build_results.(list)
  {score_tuple, score_keyword, results}
end

list_map = fn scores, results ->
  scores = Map.new(scores)

  Enum.sort(results, fn first, second -> scores[first.id] >= scores[second.id] end)
end

map = fn scores, results ->
  Enum.sort(results, fn first, second -> scores[first.id] >= scores[second.id] end)
end

Benchee.run(
  %{
    "list_map" => fn {scores, _, results} -> list_map.(scores, results) end,
    "map" => fn {_, scores, results} -> map.(scores, results) end
  },
  time: 5,
  warmup: 5,
  memory_time: 2,
  inputs: %{
    "Small" => Enum.to_list(1..5) |> build_items.(),
    "Medium" => Enum.to_list(1..25) |> build_items.(),
    "Bigger" => Enum.to_list(1..100) |> build_items.(),
    "Biggest" => Enum.to_list(1..1000) |> build_items.()
  }
)
