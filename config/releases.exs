import Config

config :piari, Piari.Repo, url: System.fetch_env!("PIARI_DATABASE_URL")

config :piari_web, PiariWeb.Endpoint,
  secret_key_base: System.fetch_env!("PIARI_SECRET_KEY_BASE"),
  token_salt: System.fetch_env!("PIARI_TOKEN_SALT"),
  http: [port: System.fetch_env!("PORT")],
  client_callback_url: System.fetch_env!("PIARI_CLIENT_CALLBACK_URL")

config :piari, Piari.Searches.Repo, host: System.fetch_env!("PIARI_ELASTICSEARCH")

config :piari, Piari.Accounts.Auths.Github,
  client_secret: System.fetch_env!("PIARI_GITHUB_SECRET")

config :rollbax,
  access_token: System.fetch_env!("PIARI_ROLLBAR_ACCESS_TOKEN")
