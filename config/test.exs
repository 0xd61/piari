use Mix.Config

# Configure your database
config :piari, Piari.Repo,
  username: System.get_env("MYSQL_USER") || "root",
  password: System.get_env("MYSQL_ROOT_PASSWORD") || "mysql",
  database: System.get_env("MYSQL_DB") || "piari_test",
  hostname: System.get_env("MYSQL_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :piari_web, PiariWeb.Endpoint,
  http: [port: 4002],
  server: false,
  token_salt: "TOKEN_SALT"

# Print only warnings and errors during test
config :logger, level: :warn

config :piari,
  auth_provider: [
    Piari.Accounts.Auths.Github,
    Piari.Accounts.Auths.Email,
    Piari.TestHelpers.Accounts.Auths.FakeAuth
  ]

config :notify, Notify.Adapter.Email.Mailer, adapter: Bamboo.TestAdapter

config :piari, Piari.Accounts.Auths.Github,
  client_id: "79ae4408b65f2e1a416a",
  client_secret: "0c53eaf1292bdc149fcb54e3cf1fdac63add22c9"

config :search,
  adapter: Search.TestHelpers.Adapter,
  host: System.get_env("ELASTICSEARCH_URL") || "http://localhost:9200"

config :store, path: "/tmp"
