# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

config :piari, Piari.Notifications.Email, from: %{name: "Piari", email: "no-reply@samuu.srl"}

# Configure Mix tasks and generators
config :piari,
  ecto_repos: [Piari.Repo],
  languages: ["en", "es", "de"],
  default_locale: "en",
  member_confirm: false,
  secret: "test",
  auth_provider: [
    Piari.Accounts.Auths.Github,
    Piari.Accounts.Auths.Email
  ]

config :piari_web,
  ecto_repos: [Piari.Repo],
  generators: [context_app: :piari]

# Configures the endpoint
config :piari_web, PiariWeb.Endpoint,
  server: true,
  http: [port: 4000],
  url: [host: "localhost"],
  secret_key_base: "UZknRYSZaKBnWDKsh+Clh4O9pj/MoOJduPcfMZpAarRa8jHBc+VFcRyDDeuo4UFq",
  render_errors: [view: PiariWeb.ErrorView, accepts: ~w(json html)],
  pubsub: [name: PiariWeb.PubSub, adapter: Phoenix.PubSub.PG2],
  root: Path.dirname(__DIR__),
  client_callback_url: "http://localhost:3000/api/auth/callback"

config :phoenix, :format_encoders, svg: Phoenix.Template.HTML

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :elastix,
  json_codec: Search.Adapter.Elastic.JSON

# Default Adapter Configuration
config :auth, Auth.Adapter.Github,
  site: "https://api.github.com",
  authorize_url: "https://github.com/login/oauth/authorize",
  token_url: "https://github.com/login/oauth/access_token"

config :auth, Auth.Adapter.Facebook,
  site: "https://graph.facebook.com",
  authorize_url: "https://www.facebook.com/dialog/oauth",
  token_url: "/oauth/access_token"

config :auth, Auth.Adapter.Google,
  site: "https://accounts.google.com",
  authorize_url: "/o/oauth2/auth",
  token_url: "/o/oauth2/token"

# Notify template directory
config :notify, Notify.Templates.Render, directory: "templates"

config :search,
  adapter: Search.Adapter.Elastic,
  host: System.get_env("ELASTICSEARCH_URL") || "http://localhost:9200"

config :rollbax, enabled: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
