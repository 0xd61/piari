ARG ALPINE_VERSION=3.9

# FROM elixir:1.8.1-alpine as build
# begin custom Elixir image
FROM erlang:21-alpine as build

# The following are build arguments used to change variable parts of the image.
# The name of your application/release (required)
ARG APP_NAME=piari
# The version of the application we are building (required)
ARG APP_VSN
# The environment to build with
ARG MIX_ENV=prod
# If you are using an umbrella project, you can change this
# argument to the directory the Phoenix app is in so that the assets
# can be built
ARG PHOENIX_SUBDIR=apps/${APP_NAME}_web

# elixir expects utf8.
ENV APP_NAME=${APP_NAME} \
    APP_VSN=${APP_VSN} \
    MIX_ENV=${MIX_ENV} \
    ELIXIR_VERSION="v1.9.0" \
	  LANG=C.UTF-8

RUN set -xe \
	&& ELIXIR_DOWNLOAD_URL="https://github.com/elixir-lang/elixir/archive/${ELIXIR_VERSION#*@}.tar.gz" \
	&& ELIXIR_DOWNLOAD_SHA512="3ecdbb2565cdaf51d6119b5dba42b4b180484aea96e9fe1f85febfb7c3f185b869aab94a22b5052dd84073be1a50ecb97d76dd1bc87f7fdc38a12cff65d2caf6" \
	&& buildDeps=' \
		ca-certificates \
		curl \
		make \
	' \
	&& apk add --no-cache --virtual .build-deps $buildDeps \
	&& curl -fSL -o elixir-src.tar.gz $ELIXIR_DOWNLOAD_URL \
	&& echo "$ELIXIR_DOWNLOAD_SHA512  elixir-src.tar.gz" | sha512sum -c - \
	&& mkdir -p /usr/local/src/elixir \
	&& tar -xzC /usr/local/src/elixir --strip-components=1 -f elixir-src.tar.gz \
	&& rm elixir-src.tar.gz \
	&& cd /usr/local/src/elixir \
	&& make install clean \
	&& apk del .build-deps
# end custom Elixir image

# install build dependencies
RUN apk add --update git build-base nodejs yarn python

# prepare build dir
RUN mkdir /app
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV=prod

COPY mix.exs mix.lock ./
COPY config config
COPY apps apps
RUN mix do deps.get, deps.compile, compile

RUN cd ${PHOENIX_SUBDIR}/assets && \
  yarn install && \
  yarn deploy && \
  cd - && \
  mix phx.digest

# build release
COPY rel rel
RUN mix release

# prepare release image
FROM alpine:${ALPINE_VERSION} AS app
RUN apk add --update bash openssl imagemagick

# ONLY FOR TESTING (WILL BE REPLACED BY S3)
RUN mkdir -p /media/uploads
RUN mkdir /app
WORKDIR /app

COPY --from=build /app/_build/prod/rel/${APP_NAME} ./
RUN chown -R nobody: /app
USER nobody

ENV HOME=/app

CMD ["piari/bin/piari", "start"]
