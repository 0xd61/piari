# Piari is a brand name and should not be localized.
-piari-brand = Piari
-piari-short-brand = Piari
-samuu = Samuu S.R.L.
introTitle = { -piari-brand }
navAccountLabel = Account
signInButton = Sign In

submitButton = Save
backButton = Back
abortButton = Abort
failedTitle = Failed!
failedDescription = Please try again later.

accountHeading = Account
accountOAuthTitle = Connected with { $provider }
accountOAuthSubtitle = This account is connected with { $provider }
accountOAuthDisconnectButton = Disconnect
accountNameTitle = What do you prefer to be called?
accountNameInputPlaceholder = Type your name here...
accountEmailTitle = How can I contact you if something important happens?
accountEmailSubtitle = This email address is also used for your login
accountEmailFooter = If you request a change of your email, please check your inbox for a verification link.
accountEmailInputPlaceholder = Type your email here...
accountLanguageTitle = What is your preferred language?
accountLanguageFooter = The language will be changed on your next login.
accountDangerHeading = Danger Zone
accountLogoutTitle = Logout
accountLogoutSubtitle = This will close your current session
accountLogoutButton = Logout
accountDeleteTitle = Delete your account
accountDeleteSubtitle = Once you delete your account there is no going back
accountDeleteFooter = You have to leave or delete your owned businesses first.
accountDeleteButton = Delete this account

businessCreateButton = Create Business
businessSelectButton = Select Business
businessHeading = { $name ->
   [undefined] New Business
  *[other] {$name} Settings
}
businessNameTitle = How would you name your business?
businessNameInputPlaceholder = Type the business name here...
businessSlugTitle = Use a custom url for your business
businessSlugSubtitle = This is your personal business url within { -piari-brand }
businessPhoneTitle = How can others reach your business?
businessPhoneInputPlaceholder = Type the phone number here...
businessPhoneFooter = This phone number is used to validate your business.
businessDangerHeading = Danger Zone
businessDeleteTitle = Delete this business
businessDeleteSubtitle = Once you delete your business there is no going back
businessDeleteButton = Delete { $name }
businessDeleteFooter = Deleting a business will remove all associated products.
