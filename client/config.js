require("dotenv").config();

const defaults = {
  PIARI_SECRET: "piari-client-application",
  PIARI_API_URL: "http://localhost:4000/api",
  PIARI_OAUTH_URL: "http://localhost:4000/session/oauth",
  PIARI_LOCALES: "en,es,de"
};

module.exports = (targetPrefix = "process.env.") => {
  const SAPPER_APP_ENV_VARS = {};
  for (let key in defaults) {
    let envVar = process.env[key];
    if (envVar) {
      SAPPER_APP_ENV_VARS[targetPrefix + key] = "'" + envVar + "'";
    } else {
      SAPPER_APP_ENV_VARS[targetPrefix + key] = "'" + defaults[key] + "'";
    }
  }
  return SAPPER_APP_ENV_VARS;
};
