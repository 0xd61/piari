module.exports = {
  theme: {
    extend: {
      inset: {
        px: "1px"
      },
      flexGrow: {
        "2": 2,
        "3": 3
      }
    },
    customForms: theme => ({
      default: {
        input: {
          borderRadius: 0,
          borderColor: theme("colors.neut.300"),
          borderWidth: `0 0 ${theme("borderWidth.default")} 0`,
          "&:focus": {
            boxShadow: 0,
            borderColor: theme("colors.white")
          },
          "&::placeholder": {
            color: theme("colors.neut.400"),
            opacity: "1"
          }
        },
        select: {
          borderRadius: 0,
          borderColor: theme("colors.neut.300"),
          borderWidth: `0 0 ${theme("borderWidth.default")} 0`,
          "&:focus": {
            boxShadow: 0,
            borderColor: theme("colors.white")
          }
        }
      }
    }),
    colors: {
      white: "#FFFFFF",
      black: "#000000",
      prim: {
        900: "#240754",
        800: "#34126F",
        700: "#421987",
        600: "#51279B",
        500: "#653CAD",
        400: "#724BB7",
        300: "#8662C7",
        200: "#A081D9",
        100: "#CFBCF2",
        "000": "#EAE2F8"
      },
      sec: {
        900: "#610316",
        800: "#8A041A",
        700: "#AB091E",
        600: "#CF1124",
        500: "#E12D39",
        400: "#EF4E4E",
        300: "#F86A6A",
        200: "#FF9B9B",
        100: "#FFBDBD",
        "000": "#FFE3E3"
      },
      neut: {
        900: "#102A43",
        800: "#243B53",
        700: "#334E68",
        600: "#486581",
        500: "#627D98",
        400: "#829AB1",
        300: "#9FB3C8",
        200: "#BCCCDC",
        100: "#D9E2EC",
        "000": "#F0F4F8"
      },
      supp1: {
        900: "#004440",
        800: "#016457",
        700: "#048271",
        600: "#079A82",
        500: "#17BB97",
        400: "#2DCCA7",
        300: "#5FE3C0",
        200: "#8EEDD1",
        100: "#C6F7E9",
        "000": "#F0FCF9"
      },
      supp2: {
        900: "#8D2B0B",
        800: "#B44D12",
        700: "#CB6E17",
        600: "#DE911D",
        500: "#F0B429",
        400: "#F7C948",
        300: "#FADB5F",
        200: "#FCE588",
        100: "#FFF3C4",
        "000": "#FFFBEA"
      }
    }
  },
  variants: {},
  plugins: [require("@tailwindcss/custom-forms")]
};
