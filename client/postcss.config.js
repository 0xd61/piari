// postcss.config.js
const purgecss = (module.exports = (purge = false) => {
  return [
    require("postcss-import")(),
    require("tailwindcss")("./tailwind.config.js"),
    require("autoprefixer")(),
    purge &&
      require("@fullhuman/postcss-purgecss")({
        // Specify the paths to all of the template files in your project
        content: ["./**/*.svelte", "./**/*.html"],

        // Include any special characters you're using in this regular expression
        extractors: [
          {
            extractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
            extensions: ["svelte", "html"]
          }
        ],
        whitelist: ["html", "body"]
      }),
    purge &&
      require("cssnano")({
        preset: "default"
      })
  ].filter(Boolean);
});
