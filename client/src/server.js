import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import bodyParser from "body-parser";
import session from "express-session";
import sessionFileStore from "session-file-store";
import * as sapper from "@sapper/server";
import language from "@/middleware/language";
import locale from "@/middleware/locale";

import "./css/app.css";

const FileStore = sessionFileStore(session);

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

polka() // You can also use Express
  .use(bodyParser.json())
  .use(
    session({
      secret: process.env.PIARI_SECRET,
      resave: false,
      saveUninitialized: true,
      cookie: {
        maxAge: 31536000
      },
      store: new FileStore({
        path: process.env.NOW_REGION ? `/tmp/sessions` : `.sessions`
      })
    }),
    // TODO: optimize (currently runs on every request)
    language,
    locale
  )
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      session: req => ({
        user: req.session && req.session.user,
        locale: req.language || "en"
      })
    })
  )
  .listen(PORT, err => {
    if (err) console.log("error", err);
  });
