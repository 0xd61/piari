const base = process.env.PIARI_API_URL;

function send({ method, path, data, token, opts }) {
  const fetch = process.browser ? window.fetch : require("node-fetch").default;
  const timeout = opts.timeout || 10;
  const uri = url(path, opts);

  // make sure default values do not overwrite opts
  opts = { method, headers: {}, ...opts };

  if (data) {
    opts.headers["Content-Type"] = "application/json";
    opts.body = JSON.stringify(data);
  }

  if (token) {
    opts.headers["Authorization"] = `Bearer ${token}`;
  }

  return _timeout(
    timeout,
    fetch(uri, opts)
      .then(r => r.text())
      .then(json => {
        return _parseJSON(json);
      })
  );
}

export function url(path, opts = {}) {
  const version = opts.version || "v1";
  return `${base}/${version}/${path}`;
}

export function get(path, opts = {}) {
  const { token } = opts;
  return send({ method: "GET", path, token, opts });
}

export function del(path, opts = {}) {
  const { token } = opts;
  return send({ method: "DELETE", path, token, opts });
}

export function post(path, data, opts = {}) {
  const { token } = opts;
  return send({ method: "POST", path, data, token, opts });
}

export function put(path, data, opts = {}) {
  const { token } = opts;
  return send({ method: "PUT", path, data, token, opts });
}

function _timeout(s, promise) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      reject(new Error("Request timed out"));
    }, s * 1000);
    promise.then(resolve, reject);
  });
}

function _parseJSON(json) {
  try {
    return JSON.parse(json);
  } catch (err) {
    return json;
  }
}
