import * as sapper from "@sapper/app";
import { getTranslator } from "@/locale";
import { setTranslate } from "@/stores/locale";
import { locale } from "@/utils";

(async function() {
  const translator = await getTranslator(locale());
  setTranslate(translator);

  sapper.start({
    target: document.querySelector("#sapper")
  });
})();
