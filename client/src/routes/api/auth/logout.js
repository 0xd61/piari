import * as api from "@/api";

export function post(req, res) {
  const { token } = req.body;
  delete req.session.user;
  revokeToken(token);

  res.end(JSON.stringify({ success: true }));
}

async function revokeToken(token) {
  api.del(`auths`, { token }).catch(error => console.log(error));
}
