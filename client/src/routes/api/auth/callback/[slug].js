import * as api from "@/api";
import * as utils from "@/utils";
import cookie from "cookie";

export function get(req, res, next) {
  const { slug } = req.params;
  const { temp_token, locale } = req.query;

  const { user, token } = getApiKey(temp_token)
    .then(({ user, token }) => {
      if (!user || !token) {
        console.log("token or user undefined");
        logout();
        return;
      }

      // Set session
      req.session.user = { id: user, token: token };

      // Set user locale
      res.setHeader(
        "Set-Cookie",
        cookie.serialize("locale", locale, {
          path: "/"
        })
      );

      res.writeHead(302, {
        Location: `/`
      });
      res.end();
    })
    .catch(errors => {
      console.log(errors);
      logout();
    });
}

function logout() {
  delete req.session.user;
  res.writeHead(302, {
    Location: `/login`
  });
  res.end();
}

function getCookie(req, name) {
  const match = req.cookie.match(new RegExp("(^| )" + name + "=([^;]+)"));
  if (!match) return;
  return match[2];
}

function getApiKey(temp_token) {
  return api
    .post(`auths`, { temp_token })
    .then(res => {
      if (res.error) {
        console.log(res.error);
        return;
      }
      return { user: res.user_id, token: res.token };
    })
    .catch(error => console.log(error));
}
