import * as api from "@/api";

const sessionUrl = process.env.PIARI_OAUTH_URL;

export function get(req, res, next) {
  const { slug } = req.params;
  res.writeHead(302, {
    Location: `${sessionUrl}/${slug}`
  });
  res.end();
}

export function post(req, res, next) {
  const { slug } = req.params;
  const { email } = req.body;

  api.post(`session/${slug}`, { email }).then(({ url }) => {
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify({ redirect: url }));
  });
}
