import { writable } from "svelte/store";

import { getTranslator } from "@/locale";
import { locale } from "@/utils";

export let translate = writable(function() {
  throw new Error("uninitialized translate function. call setTranslate first");
});

export function setTranslate(t) {
  translate.set(t);
}
