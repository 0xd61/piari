import { get, writable } from "svelte/store";
import { get as fetch } from "@/api";

export let current;

export function set(business) {
  setLocal(business);
  current.set(business);
}

export async function refresh(token) {
  const list = await fetch("businesses", { token });
  const { id } = get(current);
  const business = list.find(item => item.id === id);

  if (!business) return;
  set(business);
}

if (typeof window !== "undefined") {
  current = writable(getLocal() || null);
} else {
  current = writable(null);
}

function setLocal(business) {
  localStorage.setItem("context", JSON.stringify(business));
}

function getLocal() {
  const item = localStorage.getItem("context");
  return JSON.parse(item);
}
