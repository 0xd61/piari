import { stores } from "@sapper/app";

export function post(endpoint, data) {
  return fetch(endpoint, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  }).then(r => r.json());
}

export function setCookie(name, { value, days }) {
  let expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = `expires=${date.toUTCString()};`;
  }
  document.cookie = `${name}=${value || ""}; ${expires} path=/;`;
}

export function getCookie(name) {
  const match = document.cookie.match(new RegExp("(^| )" + name + "=([^;]+)"));
  if (!match) return;
  return match[2];
}

export function deleteCookie(name) {
  document.cookie = `${name}=; Max-Age=-99999999;`;
}

export function locale() {
  // TODO: replace with html tag value
  return getCookie("locale");
}
