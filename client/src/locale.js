import { FluentBundle, FluentResource } from "@fluent/bundle";

function makeBundle(locale, ftl) {
  const bundle = new FluentBundle(locale, { useIsolating: false });
  const resource = new FluentResource(ftl);
  bundle.addResource(resource);
  return bundle;
}

export async function getTranslator(locale) {
  const bundles = [];
  const { default: en } = await selectImport();
  if (locale !== "en") {
    const { default: ftl } = await selectImport(locale);

    bundles.push(makeBundle(locale, ftl));
  }
  bundles.push(makeBundle("en", en));
  return function(id, data) {
    for (let bundle of bundles) {
      const msg = bundle.getMessage(id);
      if (msg && msg.value) {
        return bundle.formatPattern(msg.value, data);
      }
    }
  };
}

// TODO: make dynamic imports?
function selectImport(locale) {
  switch (locale) {
    case "es":
      return import("@/../../locales/es/piari.ftl");
    case "de":
      return import("@/../../locales/de/piari.ftl");
    default:
      return import("@/../../locales/en/piari.ftl");
  }
}
