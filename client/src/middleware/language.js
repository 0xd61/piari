import fs from "fs";
import path from "path";
import { negotiateLanguages } from "@fluent/langneg";
import cookie from "cookie";
const langData = require("cldr-core/supplemental/likelySubtags.json");

// We return early in the middleware if the lang header is long.
// If that ever changes we should re-evaluate this regex.
// eslint-disable-next-line security/detect-unsafe-regex
const acceptLanguages = /(([a-zA-Z]+(-[a-zA-Z0-9]+){0,2})|\*)(;q=[0-1](\.[0-9]+)?)?/g;

function allLangs() {
  return fs.readdirSync(
    path.join(__dirname, "..", "..", "..", "..", "locales")
  );
}

function locales() {
  let localeString = process.env.PIARI_LOCALES;
  let locales = localeString.split(",");
  return locales;
}

const languages =
  process.env.NODE_ENV === "development" ? allLangs() : locales();

export default function(req, res, next) {
  let cookies = cookie.parse(req.headers.cookie || "");
  if (cookies.locale) {
    req.language = cookies.locale;
    return next();
  }
  const header = req.headers["accept-language"] || "en";
  if (header.length > 255) {
    req.language = "en";
    res.setHeader(
      "Set-Cookie",
      cookie.serialize("locale", req.language, {
        path: "/"
      })
    );
    return next();
  }
  const langs = header.replace(/\s/g, "").match(acceptLanguages) || ["en"];
  const preferred = langs
    .map(l => {
      const parts = l.split(";");
      return {
        locale: parts[0],
        q: parts[1] ? parseFloat(parts[1].split("=")[1]) : 1
      };
    })
    .sort((a, b) => b.q - a.q)
    .map(x => x.locale);
  req.language = negotiateLanguages(preferred, languages, {
    strategy: "lookup",
    likelySubtags: langData.supplemental.likelySubtags,
    defaultLocale: "en"
  })[0];
  res.setHeader(
    "Set-Cookie",
    cookie.serialize("locale", req.language, {
      path: "/"
    })
  );
  next();
}
