import { getTranslator } from "@/locale";
import { setTranslate } from "@/stores/locale";

export default async function(req, res, next) {
  try {
    const translator = await getTranslator(req.language || "en");
    setTranslate(translator);
    next();
  } catch (error) {
    next(error);
  }
}
