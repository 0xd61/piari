# Piari.Umbrella

## Apps

Piari is an umbrella project with several applications.

### Auth

Authentication app. Handles OAuth login and url tokens which can be sent via email.

### Notify

Notification app. Sends messages via Email

### Search

Search app. Connects to search provider aka. Elasticsearch and provides text
search

### Piari

Main app. Is the main piari app. It uses Repo to connect to a postgres DB.

### PiariWeb

Main app server. Handles incoming requests.


## Getting Started

### Create Development Environment

Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).
Install Elixir and Erlang (preferable with [asdf](https://asdf-vm.com/#/))

Clone the repo and install the dependencies:

```bash
git clone git@gitlab.com:samuu.dev/piari.git piari_umbrella
cd piari_umbrella
docker-compose up -d
mix deps.get
cd apps/piari_web/assets 
yarn install
cd -
```

For more info on the commands see the official [guide](https://hexdocs.pm/phoenix/up_and_running.html)

### Initialize the DB and Elastic Search

```bash
# this will reset the DB also create the seed data
mix ecto.reset 
# creates a new elastic search index offers_v1 and populates it with the seeded products
mix search.index offers_v1 Elixir.Piari.Offers.Product
# creates an alias offers to offers_v1 (if the alias exists it will swap to the new index)
mix search.index.swap offers_v1 offers
```

### Start the dev server

The server will listen on `0.0.0.0:4000`

```bash
mix phx.server
```

## Project Structure

This project is created as Elixir Umbrella Project. The applications are listed [here](#apps)


```bash
./
├── Dockerfile
├── README.md
├── apps # Contains the different applications
├── benchmarks
├── config # Config for all applications
├── deps
├── doc # Outdated (Document for DB structure)
├── docker-compose.yml
├── docs # Generated docs
├── heroku.yml
├── mix.exs
├── mix.lock
├── network_throttle.sh # Throttle service to emulate chaco network ;-)
└── rel # additional release config
```

Each application in the application folder is a separate Elixir application. The dependencies are:

```
piari_web --> piari -|-> auth
                     |-> notify
                     |-> search
                     |-> upload
```

The Server Code (Routes, Controllers, Views) lives in `apps/piari_web/lib/piari_web/`:

```bash
apps/piari_web/lib/piari_web/
├── application.ex
├── channels
├── controllers
├── endpoint.ex
├── gettext.ex
├── param.ex
├── plugs.ex
├── router.ex
├── templates
└── views
```

## Frontend

The frontend is written with svelte components. Everything which runs on the client side is located at `apps/piari_web/assets`. These assets are compiled by webpack
and stored at `apps/piari_web/priv/static`. The frontend is seperated into different pages. These pages are the "entrypoint" of the svelte components and are located at `apps/piari_web/assets/pages`.

During the webpack build two versions of these components are build. One is the client version which is stored as bundle in `apps/piari_web/priv/static/js`. The other one is a SSR build.
This SSR build is stored at `apps/piari_web/priv/pages`. These pages use the Elixir/Phoenix Templating syntax (EEX) and are used by phoenix controllers and views to render the initial page.
Then the client side js is loaded and replaces the SSR content with the JS components. The filename of each page must be the same as the controller/view in the server.
e.g. 

```
page: assets/pages/account.html.svelte
controller: controllers/account_controller.ex
view: views/account_view.ex
```