.PHONY: help

export VERSION ?= `grep 'version' mix.exs | sed -e 's/ //g' -e 's/version://' -e 's/[",]//g'`

HASH := `git rev-parse --short HEAD`

SERVICE := api
export PROJECT := piari-251320
export REPOSITORY ?= gcr.io
IMAGE := $(patsubst /%,%,$(REPOSITORY)/$(PROJECT)/$(SERVICE):$(HASH))

PWD ?= `pwd`

help:
	@echo "$(IMAGE):$(VERSION)"
	@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

init: ## Initialize the project from a clean state
	mix local.rebar --force
	mix local.hex --force
	mix deps.get

compile: ## Build the application
	mix do compile, phx.digest

clean: ## Clean up generated artifacts
	mix clean

rebuild: clean compile ## Rebuild the application

release: ## Build a release of the application with MIX_ENV=prod
	MIX_ENV=prod mix do deps.get, compile
	MIX_ENV=prod mix phx.digest
	MIX_ENV=prod mix release --verbose
	@cp _build/prod/rel/$(IMAGE)/releases/$(VERSION)/$(IMAGE).tar.gz $(IMAGE).tar.gz

docker: ## Build a release docker image
	@echo "\n~> building docker image"
ifeq "$(REPOSITORY)" "gcr.io"
	@gcloud builds submit -t $(IMAGE)
else
	@docker image build -t $(IMAGE) --build-arg APP_NAME=$(PROJECT)/$(SERVICE) --build-arg APP_VSN=$(VERSION) .
endif

.PHONY: client
client: ## Build and deploy client application
	@echo "\n~> building client"
	@make -C ./client deploy
